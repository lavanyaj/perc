from itertools import izip_longest
import sys
import fileinput

annot = sys.argv[1]
filenames = sys.argv[2:]

num_lines = [sum([1 for line in open(filename)]) for filename in filenames]
#print num_lines

sorted_fileindices = sorted([f for f in range(len(filenames))], key=lambda f: -num_lines[f])
#print sorted_fileindices

sorted_filenames = [filenames[f] for f in sorted_fileindices]
#print sorted_filenames

for i in sorted_fileindices:
    #print "%d,%d,"%(num_lines[i],num_lines[i]),
    pass
#print ""
files = [open(filename) for filename in sorted_filenames]


#print annot
for lines in izip_longest(*files):
    print ",".join([line.rstrip() for line in lines if line is not None])
    pass

