# USE LIKE THIS python stats4.py 60pc-12us-100G OUTFILE
# input files number 60pc-12us-100G-1.csv, .. where the files have ..
# (time,idealFlowSynId), (time, idealFlowSize), (time, idealFlowFCT) , perhop (..), rcp (..) .. no sorting just export filter
# like attr:l(0.6) AND attr:j(10) AND attr:c(10e9) AND  (name(flowSynId:vector) OR name(flowSynSize:vector) OR name(flowCompletionTime:vector) )
# .. sort by expname before exporting to 60pc-12us-100G.csv


import os
import sys
EXPERIMENT=sys.argv[1] # "all-1.2us.csv"
OUTFILE= sys.argv[2]#"stats-1.2us.txt"

print EXPERIMENT
print OUTFILE

#bins = ["%d-%d"%((10**i)+1, 10**(i+1)) for i in range(3,8)]
bins = ["%d-%d"%(i,j) for i,j in [(1001,10000),(10001,1000000),(1000000,100000000)]]
#binNames = [str(sz) for sz in bins]
binNames = ["small", "medium", "large"]
print bins

skip = ["CHARNY_AGG"]
algs = ["CHARNY_AGG", "CHARNY_CONS", "DCTCP", "IDEAL", "PERC", "RCP"]
#algs = ["ideal","perc-c2","rcp"]
indalgs = {}#"ideal":0, "perhopa": 1, "perhopc": 2, "rcp": 3}
fctcols = {}
sizecols = {}
i = 0
for alg in algs:
    indalgs[alg] = i
    fctcols[alg] = 6*indalgs[alg]+1+1
    sizecols[alg] = 6*indalgs[alg]+6-2+1+1
    i+=1
    pass
print fctcols
print sizecols


loadStr,capStr,rttStr = EXPERIMENT.split("-")


cap = int(capStr.rstrip("G")) * 1.0e+9
rtt = float(rttStr[:-2]) * 1.0e-6
load = int(loadStr[:-2])/100.0

print "cap: %f, rtt: %f, load: %f" % (cap, rtt, load)

LSSTRING_OUTFILE = "tmp/%s_LSSTRING.txt"%OUTFILE
cmd = "ls -1 %s-*csv | sed \"s/\./-/g\" | sort -t\"-\" -n -k 4 | sed \"s/-csv/\.csv/g\" > %s" % (EXPERIMENT, LSSTRING_OUTFILE)
os.system(cmd)

INFILE =  "tmp/%s_INFILE_%s.txt"%(OUTFILE, EXPERIMENT)
cmd = "cat %s | xargs -n %d python newPaste.py %s > %s" % (LSSTRING_OUTFILE, len(algs)*3, EXPERIMENT, INFILE) 
print cmd
os.system(cmd)


cleanup = []
cleanup.append(cmd)
#print cmd



#cmd = "echo \"%s size in B\" > %s" % (str(bins), OUTFILE);
#print cmd
#os.system(cmd)

for i in range(1):#alg in ["RCP", "IDEAL", "PERHOP"]:
    #cmd = "echo \"%s STATS\" >> %s" % (alg, OUTFILE);
    #print cmd
    #os.system(cmd)

    allFiles = []
    binindex = 0
    for bin in bins:
        binindex += 1
        bin1, bin2 = [int(b) for b in bin.split("-")]
        #BIN_OUTFILE = "%s_BIN_%s"%(OUTFILE, bin)
        #allFiles.append(BIN_OUTFILE)
        #cmd = "echo \"%s\" >> %s" % (bin, OUTFILE);
        #print cmd
        #os.system(cmd)

        #allBinFiles = []
        for name in algs:
            if name in skip:
                continue
            ALG_BIN_OUTFILE = "tmp/%s_BIN_%s_ALG_%s" % (OUTFILE, bin, name)
            allFiles.append(ALG_BIN_OUTFILE)
            awkIf = "if (($%d*1500) >= %d && ($%d*1500) <= %d)" % (sizecols[name], bin1, sizecols[name], bin2)

            #awkSizeStr =  "\"size_pkts \"$%d" % sizecol
            #awkMinFctStr = "\" min_fct \"((($%d*12000)/%f)+%f)" % (sizecols[name], cap, rtt)

            
            awkFctStr = "\" %s_fct \"$%d" % (name, fctcols[name])
            awkStretchHeaderStr = "\" %d_%s_%s_stretch \"" % (binindex, binNames[binindex-1], name)
            awkStretchStr = "($%d/((($%d*12000)/%f)+%f))" % (fctcols[name], sizecols[name], cap, rtt)
            
          
            catStr = "cat %s" % INFILE
            awk1Str = "awk -F, '{if (NR >=2) {%s {print %s;}}}'" % (awkIf, awkStretchStr)

            cmd = "echo %s > %s;" % (awkStretchHeaderStr, ALG_BIN_OUTFILE)
            #print cmd
            os.system(cmd)            

            cmd = "%s | %s  >> %s;" % (catStr, awk1Str, ALG_BIN_OUTFILE)
            os.system(cmd)            
            pass
        pass
    
    allFilesStr = " ".join(filename for filename in allFiles)
    ALL_OUTFILE = "tmp/%s_all-%s.txt"%(OUTFILE, EXPERIMENT)

    PERCENTILES_OUTFILE = "tmp/%s_PERCENTILES-%s.txt"%(OUTFILE, EXPERIMENT)
    cmd = "python paste_data.py %s %s > %s" % (OUTFILE, allFilesStr, ALL_OUTFILE)
    #print cmd
    os.system(cmd)

        
    rscriptStr = "Rscript b.R"

    cmd = "cat %s > tmp/%s_BOXPLOT_INPUT_%s.txt" % (ALL_OUTFILE, OUTFILE, EXPERIMENT)
    os.system(cmd)
    
    cmd = "cat %s | Rscript b.R > %s" % (ALL_OUTFILE, PERCENTILES_OUTFILE)
    os.system(cmd)

    cmd = "mv boxplot out/%s_MEANS_%s.jpeg" % (OUTFILE, EXPERIMENT)
        #print cmd
    os.system(cmd)

    cmd = "mv boxplot2 out/%s_TAILS_%s.jpeg" % (OUTFILE, EXPERIMENT)
    #print cmd
    os.system(cmd)

    cmd = "mv boxplot3 out/%s_MEANANDTAILS_%s.jpeg" % (OUTFILE, EXPERIMENT)
    #print cmd
    os.system(cmd)
    
    allFilesLbStr = "\\n".join(filename for filename in allFiles)
    cmd = "printf \"%s\" | xargs rm" % (allFilesLbStr)
    #print cmd
    #os.system(cmd)
