[1] "X1_small_CHARNY_CONS_stretch" "X1_small_DCTCP_stretch"      
[3] "X1_small_IDEAL_stretch"       "X1_small_PERC_stretch"       
[5] "X1_small_RCP_stretch"        
[1] "X2_medium_CHARNY_CONS_stretch" "X2_medium_DCTCP_stretch"      
[3] "X2_medium_IDEAL_stretch"       "X2_medium_PERC_stretch"       
[5] "X2_medium_RCP_stretch"        
[1] "X3_large_CHARNY_CONS_stretch" "X3_large_DCTCP_stretch"      
[3] "X3_large_IDEAL_stretch"       "X3_large_PERC_stretch"       
[5] "X3_large_RCP_stretch"        
[1] "number of algs"
[1] 5
[1] "names"
[1] "CHARNY_CONS_stretch" "DCTCP_stretch"       "IDEAL_stretch"      
[4] "PERC_stretch"        "RCP_stretch"        
[1] "means"
                                Small   Medium    Large
X1_small_CHARNY_CONS_stretch 4.668678 3.645221 2.045541
X1_small_DCTCP_stretch       1.026072 2.478121 2.645208
X1_small_IDEAL_stretch       1.020433 1.256276 1.693979
X1_small_PERC_stretch        2.507692 2.280087 1.853379
X1_small_RCP_stretch         2.146717 2.399871 2.376044
[1] "tails"
                                    Small    Medium
X1_small_CHARNY_CONS_stretch.99% 4.940639  5.241924
X1_small_DCTCP_stretch.99%       1.167921  6.781359
X1_small_IDEAL_stretch.99%       1.156089  3.292685
X1_small_PERC_stretch.99%        2.787750  3.893800
X1_small_RCP_stretch.99%         6.554495 10.311813
