[1] "X1_small_CHARNY_CONS_stretch" "X1_small_DCTCP_stretch"      
[3] "X1_small_IDEAL_stretch"       "X1_small_PERC_stretch"       
[5] "X1_small_RCP_stretch"        
[1] "X2_medium_CHARNY_CONS_stretch" "X2_medium_DCTCP_stretch"      
[3] "X2_medium_IDEAL_stretch"       "X2_medium_PERC_stretch"       
[5] "X2_medium_RCP_stretch"        
[1] "X3_large_CHARNY_CONS_stretch" "X3_large_DCTCP_stretch"      
[3] "X3_large_IDEAL_stretch"       "X3_large_PERC_stretch"       
[5] "X3_large_RCP_stretch"        
[1] "number of algs"
[1] 5
[1] "names"
[1] "CHARNY_CONS_stretch" "DCTCP_stretch"       "IDEAL_stretch"      
[4] "PERC_stretch"        "RCP_stretch"        
[1] "means"
                                Small   Medium    Large
X1_small_CHARNY_CONS_stretch 4.678994 4.013560 3.165294
X1_small_DCTCP_stretch       1.046193 2.840662 3.613211
X1_small_IDEAL_stretch       1.045063 1.587355 2.520696
X1_small_PERC_stretch        2.518940 2.605755 2.866291
X1_small_RCP_stretch         2.289520 2.961813 3.597968
[1] "tails"
                                    Small    Medium
X1_small_CHARNY_CONS_stretch.99% 5.205712  7.518223
X1_small_DCTCP_stretch.99%       1.211547  7.913302
X1_small_IDEAL_stretch.99%       1.245210  5.767777
X1_small_PERC_stretch.99%        2.937181  6.615824
X1_small_RCP_stretch.99%         8.779738 11.859521
