[1] "X1_small_CHARNY_CONS_stretch" "X1_small_DCTCP_stretch"      
[3] "X1_small_IDEAL_stretch"       "X1_small_PERC_stretch"       
[5] "X1_small_RCP_stretch"        
[1] "X2_medium_CHARNY_CONS_stretch" "X2_medium_DCTCP_stretch"      
[3] "X2_medium_IDEAL_stretch"       "X2_medium_PERC_stretch"       
[5] "X2_medium_RCP_stretch"        
[1] "X3_large_CHARNY_CONS_stretch" "X3_large_DCTCP_stretch"      
[3] "X3_large_IDEAL_stretch"       "X3_large_PERC_stretch"       
[5] "X3_large_RCP_stretch"        
[1] "number of algs"
[1] 5
[1] "names"
[1] "CHARNY" "DCTCP"  "IDEAL"  "PERC"   "RCP"   
[1] "means"
                                Small   Medium    Large
X1_small_CHARNY_CONS_stretch 4.669088 3.645307 2.045455
X1_small_DCTCP_stretch       1.061250 2.487693 2.645232
X1_small_IDEAL_stretch       1.020480 1.256314 1.693980
X1_small_PERC_stretch        2.507758 2.281001 1.853106
X1_small_RCP_stretch         2.148630 2.408136 2.379249
[1] "tails"
                                    Small    Medium
X1_small_CHARNY_CONS_stretch.99% 4.939752  5.241557
X1_small_DCTCP_stretch.99%       1.188403  6.550705
X1_small_IDEAL_stretch.99%       1.156002  3.292663
X1_small_PERC_stretch.99%        2.811908  3.890594
X1_small_RCP_stretch.99%         7.001558 10.199621
