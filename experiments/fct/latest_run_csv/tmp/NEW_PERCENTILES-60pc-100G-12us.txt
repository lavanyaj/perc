[1] "X1_small_CHARNY_CONS_stretch" "X1_small_DCTCP_stretch"      
[3] "X1_small_IDEAL_stretch"       "X1_small_PERC_stretch"       
[5] "X1_small_RCP_stretch"        
[1] "X2_medium_CHARNY_CONS_stretch" "X2_medium_DCTCP_stretch"      
[3] "X2_medium_IDEAL_stretch"       "X2_medium_PERC_stretch"       
[5] "X2_medium_RCP_stretch"        
[1] "X3_large_CHARNY_CONS_stretch" "X3_large_DCTCP_stretch"      
[3] "X3_large_IDEAL_stretch"       "X3_large_PERC_stretch"       
[5] "X3_large_RCP_stretch"        
[1] "number of algs"
[1] 5
[1] "names"
[1] "CHARNY" "DCTCP"  "IDEAL"  "PERC"   "RCP"   
[1] "means"
                                Small   Medium    Large
X1_small_CHARNY_CONS_stretch 4.680942 4.013526 3.165219
X1_small_DCTCP_stretch       1.044831 2.872318 3.632977
X1_small_IDEAL_stretch       1.045004 1.587392 2.520719
X1_small_PERC_stretch        2.519116 2.605098 2.866687
X1_small_RCP_stretch         2.295582 2.960608 3.597403
[1] "tails"
                                    Small    Medium
X1_small_CHARNY_CONS_stretch.99% 5.319551  7.618507
X1_small_DCTCP_stretch.99%       1.213149  8.276695
X1_small_IDEAL_stretch.99%       1.246682  5.767776
X1_small_PERC_stretch.99%        2.955376  6.644656
X1_small_RCP_stretch.99%         8.752270 11.452777
