origRates <- read.csv(file("stdin"))

cols <- grep( "Rate", names(origRates))
cols
origRates[, cols] <- origRates[, cols]/10e+9

timeCols <- grep( "time", names(origRates))

origRates[,timeCols] <- (origRates[,timeCols]) / 1e-6

rates <- origRates

percFlow1 <- rates[,1:2]
percFlow2 <- rates[,3:4]
rcpFlow1 <- rates[,5:6]
rcpFlow2 <- rates[,7:8]

jpeg("timeseries.jpeg")
plot(percFlow1, type="l",col="red",xlab="Time (us)", ylab="Sending rate (Gbps)")
lines(percFlow2, col="purple")
lines(rcpFlow1, col="blue")
lines(rcpFlow2, col="green")

abline(v=600, col = "lightgray")
abline(v=600+(20*12), col = "lightgray")



flowNames <- c("Flow 1 (PERC)", "Flow 2 (PERC)", "Flow 1 (RCP)", "Flow 2 (RCP)")
colors <- c("red","purple","blue","green")

par(xpd=TRUE)
legend('topleft',flowNames,fill=colors,inset=c(0,-0.1), horiz=TRUE, cex=0.8)


