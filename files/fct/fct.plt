set title "FCT (40Gbps, 1KB, 1000x, rtt:0.2ms)
set xlabel "Flow size (KB)"
set ylabel "Flow completion (/ideal):
set datafile separator ","
plot filename every ::1 using 2:($5/$4) title 'per-hop', filename every ::1 using 2:($6/$4) title 'rcp' 
set terminal postscript color portrait dashed enhanced
set size 1,0.5
replot
exit

# time,id,size,idealfct,perhopfct,rcpfct
