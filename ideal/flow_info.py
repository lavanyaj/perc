class FlowInfo:
    # startTimes[i] >= 0 is the start time of the ith flow
    # it's NaN if flow never starts
    # sizes[i] is the size of the ith flow
    def __init__(self, startTimes, sizes):
        self.startTimes = startTimes
        self.sizes = sizes
        pass
    pass
        
