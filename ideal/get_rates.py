"""
Get max min rates for flows.
Initialize with traffic matrix i.e., which flows on which link
Run with flow sizes.
"""

from google3.pyglib import app
from google3.pyglib import flags

FLAGS = flags.FLAGS

import numpy as np
from google3.pyglib import logging
#import logging

logging.set_verbosity(logging.WARN)

# rounding down increments to nearest bps, so 
# links won't be fully saturated possibly..
# call them saturated is spare capacity <= ZERO
# link capacity 100bps left, 200 unsaturated flows .. 
# each increases by 0.5bps rounded down to 0, oops.
# maybe rounding down to nearest micro-bps?
ZERO = 1
class GetRates:
    def __init__(self, tm):
        incidence = tm.incidence
        logging.info("incidence matrix has shape %s" % str(incidence.shape))
        assert(len(incidence.shape) == 2)
        self.numFlows = incidence.shape[0]
        self.numLinks = incidence.shape[1]
        self.incidence = incidence

        assert(tm.capacities.size == self.numLinks)
        capacities = tm.capacities.reshape(1, self.numLinks)
        logging.info("capacities shape: %s" % str(capacities.shape))
        self.capacities = capacities

        pass

    def getRates(self):
        numFlows = self.numFlows        
        
        capacities = self.capacities
        
        
        logging.info("incidence \n%s, \n shape %s" % (self.incidence, self.incidence.shape))

        incidence = self.incidence

        capacities = capacities.flatten()
        numLinks = capacities.size


        link_usage = np.zeros_like(capacities)
        flow_rates = np.zeros((1,numFlows)).flatten()
        logging.info("flow rates %s, shape %s" % (flow_rates, str(flow_rates.shape)))

        unsaturated_links = np.where(capacities > ZERO)[0] # initially
        logging.info("unsaturated_links (not all?) %s" % str(unsaturated_links))
        saturated_links =  np.where(capacities-link_usage <= ZERO)[0]
        logging.info("saturated_links (some?) %s" % str(saturated_links))
        # update unsaturated flows .. check no links are saturated
       
        logging.info("incidence matrix for just saturated links %s"% str(incidence[:,saturated_links]))
        logging.info("number of saturated links / flow (axis=1) %s"% str(np.sum(incidence[:,saturated_links],axis=1)))
        logging.info("number of flows/saturated links (axis=0) %s"% str(np.sum(incidence[:,saturated_links],axis=0)))

        cond1 = (np.sum(incidence[:,saturated_links],axis=1)==0)
        cond2 = (np.sum(incidence,axis=1) > 0)
        cond3 = cond1 & cond2
        # print "condition 1: %s, %s" % (cond1, np.where(cond1))
        # print "condition 2: %s, %s" % (cond2, np.where(cond2))
        # print "condition 1 and condition 2: %s, %s" % (cond3, np.where(cond3))

        unsaturated_flows = np.where(cond3)[0] # TODO check axis


        if (saturated_links.size == 0):
            logging.info("number of links / flow (axis=1) %s"% str(np.sum(incidence,axis=1)))
            logging.info("number of flows/link (axis=0) %s"% str(np.sum(incidence,axis=0)))
            unsaturated_flows = np.where(cond2)[0]
            pass

        logging.info("unsaturated flows (that don't go thru saturated links?) %s" % str(unsaturated_flows))

        # number of unsaturated flows per link
        logging.info("incidence for only unsaturated flows\n %s" %  incidence[unsaturated_flows,:])
        num_unsaturated_flows = np.sum( incidence[unsaturated_flows,:],axis=0) # sums numbers in same column/link TODO check axis
        if (unsaturated_flows.size == 0):
            num_unsaturated_flows = np.zeros_like(capacities)
            pass
        logging.info("number of unsaturated flows per link: %s" % str(num_unsaturated_flows))

        # filter out flows where demand 0?
        iterNo = 0
        while (unsaturated_flows.size > 0 and iterNo <= numLinks):
            logging.info("iterNo %d of %d" % (iterNo, numLinks))
            iterNo += 1
            reduced_capacities = capacities[unsaturated_links]
            reduced_link_usage = link_usage[unsaturated_links]
            reduced_num_flows = num_unsaturated_flows[unsaturated_links]

            logging.info("reduced capacities (shape %s)\n %s" % (str(reduced_capacities.shape), reduced_capacities))
            logging.info("reduced link_usage (shape %s)\n %s" % (str(reduced_link_usage.shape), reduced_link_usage))
            logging.info("reduced num_flows (shape %s)\n %s" % (str(reduced_num_flows.shape), reduced_num_flows))
            
            # try:
            #     increments = ((reduced_capacities-reduced_link_usage)/reduced_num_flows)
            # except RuntimeWarning, e:
            #     logging.info(e)
            #     pass
            
            # round to nearest bps?
            increments = np.round(((reduced_capacities-reduced_link_usage)/reduced_num_flows),6)
            
            logging.info("increments %s" % str(increments))
            min_increment = np.nanmin(increments)

            on_links = np.where(increments == min_increment)[0]
            logging.info("min increment: %s on links %s" % (min_increment, on_links[0]))

            assert(min_increment >= 0)
            flow_rates[unsaturated_flows] += min_increment
            logging.info("flow rates\n %s" % flow_rates)
            # undate link_usage
            link_usage = np.dot(flow_rates, incidence).flatten() # check dims
            logging.info("link_usage \n%s" % link_usage)

            # update unsaturated links
            unsaturated_links = np.where(capacities-link_usage > ZERO)[0]
            logging.info("unsaturated links %s" % unsaturated_links)

            saturated_links =  np.where(capacities-link_usage <= ZERO)[0]
            logging.info("incidence matrix for just saturated links %s"% str(incidence[:,saturated_links]))
            logging.info("number of saturated links / flow (axis=1) %s"% str(np.sum(incidence[:,saturated_links],axis=1)))
            #logging.info("number of flows/saturated links (axis=0) %s"% str(np.sum(incidence[:,saturated_links],axis=0)))

            cond1 = (np.sum(incidence[:,saturated_links],axis=1)==0)
            cond2 = (np.sum(incidence,axis=1) > 0)
            cond3 = cond1 & cond2

            unsaturated_flows = np.where(cond3)[0] # TODO check axis

            if (saturated_links.size == 0):
                logging.info("number of links / flow (axis=1) %s"% str(np.sum(incidence,axis=1)))
                logging.info("number of flows/link (axis=0) %s"% str(np.sum(incidence,axis=0)))
                unsaturated_flows = np.where(cond2)[0]
                pass

            logging.info("unsaturated_flows %s" % unsaturated_flows)

            # update number of unsaturated flows per link
            num_unsaturated_flows = np.sum(incidence[unsaturated_flows,:],axis=0) # sums numbers in same column/link TODO check axis
            if (unsaturated_flows.size == 0):
                num_unsaturated_flows = np.zeros_like(capacities)
                pass

            logging.info("number of unsaturated flows %s" % str(num_unsaturated_flows))
            
            pass

        
        assert(iterNo <= numLinks)
        logging.info("final rates: %s" % flow_rates)
        #flow_rates[unsaturated_flows] = 1.0
        return flow_rates

        
    pass
    

