import google3
import numpy as np
from google3.experimental.users.lav.flow_scheduler.ideal.get_rates import * 
from google3.experimental.users.lav.flow_scheduler.ideal.flow_info import *
from google3.experimental.users.lav.flow_scheduler.ideal.traffic_matrix import *
from google3.testing.pybase import googletest
#import unittest

 
#class TestGetRates(unittest.TestCase):
class TestGetRates(googletest.TestCase):
    # not really unit test ..
    def test_random(self):
        numFlows = 3
        numLinks = 2
        #print "%d flows, %d links" % (numFlows, numLinks)
        np.random.seed(1)
        for i in range(1000):
            # need incidence, capacities
            incidence = np.random.randint(2, size=(numFlows, numLinks))
            capacities = np.ones(numLinks)

            #print "sizes %s" % sizes
            #print "incidence %s" % incidence
            #print "capacities %s" % capacities
    
            tm = TrafficMatrix(incidence=incidence, capacities=capacities)
            gr = GetRates(tm)

            rates = gr.getRates()
            
            #print "rates %s" % rates
            #print "-------\n\n"

            # check it's feasible
            link_usage = np.dot(rates, incidence).flatten() # check dims
            self.assertEqual(link_usage.size, numLinks)
            for link in range(numLinks):
                self.assertLessEqual(link_usage[link], capacities[link])
                pass
            # check for each flow, there's one link that's saturated
            saturated_links = np.where(link_usage == capacities)[0]
            saturated_flows = np.where(np.sum(incidence[:, saturated_links],axis=1) > 0)[0]
            non_zero_flows = np.where(np.sum(incidence[:,],axis=1) > 0)[0]
    
            #print "saturated flows %s"  % saturated_flows
            #print "non zero flows (go through some link, have > 0 demand)%s" % non_zero_flows
            self.assertEqual(saturated_flows.size, non_zero_flows.size)
            if (saturated_flows.size > 0):
                self.assertItemsEqual(saturated_flows, non_zero_flows)
                pass
                #fi = FlowInfo(startTimes=startTimes, sizes=sizes)
                
                #sf = ScheduleFlows(tm, fi)
                #sf.scheduleFlows()
                pass
            pass
        pass
    pass


if __name__ == '__main__':
    googletest.main()
