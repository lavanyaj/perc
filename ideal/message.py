class Message:
    def __init__(self, sendTime, flowId, rate, other=""):
        self.sendTime = sendTime
        self.flowId = flowId
        self.rate = rate
        self.other = other
        pass

    def __eq__(self, other):
        return (isinstance(other, Message)\
                and self.sendTime == other.sendTime\
                and self.flowId == other.flowId\
                and self.rate == other.rate\
                and self.other == other.other)
        #return self.__dict__ == other.__dict__

    def __repr__(self):
        keys = sorted(self.__dict__)
        return "{"+ ", ".join(["%s:%s"%(key,self.__dict__[key])\
                         for key in keys]) + "}"

    pass
