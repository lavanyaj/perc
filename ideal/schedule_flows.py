"""
Given traffic matrix i.e., which flows on which links
And flow start times and sizes.
Schedule (max-min) rates for flows from start of first flow until all flows have finished.
"""

from google3.pyglib import app
from google3.pyglib import flags
from google3.pyglib import logging

FLAGS = flags.FLAGS

import numpy as np
from google3.experimental.users.lav.flow_scheduler.ideal.get_rates import * 
from google3.experimental.users.lav.flow_scheduler.ideal.flow_info import *
from google3.experimental.users.lav.flow_scheduler.ideal.message import *
from google3.experimental.users.lav.flow_scheduler.ideal.traffic_matrix import *


#TODO(): add units
logging.set_verbosity(logging.WARN)

TIME_GRAN = 10

class ScheduleFlows:
    def __init__(self, trafficMatrix, flowInfo):
        self.tm = trafficMatrix
        self.fi = flowInfo
        self.gr = GetRates(trafficMatrix)
        pass

    def scheduleFlows(self):
        startTimes = np.array(self.fi.startTimes)
        endTimes = np.empty_like(startTimes)
        endTimes.fill(np.nan)
        
        numFlows = startTimes.size
        
        demands = np.empty_like(startTimes)
        demands.fill(0)

        lastFlowEventTime = -1
        rates = np.empty_like(startTimes)
        rates.fill(0)

        sendingRates = np.array(rates)

        totalScheduled = np.zeros_like(demands)
        logging.info("starting sizes(b): %s" % self.fi.sizes)
        logging.info("starting rates(bps): %s" % rates)
        logging.info("starting startTimes(s): %s" % startTimes)
        logging.info("starting endTimes(s): %s" % endTimes)
        logging.info("--------------")

        activeFlows = np.where(rates > 0)[0]

        iterNo = 0

        # messages with sendTime, and rateChange or startFlows
        messages = []
        fct = np.ones(numFlows) * np.inf
        
        while True:
            logging.info("\niter %s" % iterNo)
            iterNo += 1
            newFlows = np.array([], dtype=int)
            numNewFlows = 0
            oldFlows = np.array([], dtype=int)
            numOldFlows = 0

            # get new flows that will start next
            minStartTime = np.nan
            if (np.nanmin(startTimes) >= 0):
                minStartTime = np.nanmin(startTimes)
                pass

            if (not np.isnan(minStartTime)):
                newFlows = np.where(startTimes == minStartTime)[0]
                numNewFlows = newFlows.size # TODO check dims
                pass
            logging.info("get flows that will start next")
            logging.info("minStartTime: %ss" % minStartTime)
            logging.info("flows that start at %ss: %s" % (minStartTime, newFlows))
            logging.info("--------------")

            # get old flows that will end next
            minEndTime = np.nan
            if (np.nanmin(endTimes) >= 0):
                minEndTime = np.nanmin(endTimes)
                pass

            if (not np.isnan(minEndTime)):
                oldFlows = np.where(endTimes == minEndTime)[0]
                numOldFlows = oldFlows.size
                pass

            #print "Checking after last event %s: old flows %s end at %s" % (lastFlowEventTime, oldFlows, minEndTime)

            logging.info("get flows that will end next")
            logging.info("minEndTime: %ss" % minEndTime)
            logging.info("flows that end at %ss: %s" % (minEndTime, oldFlows))
            logging.info("--------------")
        
            # TODO: must be a simpler way to ignore NaNs and get minimum
            if (np.isnan(minStartTime) and np.isnan(minEndTime)):
                logging.info("No flows left to start or end\n")
                break
            elif (not np.isnan(minStartTime) and not np.isnan(minEndTime)):
                flowEventTime = min(minStartTime, minEndTime) # TODO() check dims
                if (flowEventTime == minStartTime):
                    logging.info("Next event: %d flows start at time %.2fs\n" %\
                    (numNewFlows, minStartTime))
                    startTimes[newFlows] = np.nan
                    pass
                if (flowEventTime == minEndTime):
                    logging.info("Next event: %d flows end at time %.2fs\n" %\
                    (numOldFlows, minEndTime))
                    logging.info("%d flows ending at %ss: %s" % (numOldFlows, minEndTime, oldFlows))
                    endTimes[oldFlows] = np.nan
                    # remove flows from waterfilling
                    self.gr.incidence[oldFlows,:] = 0
                    pass
                pass
            elif (not np.isnan(minEndTime)):
                logging.info("Next event: %d flows end at time %.2fs\n" % (numOldFlows, minEndTime))
                flowEventTime = minEndTime
                fct[oldFlows] = flowEventTime
                logging.info("%d flows ending at %ss: %s" % (numOldFlows, minEndTime, oldFlows))
                #print "old flows %s end at %s" % (oldFlows, flowEventTime)
                endTimes[oldFlows] = np.nan
                self.gr.incidence[oldFlows,:] = 0
                pass
            elif (not np.isnan(minStartTime)):
                logging.info("Next event: %d flows start at time %.2fs\n" % (numNewFlows, minStartTime))
                flowEventTime = minStartTime
                startTimes[newFlows] = np.nan
                pass
            else:
                logging.info("Don't know what to do with min start time %ss and end time %ss" % (minStartTime, minEndTime))
                break
                
            logging.info("--------------")

            assert(not np.isnan(flowEventTime))
            # some flows will end, some will start at flowEventTime
            if (lastFlowEventTime == -1):
                lastFlowEventTime = flowEventTime
                pass
            delta = flowEventTime - lastFlowEventTime
            logging.info("New flow event at time: %ss" % flowEventTime)
            logging.info("Time since last flow event: %ss" % delta)
            logging.info("--------------")

            # update demands of existing flows at this time
            scheduled = sendingRates * delta
            totalScheduled += scheduled

            demands[activeFlows] -= scheduled[activeFlows]
            #assert((demands[activeFlows] >= 0).all())
            assert((demands >= -0.5).all())

            logging.info("total scheduled up to %ss: %sb" % (flowEventTime, totalScheduled))

            logging.info("update demands of active flows from %ss to %ss" %(lastFlowEventTime, flowEventTime))
            logging.info("active flows: %s" % activeFlows)
            logging.info("sendingRates(bps): %s" % sendingRates[activeFlows])
            logging.info("scheduled(b): %s" % scheduled[activeFlows])
            logging.info("new demands(b): %s" % demands[activeFlows])
            logging.info("--------------")

            # flows with demand 0 just exited, reset demands to NaN
            # exitedFlows = np.where(demands == 0)
            # demands[exitedFlows] = 0

            # logging.info("exited flows: %s" % exitedFlows
            # logging.info("demands: %s" % demands
            
            # update demands of new flows            
            logging.info("initialize demands of new flows")
            logging.info("new flows: %s" % newFlows)
            logging.info("sizes(b): %s" % self.fi.sizes[newFlows])
            demands[newFlows] = self.fi.sizes[newFlows]
            logging.info("demands(b): %s" % demands[newFlows])
            logging.info("--------------")

            # get new rates for all active flows   
            logging.info("get new rates for time %s to  .." % (flowEventTime))
            rates = self.gr.getRates()            
            logging.info("rates(bps): %s" % rates)
            logging.info("demands(b): %s" % demands)
            sendingRates = rates #np.minimum(rates, demands)
            logging.info("sendingRates(bps): %s" % sendingRates)
            activeFlows = np.where(sendingRates > 0)[0]
            logging.info("active flows during time %ss to .." % (flowEventTime))
            logging.info("activeFlows: %s" % activeFlows)
            logging.info("--------------")

            # update end times of only active flows based on these rates
            assert(np.isnan(startTimes[activeFlows]).all())
            # end times could be nan if flow just started/ hasn't started/ has finished
            # positive if already started but not yet finished
            logging.info("update end times of active flows")
            timeLeft = np.round(demands[activeFlows]/sendingRates[activeFlows],TIME_GRAN)
            assert(np.isfinite(timeLeft).all())

            endTimes[activeFlows] = flowEventTime + timeLeft
            assert(np.isfinite(endTimes[activeFlows]).all())

            logging.info("demand(b): %s" % demands[activeFlows])
            logging.info("sending rates(bps): %s" % sendingRates[activeFlows])
            logging.info("time left(s): %s" % timeLeft)
            logging.info("end time(s): %s" % endTimes[activeFlows])
            logging.info("--------------")


            numRates = rates.size
            for flow in range(numRates):
                if rates[flow] > 0:
                    messages.append(Message(sendTime=flowEventTime, flowId=flow, rate=rates[flow]))
                    pass
                pass

            lastFlowEventTime = flowEventTime
            pass


        logging.info("totalScheduled(b): %s" % totalScheduled)
        logging.info("sizes(b): %s" % self.fi.sizes)
        logging.info("demands(b): %s" % demands)
        logging.info("number of changes: %s" % iterNo)
        logging.info("number of messages: %d" % len(messages))

        if not (((totalScheduled - self.fi.sizes) < 0.5).all()): # some may be less cuz V
            buggyFlows = np.where(totalScheduled > self.fi.sizes)
            logging.warn("Scheduled more than flows size %s" % (totalScheduled[buggyFlows]-self.fi.sizes[buggyFlows]))
            pass

        if not ((demands >= -0.5).all()):
            buggyFlows = np.where(demands < 0)
            logging.warn("Some flows now have demand < 0 %s" % (demands[buggyFlows]))
            pass
                         
        return {'messages': messages, 'fct':fct}
    pass
