import google3

import numpy as np
from google3.experimental.users.lav.flow_scheduler.ideal.get_rates import * 
from google3.experimental.users.lav.flow_scheduler.ideal.schedule_flows import * 
from google3.experimental.users.lav.flow_scheduler.ideal.flow_info import *
from google3.experimental.users.lav.flow_scheduler.ideal.message import *
from google3.experimental.users.lav.flow_scheduler.ideal.traffic_matrix import *
from google3.testing.pybase import googletest


class TestScheduleFlows(googletest.TestCase):
    # not really unit test

    def test_two_flows_one_link(self):
        numFlows = 2
        numLinks = 1

        #print "%d flows, %d links" % (numFlows, numLinks)

        capacities = np.ones(numLinks) * 10.0e9
        sizes = np.ones(numFlows) * 10.0e9
        startTimes = np.ones(numFlows) * 0.0
        incidence = np.ones((numFlows,numLinks))

        #print "sizes %s" % sizes
        #print "start times %s" % startTimes
        #print "incidence %s" % incidence
        
        tm = TrafficMatrix(incidence=incidence, capacities=capacities)
        fi = FlowInfo(startTimes=startTimes, sizes=sizes)
        
        sf = ScheduleFlows(tm, fi)
        ret = sf.scheduleFlows()
        messages = ret['messages']
        fct = ret['fct']

        m0 = Message(sendTime=0.0,flowId=0,rate=5e9)
        m1 = Message(sendTime=0.0,flowId=1,rate=5e9)
        rightMessages = [m1,m0]

        #print "messages %d" % len(rightMessages) 
        #print str(messages)

        #print "right messages %d" % len(rightMessages)
        #print str(rightMessages)

        self.assertItemsEqual(messages, rightMessages)
        pass

    def test_one_long_flow_one_link(self):
        numFlows = 1
        numLinks = 1

        #print "%d flows, %d links" % (numFlows, numLinks)

        capacities = np.ones(numLinks) * 10.0e9
        sizes = np.ones(numFlows) * 10.0e9 * 5
        startTimes = np.ones(numFlows) * 0.0
        incidence = np.ones((numFlows,numLinks))

        #print "sizes %s" % sizes
        #print "start times %s" % startTimes
        #print "incidence %s" % incidence
        
        tm = TrafficMatrix(incidence=incidence, capacities=capacities)
        fi = FlowInfo(startTimes=startTimes, sizes=sizes)
        
        sf = ScheduleFlows(tm, fi)
        ret = sf.scheduleFlows()
        messages = ret['messages']
        fct = ret['fct']

        m0 = Message(sendTime=0.0,flowId=0,rate=10e9)
        rightMessages = [m0]

        #print "messages %d" % len(rightMessages) 
        #print str(messages)

        #print "right messages %d" % len(rightMessages)
        #print str(rightMessages)

        self.assertItemsEqual(messages, rightMessages)
        pass

    def test_random_flows_one_link(self):
        np.random.seed(100)
        numFlows = 100
        numLinks = 1

        capacities = np.ones(numLinks) * 10.0e9
        sizes = np.random.zipf(2, numFlows).astype(float)
        maximum_size = np.max(sizes)
        sizes /= maximum_size
        sizes *= 10.0e9

        startTimes = np.ones(numFlows) * 0.0
        incidence = np.ones((numFlows,numLinks))

        # print "zipf sizes"
        # print sizes
        # print "scaled down by %.9f" % maximum_size
        # print sizes
        # print "scaled up by 10.0e9"
        # print sizes
        # print "capacities %s" % capacities
        # print "incidence %s" % incidence
        # print "sizes %s" % sizes

        tm = TrafficMatrix(incidence=incidence, capacities=capacities)
        fi = FlowInfo(startTimes=startTimes, sizes=sizes)
        
        sf = ScheduleFlows(tm, fi)
        ret = sf.scheduleFlows()

        messages = ret['messages']
        fct = ret['fct']

        # print messages

        # for f in range(numFlows):
        #     print "%d, %d, %.9f" % (f, sizes[f], fct[f])
        #     pass

        #self.assertEquals(len(messages), numFlows)

        
        # incidence random 0/1 numFlowsxnumLinks matrix
        # sizes random flow sizes numFlows long array
        # startTimes random ..
 
        #print "%d flows, %d links" % (numFlows, numLinks)
        pass

    # def test_random(self):
    #     numFlows = 10
    #     numLinks = 4

    #     # incidence random 0/1 numFlowsxnumLinks matrix
    #     # sizes random flow sizes numFlows long array
    #     # startTimes random ..
 
    #     #print "%d flows, %d links" % (numFlows, numLinks)
        
    #     for i in range(0):
    #         sizes = np.random.zipf(2, numFlows).astype(float)
    #         startTimes = np.random.poisson(1, numFlows).astype(float)
    #         incidence = np.random.randint(2, size=(numFlows, numLinks))
    #         capacities = np.ones(numLinks)

    #         #print "sizes %s" % sizes
    #         #print "start times %s" % startTimes
    #         #print "incidence %s" % incidence
    
    #         tm = TrafficMatrix(incidence=incidence, capacities=capacities)
    #         fi = FlowInfo(startTimes=startTimes, sizes=sizes)

    #         sf = ScheduleFlows(tm, fi)
    #         sf.scheduleFlows()
    #         pass
    #     pass
    pass

if __name__ == '__main__':
    googletest.main()
