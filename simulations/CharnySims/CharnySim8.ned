//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package simulations.CharnySims;

import src.Generic.CharnyFlowGenerator;
import src.Charny.CharnyHost;
import src.Charny.CharnySwitch;
import src.Charny.CharnyNode;
;






//
// Mohammad's example: NxN switch with 2N links, all with capacity 1.
// Let delay be relatively small.
// Separate into N hosts, N receivers.
// 1st host sends to 1st receiver,
// 2nd host sends to 1..2 receiver,
// ...
// nth host sends to n receivers.
// Expected dependency chain is 2N links ...?

// Topology
// N hosts (--- 1 Gbps --)xN Switch (-- 1 Gbps --)xN N receivers
// so 2N+1 nodes total.

network CharnySim8
{
   	parameters:
   	    int N = default(10);
   	        
    types:
        channel Channel extends ned.DatarateChannel
        {          
        }
    submodules:

        charny[2*N+1]: CharnyNode; 
        flowGenerator3: CharnyFlowGenerator;
	// 8 and 9 are TORs, 10 is an AGG
    connections:
        
        for i=0..sizeof(charny)-1 {
            flowGenerator3.gate++ <--> charny[i].flowAlert;
        }

       for i=0..sizeof(charny)-2 { // connect all to 2Nth Charny
           charny[i].port++ <--> Channel <--> charny[2*N].port++;
        }

}
