//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package simulations.RcpSims;

import src.Rcp.FlowGenerator;
import src.Rcp.RcpHost;
import src.Rcp.RcpSwitch;
import src.Rcp.RcpNode;

import src.Rcp.QueueChannel;
import src.Rcp.QueueChannelManager;

import inet.linklayer.queue.DropTailQueue;

network SingleLink
{
    @display("bgb=639,387");
    types:
        channel Channel extends ned.DatarateChannel
        {
        }
    submodules:
        flowGenerator: FlowGenerator;
        rcp[2]: RcpNode;
    connections:
        for i=0..sizeof(rcp)-1 {
            flowGenerator.gate++ <--> rcp[i].flowAlert;
        }
        rcp[0].port++ <--> Channel <--> rcp[1].port++;
}

// Topology
// All links have same capacity
// 0				  4
//		2 ------  3
// 1				  5
network MultiNode //From the white board. 
{
    @display("bgb=533,242;bgl=2");
    types:
        channel Channel extends ned.DatarateChannel
        {
        }
    submodules:
        flowGenerator: FlowGenerator {
            @display("p=271,124");
        }
        rcp[6]: RcpNode;
    connections:
        for i=0..sizeof(rcp)-1 {
            flowGenerator.gate++ <--> rcp[i].flowAlert;
        }
        rcp[0].port++ <--> Channel <--> rcp[2].port++;
        rcp[1].port++ <--> Channel <--> rcp[2].port++;
        rcp[2].port++ <--> Channel <--> rcp[3].port++; // bottleneck
        rcp[3].port++ <--> Channel <--> rcp[4].port++;
        rcp[3].port++ <--> Channel <--> rcp[5].port++;
}

network RcpSim5
{
    @display("bgb=639,387");
    
    types:
        channel Channel extends ned.DatarateChannel
        {          
        }
    submodules:

        rcp[4]: RcpNode;
        flowGenerator: FlowGenerator;

    connections:
        for i=0..sizeof(rcp)-1 {
            flowGenerator.gate++ <--> rcp[i].flowAlert;
        }
        rcp[0].port++ <--> Channel <--> rcp[1].port++;
        rcp[1].port++ <--> Channel <--> rcp[2].port++;
        rcp[1].port++ <--> Channel <--> rcp[3].port++;
}
