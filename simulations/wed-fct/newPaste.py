from itertools import izip_longest
import sys

annot = sys.argv[1]
filenames = sys.argv[2:]


firstLines  = []
seen = {}

for filename in filenames:
    with open(filename, 'r') as f:
        first_line = f.readline().rstrip()
        stats = first_line.split(",")
        if (len(stats) != 2):
            print "XX"
            print filename, first_line
            firstLines.append("xx")
            pass
        else:
            if (stats[1] in seen):
                seen[stats[1]] += 1
                pass
            else:
                seen[stats[1]] = 1
                pass
            parts = stats[1].split(".")
            assert(len(parts) == 3)
            name = "%s_%d.%s.%s"%(parts[0], seen[stats[1]], parts[1], parts[2])
            newFirstLine = "time,%s"%name
            firstLines.append(newFirstLine)
        pass
    f.close()
    pass

#print "first lines"
#print firstLines

sorted_fileindices = sorted([f for f in range(len(filenames))], key=lambda f: firstLines[f])
#print "sorted file indices"
#print sorted_fileindices


sortedFirstLines = [firstLines[i] for i in sorted_fileindices]
    
sorted_filenames = [filenames[f] for f in sorted_fileindices]
#print sorted_filenames


files = [open(filename,'r') for filename in sorted_filenames]

index = 0
for lines in izip_longest(*files):
    if (index == 0):
        print ",".join([line.rstrip() for line in sortedFirstLines if line is not None])        
        pass
    else:
        print ",".join([line.rstrip() for line in lines if line is not None])
        pass
    index += 1
    pass
