#include "CharnyHost.h"
#include <assert.h>

namespace rcp {
Define_Module(CharnyHost);

#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 if (false) std::cout

GenericControlPacket *CharnyHost::newProtocolSpecificControlPacket(const char* pktname, int flowId) {
    CharnyPacket* pkt = new CharnyPacket(pktname);
    GenericControlHostSessionState &session = pIActiveSessions[flowId];
    CharnyHostSessionState &sMSession = sMActiveSessions[flowId];
    if (sMSession.stampedRate == -1) {
            // first packet, haven't received any incoming stamped rates or marked bit yet
            pkt->setStampedRate(session.demand);
            if (session.demand < INFINITE_RATE)
                pkt->setMarkedBit(true);
            else pkt->setMarkedBit(false);
        } else {
            pkt->setStampedRate(sMSession.stampedRate);
            pkt->setMarkedBit(sMSession.markedBit);
        }

    return pkt;
}

void CharnyHost::protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    CharnyHostSessionState session(flowId);
    sMActiveSessions[flowId] = session;
}


void CharnyHost::sourceReceivePacketInfiniteDemand(const GenericControlPacket *msg) {
    const CharnyPacket *pkt = check_and_cast<CharnyPacket *>(msg);
    emitSignal(pkt->getFlowId(), "StampedRate", pkt->getStampedRate());
    //emit(sourceReceivePacketSignal, pkt->getStampedRate());
    EV2 << "At time "<< simTime() << ", emitting sourceReceivePacketSignal: "\
            << pkt->getStampedRate() << " marked(" << pkt->getMarkedBit() << ")\n";

    GenericControlHostSessionState &session = pIActiveSessions[pkt->getFlowId()];
    CharnyHostSessionState &sMSession = sMActiveSessions[pkt->getFlowId()];
    session.numRecvd++;

    if (pkt->getPktType() == CHARNY_FIN) {
         EV3 << "Source received FIN for flow " << pkt->getFlowId() << ", do nothing.\n";
         // since switches don't really add any info. to packets, just delete flow from state.
         return;
     }

    // incoming stamped rate, maybe add one for outgoing stamped rate

   // NOTE THIS IS THE VERSION WHERE WE ASSUME DEMANDS ARE INFINITE

    sMSession.markedBit = false;
     if (pkt->getMarkedBit() == false) {
        sMSession.stampedRate = session.demand;
        EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                << ", sets packet.rate to "<< session.demand\
                << " because packet was not marked, so rate = demand.\n";

    } else {
        // Packet passed at least one link whose advertised rate
        // was equal to the packets's current stamped rate
        // so obey this rate.
      sMSession.stampedRate = pkt->getStampedRate();
      sMSession.markedBit = false; // not restricted by host
      EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                      << ", sets packet.rate to stamped rate "<< pkt->getStampedRate()\
                      << " and marked bit to false "\
                      << " because packet was marked.\n";
    }


    updateTransmissionRate(msg);
}

void CharnyHost::sourceReceivePacketGeneral(const GenericControlPacket *msg) {
    if (par("hostFiniteDemandMode").boolValue()) {
        sourceReceivePacketFiniteDemand(msg);
    } else {
        sourceReceivePacketInfiniteDemand(msg);
    }
}
// DEMAND'S NOT ALWAYS INFINITE
void CharnyHost::sourceReceivePacketFiniteDemand(const GenericControlPacket *msg) {
    const CharnyPacket *pkt = check_and_cast<CharnyPacket *>(msg);
    emitSignal(pkt->getFlowId(), "StampedRate", pkt->getStampedRate());
    //emit(sourceReceivePacketSignal, pkt->getStampedRate());
    EV2 << "At time "<< simTime() << ", emitting sourceReceivePacketSignal: "\
            << pkt->getStampedRate() << " marked(" << pkt->getMarkedBit() << ")\n";

    GenericControlHostSessionState &session = pIActiveSessions[pkt->getFlowId()];
    CharnyHostSessionState &sMSession = sMActiveSessions[pkt->getFlowId()];
    // incoming stamped rate, maybe add one for outgoing stamped rate


    session.numRecvd++;

    if (rateGreaterThan(pkt->getStampedRate(), session.demand)) { // must be that demand had decreased
        sMSession.stampedRate = session.demand;
        sMSession.markedBit = true;
        EV2 << "Packet stamped rate is more than session demand, demand must have decreased.\n";

        EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                       << ", sets packet.rate to "<< session.demand\
                       << " and marks packet "
                       << " because incoming packet rate > demand.\n";

    } else if (pkt->getMarkedBit() == false) {
        EV2 << "Packet return unmarked.\n";
        sMSession.stampedRate = session.demand;
        if (session.demand < INFINITE_RATE) {
            sMSession.markedBit = true;

            EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                                   << ", sets packet.rate to "<< session.demand\
                                   << " and marks packet "
                                   << " because incoming packet unmarked, rate <= demand < INF.\n";
        }
        else {
            sMSession.markedBit = false;

            EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                                               << ", sets packet.rate to "<< session.demand\
                                               << " and marks packet "
                                               << " because incoming packet unmarked, rate <= demand = INF.\n";
        }
    } else {
        // Packet passed at least one link whose advertised rate
        // was equal to the packets's current stamped rate
        // so obey this rate.
      sMSession.stampedRate = pkt->getStampedRate();
      sMSession.markedBit = false;

      EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                                         << ", sets packet.rate to "<< pkt->getStampedRate()\
                                         << " and unmarks packet "
                                         << " because incoming packet marked, rate <= demand.\n";
    }


    updateTransmissionRate(msg);
}

void CharnyHost::updateTransmissionRate(const GenericControlPacket *msg) {
    const CharnyPacket *pkt = check_and_cast<CharnyPacket *>(msg);
    GenericControlHostSessionState& session = pIActiveSessions.at(pkt->getFlowId());
    CharnyHostSessionState& sMSession = sMActiveSessions.at(pkt->getFlowId());

    // UPDATE ACTUAL TRANSMISSION RATE

    struct MarkedState {
        bool markedBit;
        double stampedRate;
        MarkedState(bool markedBit, double stampedRate) : markedBit(markedBit), stampedRate(stampedRate) {}
    } newIncoming(pkt->getMarkedBit(), pkt->getStampedRate());
    EV2 << "flow # " << session.flowId << ", incoming pkt # " << session.numRecvd << "\n";
    // How do data rates follow stamped rates? Some subset, in order, slightly delayed. I think.
    if (par("conservative").doubleValue() > 0) {
        double numRtts = par("conservative").doubleValue();
        EV2 << "Being conservative.";
        if (newIncoming.markedBit and ! rateEqual(newIncoming.stampedRate, session.dataRate)) { // marked and new rate
          EV2 << "incoming packet was marked, ";
          if (rateGreaterThan(newIncoming.stampedRate, session.dataRate)) {
              EV2 << "incoming rate is more than actual rate " << session.dataRate << ", ";
              if (session.sendAtPktNum >= session.numRecvd) { // rate increase already scheduled
                  if (rateGreaterThan(newIncoming.stampedRate, session.sendDataRate)) { // stick to schedule
                      EV2 << "incoming rate "<< newIncoming.stampedRate\
                              << "is more than existing rate increase "\
                              << session.sendDataRate << " scheduled (at RTT "\
                              << session.sendAtPktNum << " vs now " << session.numRecvd << ")"\
                              << " reschedule to "<< numRtts <<" RTTs from now at " << session.numRecvd+numRtts << ".\n";
                      session.sendAtPktNum = session.numRecvd+numRtts;
                      session.sendDataRate = newIncoming.stampedRate;
                    } else {
                        EV2 << "incoming rate "<< newIncoming.stampedRate\
                        << " is not more than existing rate increase "\
                        << session.sendDataRate << " scheduled (at RTT "\
                        << session.sendAtPktNum << " vs now " << session.numRecvd << ")"\
                        << " stick to schedule.\n";
                      session.sendDataRate = newIncoming.stampedRate;
                    }
                } else { // reschedule
                    EV2 << "No rate increase scheduled yet, schedule new rate increase to incoming rate "\
                            << newIncoming.stampedRate << " "<< numRtts <<" RTTs from now at "\
                            << session.numRecvd+numRtts << ".\n";
                    session.sendAtPktNum = session.numRecvd+numRtts;
                    session.sendDataRate = newIncoming.stampedRate;
                }
          } else { // schedule rate decrease
              EV2 << "incoming rate is less than actual rate " << session.dataRate\
                      << ", schedule rate decrease now  "  << session.numRecvd << ".\n";
              session.sendAtPktNum = session.numRecvd;
              session.sendDataRate = newIncoming.stampedRate;
          }
        } else if (!newIncoming.markedBit) {
            EV2 << "incoming packet was not marked.\n";
        } else if (rateEqual(newIncoming.stampedRate, session.dataRate)) {
            EV2 << "incoming packet was marked, but rate is same as actual rate " << session.dataRate << ".\n";
        }
    }

    if (par("conservative").doubleValue() == 0) {
        EV2 << "Being aggressive.\n";
        if (newIncoming.markedBit) {
            if (! rateEqual(newIncoming.stampedRate, session.dataRate)) {
                EV2 << "incoming packet was marked, ";
                EV2 << "incoming rate "<< newIncoming.stampedRate\
                        <<" is "\
                        << (rateGreaterThan(newIncoming.stampedRate, session.dataRate) ? "more" : "less")\
                        << " than actual rate " << session.dataRate << ", ";
                EV2 << "schedule rate change now  "  << session.numRecvd << ".\n";
                session.sendAtPktNum = session.numRecvd;
                session.sendDataRate = newIncoming.stampedRate;
            }
        }
    }

    if (session.sendAtPktNum == session.numRecvd) {
        //assert(session.sendDataRate > 0);
        sendFlowMsg(session.flowId, session.sendDataRate);
        //session.sendAtPktNum = -1;
    }

}
bool CharnyHost::destinationReceivePacket(const GenericControlPacket *msg) {
    const CharnyPacket *pkt = check_and_cast<CharnyPacket *>(msg);
    CharnyHostSessionState &session = sMActiveSessions[pkt->getFlowId()];
    session.count += 1;
    if (session.count == K) {
        EV2 << "Destination received packet of flow # "\
                << pkt->getFlowId() << " with rate " << pkt->getStampedRate() << ".\n";
        session.stampedRate = pkt->getStampedRate();
        session.markedBit = pkt->getMarkedBit();
        session.count = 0;
        return true;
    } else {
        return false;
    }
}

void CharnyHost::addProtocolSpecificActiveSession(const GenericControlPacket *pkt) {
    int flowId = pkt->getFlowId();
    CharnyHostSessionState session(flowId);
    sMActiveSessions[flowId] = session;
}; // at destination

void CharnyHost::egressAction(GenericControlPacket *msg) {

    /* NOT SURE IF IT'S AS SIMPLE AS ADDING A NEW SWITCH
     * WHEN SWITCH'S CAPACITY CHANGES, DO WE HAVE TO
     * UPDATE FLOW MARKED BITS AND ADVERTISED RATES? HOW?
     */
    if (par("hostFiniteDemandMode").boolValue()) {
        // No extra link action.
        return;
    }

    CharnyPacket *pkt = check_and_cast<CharnyPacket *>(msg);
    int flowId = pkt->getFlowId();

    double oldCapacity = sMActiveSessions.at(pkt->getFlowId()).linkState.linkCapacity;
    double newCapacity = pIActiveSessions[flowId].demand;
    // starts with infinite rate
    if (oldCapacity != newCapacity and newCapacity < INFINITE_RATE) {
        EV2 << "Updated host's demand/link capacity for flow # " << pkt->getFlowId() << " from "\
                << oldCapacity << " to " << newCapacity << ".\n";
        sMActiveSessions.at(pkt->getFlowId()).linkState.linkCapacity = pIActiveSessions[flowId].demand;
        sMActiveSessions.at(pkt->getFlowId()).linkState.calculateAdvRate();
    }

    sMActiveSessions.at(pkt->getFlowId()).linkState.linkAction(pkt);

}

void CharnyHost::removeProtocolSpecificActiveSession(int flowId) {\
    sMActiveSessions.erase(flowId);
}; // on FIN

#endif // EV2
#endif // EV3
}; // namespace
