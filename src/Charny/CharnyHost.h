//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __CHARNY_HOST_H
#define __CHARNY_HOST_H

#include <omnetpp.h>
#include "util.h"
#include "GenericControlHost.h"
#include "CharnySwitch.h"
#include "charny_m.h"
#include <map>
#include <utility>

namespace rcp {

/**
 * Implements the Txc simple module. See the NED file for more information.
 */


class CharnyHostSessionState {
public:
    int flowId;
    bool markedBit;
    double stampedRate;
    int count;
    CharnyLinkState linkState;
    CharnyHostSessionState(int fid) : flowId(fid), markedBit(false),\
            stampedRate(-1), count(0), linkState(std::to_string(fid)) {
        linkState.linkCapacity = 100000000000; // TODO: change to some constant .. and don't binary search on it!
    }
    CharnyHostSessionState(): flowId(-1), markedBit(false),\
            stampedRate(-1), count(0), linkState(std::to_string(-1)) {
        linkState.linkCapacity = 100000000000;
    }
};



class CharnyHost : public GenericControlHost
{

  private:
     // Charny state
    typedef std::map<int, CharnyHostSessionState> SessionTable;
    SessionTable sMActiveSessions;

  protected:
    virtual GenericControlPacket *newProtocolSpecificControlPacket(const char* pktname, int flowId);

    virtual void sourceReceivePacketGeneral(const GenericControlPacket *pkt); // demand could be FINITE, changing
    void sourceReceivePacketFiniteDemand(const GenericControlPacket *pkt);
    void sourceReceivePacketInfiniteDemand(const GenericControlPacket *pkt);

    virtual bool destinationReceivePacket(const GenericControlPacket *pkt);
    virtual void updateTransmissionRate(const GenericControlPacket *pkt);
    virtual void egressAction(GenericControlPacket *msg);
    virtual void protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg); // at source
    virtual void addProtocolSpecificActiveSession(const GenericControlPacket *pkt); // at destination
    virtual void removeProtocolSpecificActiveSession(int flowId); // on FIN

};


}; // namespace

#endif
