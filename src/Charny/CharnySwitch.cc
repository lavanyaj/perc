#include "CharnySwitch.h"

#include <assert.h>

namespace rcp {

Define_Module(CharnySwitch);

#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 EV
#ifndef EV1
#define EV1 if (false) std::cout

CharnyLinkState::CharnyLinkState(std::string idStr):
        linkCapacity(-1), advertisedRate(-1), numForwardSessions(0),\
                numFeedbackSessions(0), idStr(idStr), eagerSwitchMode(false) {


            WATCH(advertisedRate);
            WATCH(numForwardSessions);
            WATCH(numFeedbackSessions);

        }

CharnyLinkState::~CharnyLinkState() {
    activeSessions.clear();
}


CharnySwitch::CharnySwitch() {
}

CharnySwitch::~CharnySwitch() {



}

void CharnySwitch::finish() {
    EV << "Clearing Link State for Charny.\n";
    for (auto ls : cltable) {
        delete ls.second;
        ls.second = NULL;
    }
    cltable.clear();
    GenericSwitch::finish();
}
void CharnySwitch::initialize()
{
    GenericSwitch::initialize();
   for (auto linkState: ltable) {
        cltable[linkState.first] = new CharnyLinkState(linkState.second->getId());
        EV << "Added new Charny link state for link " << linkState.first\
                << ".\n";
        cltable[linkState.first]->linkCapacity = linkState.second->linkCapacity;
        cltable[linkState.first]->advertisedRate = linkState.second->linkCapacity;
        cltable[linkState.first]->eagerSwitchMode = par("eagerSwitchMode").boolValue();;
        cltable[linkState.first]->header_size_ = par("headerSize").doubleValue();
        cltable[linkState.first]->payload_size_ = par("payloadSize").doubleValue();
        cltable[linkState.first]->minRtt = par("minRtt").doubleValue();
        cltable[linkState.first]->maxSessionsPerRtt = par("maxSessionsPerRtt").doubleValue();


   }
}

cMessage* CharnySwitch::protocolSpecificProcessEgress(int outGateIndex, cMessage* msg) {
    EV << "Calling cltable.at("<< outGateIndex << ")->processEgress\n";
    return cltable.at(outGateIndex)->processEgress(check_and_cast<CharnyPacket *>(msg));

}

// actually ingate index etc.
cMessage* CharnySwitch::protocolSpecificProcessIngress(int outGateIndex, cMessage* msg) {
    return cltable[outGateIndex]->processIngress(check_and_cast<CharnyPacket *>(msg));

}

CharnyPacket *CharnyLinkState::processIngress(CharnyPacket *pkt)
{
     return pkt;
     //->dup();
}

CharnyPacket *CharnyLinkState::processEgress(CharnyPacket *pkt)
{
    if (pkt->getPktType() == CHARNY_HELLO || pkt->getPktType() == CHARNY_FIN) {
        EV << "Link action HELLO/FIN\n";
        linkAction(pkt);
    } else {
        EV << "Link action: Unknown packet type.\n";
    }
    return pkt;
    //->dup();
}


double CharnyLinkState::calculateAdvRateAndRevert() {
 double oldRate = advertisedRate;
 double firstRate = calculateRate();
 std::map<int, bool> unmarked;
 for (auto& session : activeSessions) {
     if (rateGreaterThan(session.second.recordedRate, firstRate)) {
         unmarked[session.first] = session.second.marked;
         session.second.marked = false;
     }
 }
 double secondRate = calculateRate();
 advertisedRate = secondRate;
 EV2 << "Switch " << this->getId() << " updated rate from "\
         << oldRate << " -> " << firstRate << " -> " << secondRate << ".\n";

 EV2 << "Reverting to original marked set and rate.\n";
 for (auto& session : unmarked)
     activeSessions[session.first].marked = session.second;
 advertisedRate = oldRate;
 return oldRate;
 }


void CharnyLinkState::calculateAdvRate() {
 double oldRate = advertisedRate;
 double firstRate = calculateRate();
 for (auto& session : activeSessions) {
     if (rateGreaterThan(session.second.recordedRate, firstRate)) {
         session.second.marked = false;
     }
 }
 double secondRate = calculateRate();
 advertisedRate = secondRate;
 EV2 << "Switch " << this->getId() << " updated rate from "\
         << oldRate << " -> " << firstRate << " -> " << secondRate << ".\n";
 }

double CharnyLinkState::calculateRate() {
    double linkCapacityForControl = (header_size_*8/minRtt * (maxSessionsPerRtt));
    double linkCapacityForData = linkCapacity - linkCapacityForControl;


    double rate = 0;
    int totalSessions = numForwardSessions + numFeedbackSessions;
    double totalMarkedRate = 0;
    double maximumMarkedRate = 0;
    int numMarkedSessions = 0;
    for (const auto& session : activeSessions) {
        if (session.second.marked) {
            numMarkedSessions++;
            totalMarkedRate += session.second.recordedRate;
            if (rateGreaterThan(session.second.recordedRate, maximumMarkedRate)) {
                maximumMarkedRate = session.second.recordedRate;
            }
        }
    }

    EV2 << numMarkedSessions << " marked sessions out of " << activeSessions.size()\
            << " with total marked rate " << totalMarkedRate << " bps.\n";

    if (totalSessions == 0) {
        rate = linkCapacityForData;
    } else if (totalSessions == numMarkedSessions) {
        rate = linkCapacityForData - totalMarkedRate + maximumMarkedRate;
        EV2 << "All sessions are marked so, advertise "\
                "extra capacity " << (linkCapacityForData - totalMarkedRate)\
                << " plus fair share " << maximumMarkedRate \
                << " i.e., "\
                << rate << ".\n";
    } else {
        double extraCapacity = linkCapacityForData - totalMarkedRate;
        double numUnmarkedSessions = totalSessions - numMarkedSessions;
        rate = extraCapacity/numUnmarkedSessions;
        EV2 << "Splitting extra capacity " << extraCapacity << " among "
                << numUnmarkedSessions << " unmarked sessions, so rate is "
                << rate << ".\n";
    }
    return rate;
    }

double CharnyLinkState::getMaxRate() {
    double maximumMarkedRate = 0;
    for (const auto& session : activeSessions) {
            if (session.second.marked) {
                 if (rateGreaterThan(session.second.recordedRate, maximumMarkedRate)) {
                    maximumMarkedRate = session.second.recordedRate;
                }
            }
        }
    return maximumMarkedRate;
}

double CharnyLinkState::getTotalRate() {
    double totalMarkedRate = 0;
    for (const auto& session : activeSessions) {
            if (session.second.marked) {
              totalMarkedRate += session.second.recordedRate;

            }
        }
    return totalMarkedRate;
}

// HELLO AND FIN PACKETS ON HIGH PRIO QUEUE, DATA PACKETS ACCORDING TO RATE
// DECOUPLE CONTROL AND DATA
void CharnyLinkState::linkUpdateSessionExit(const CharnyPacket *pkt) {
    if (pkt->getIsForward()) {
        numForwardSessions -= 1;
    } else {
        numFeedbackSessions -= 1;
    }
    activeSessions.erase(pkt->getFlowId());
    calculateAdvRate();
}

// now this is not just like UpdateKnownSession..
void CharnyLinkState::linkUpdateNewSession(CharnyPacket *pkt) {

    int flowId = pkt->getFlowId();
    int numPackets = (pkt->getIsForward()) + K * (1 - pkt->getIsForward());
    if (pkt->getIsForward()) numForwardSessions += 1;
    else numFeedbackSessions += 1;


    double marked = false;
    CharnySwitchSessionState session(0, numPackets, marked, flowId);
    activeSessions[flowId] = session;

    double wouldBeRate = calculateAdvRateAndRevert(); // rate including this new session, but unmarked .. don't want to change marked set yet.

    if (rateGreaterThan(pkt->getStampedRate(), wouldBeRate)) {
        calculateAdvRate();
        assert(!activeSessions[flowId].marked); // u1 .. this and others higher, unmarked, new u2 >= u1
        pkt->setStampedRate(advertisedRate);
        pkt->setMarkedBit(true);
        activeSessions[flowId].recordedRate = pkt->getStampedRate();

        EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                " for flow " << pkt->getFlowId() <<\
                ", records demand[flow] as " << activeSessions[flowId].recordedRate
                << " because that's packet.rate...\n";

      } else if (eagerSwitchMode and pkt->getMarkedBit() and rateLessThan(pkt->getStampedRate(), advertisedRate)) {
          // session bottle-necked elsewhere, recompute fair share, should be higher
          EV2 << "Eagerly marked session # " << flowId << " with recorded rate " << pkt->getStampedRate() << ".\n";

          activeSessions[flowId].marked = true;
          activeSessions[flowId].recordedRate = pkt->getStampedRate();

          EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                          " for flow " << pkt->getFlowId() <<\
                          ", records demand[flow] as " << activeSessions[flowId].recordedRate
                          << " because that's packet.rate...\n";

          calculateAdvRate(); // rate including this new session, but marked with incoming rate

          assert(rateLessThanOrEqual(pkt->getStampedRate(), advertisedRate)); // fair share only increased
          assert(activeSessions[flowId].marked);
      } else {
         calculateAdvRate();
          // leave packet as-is but record rate
         activeSessions[flowId].recordedRate = pkt->getStampedRate();

         EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                         " for flow " << pkt->getFlowId() <<\
                         ", records demand[flow] as " << activeSessions[flowId].recordedRate
                         << " because that's packet.rate...\n";
      }

}

void CharnyLinkState::linkUpdateKnownSession(CharnyPacket *pkt) {
    if (rateGreaterThanOrEqual(pkt->getStampedRate(), advertisedRate)) {
        pkt->setStampedRate(advertisedRate); // already accounts for the session, but possibly as unmarked
        pkt->setMarkedBit(true);
    }
    CharnySwitchSessionState& session = activeSessions[pkt->getFlowId()];
    session.numPackets += (pkt->getIsForward()) + K * (1 - pkt->getIsForward());
    assert(rateLessThanOrEqual(pkt->getStampedRate(), advertisedRate));
    if (rateLessThanOrEqual(pkt->getStampedRate(), advertisedRate)) {
        session.marked = true;
    }
    session.recordedRate = pkt->getStampedRate();
    EV2 << "Mark session " << pkt->getFlowId() << "'s recorded rate as "\
            << session.recordedRate << ".\n";
    calculateAdvRate();
  }

void CharnyLinkState::linkAction(CharnyPacket *pkt){
    if(!pkt->getIsForward()) {
        EV2 << "No link action on feedback packet of session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        return;
    }

    if (pkt->getIsExit()) {
        EV2 << ((activeSessions.count(pkt->getFlowId()) == 1) ? "Known" : "New")\
                << " session #" << pkt->getFlowId() << " exiting, at Switch " << this->getId() << ".\n";
        linkUpdateSessionExit(pkt);
        return;
    }

    if (activeSessions.count(pkt->getFlowId()) == 0) {
        EV2 << "New session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        linkUpdateNewSession(pkt);
    } else {
        EV2 << "Known session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        linkUpdateKnownSession(pkt);
    }
}



#endif // EV1
#endif // EV2
#endif // EV3
}; // namespace
