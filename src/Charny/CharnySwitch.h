//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __CHARNY_SWITCH_H
#define __CHARNY_SWITCH_H

#include <omnetpp.h>
#include "util.h"
#include "GenericSwitch.h"
#include <map>

namespace rcp {


class CharnySwitchSessionState {
public:
    double recordedRate;
    int numPackets;
    bool marked;
    int flowId;

    CharnySwitchSessionState(double rr, int np, bool m, int fid) :
        recordedRate(rr), numPackets(np), marked(m), flowId(fid) {
    }
    CharnySwitchSessionState() :
            recordedRate(0), numPackets(0), marked(false), flowId(-1) {
        }
};

class CharnyLinkState {
    public:
    std::string idStr;
    std::string getId() {return idStr;}
    //cMessage *dequeueMessage;

    // Charny state
    double linkCapacity;
    typedef std::map<int, CharnySwitchSessionState> SessionTable;
    SessionTable activeSessions;
    double advertisedRate;
    int numForwardSessions;
    int numFeedbackSessions;
    bool eagerSwitchMode;
    double header_size_;
    double payload_size_;
    double minRtt;
    double maxSessionsPerRtt;

    CharnyLinkState(std::string idStr);
    ~CharnyLinkState();

    CharnyPacket *processIngress(CharnyPacket *pkt);
    CharnyPacket *processEgress(CharnyPacket *pkt);

    void calculateAdvRate();
    double calculateAdvRateAndRevert();
    double calculateRate();
    double getMaxRate();
    double getTotalRate();
    void linkUpdateSessionExit(const CharnyPacket *pkt);
    void linkUpdateNewSession(CharnyPacket *pkt);
    void linkUpdateKnownSession(CharnyPacket *pkt);
    void linkAction(CharnyPacket *pkt);
};



/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class CharnySwitch : public GenericSwitch
{

private:
    typedef std::map<int,CharnyLinkState*> CharnyLinkStateTable; // output queue # -> link state
    CharnyLinkStateTable cltable;
protected:
    void initialize();
    void finish();

    cMessage* protocolSpecificProcessIngress(int outGateIndex, cMessage* msg);
    cMessage* protocolSpecificProcessEgress(int outGateIndex, cMessage* msg);

public:
    CharnySwitch();
    ~CharnySwitch();
};

}; // namespace

#endif
