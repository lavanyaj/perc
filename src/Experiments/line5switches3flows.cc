//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>

namespace rcp {


void CharnyFlowGenerator::initializeCharnySim2() {
    // 0 to 2 (red)
    // 1 to 3 (blue)
    // 2 to 4 (green)
    double endTime = par("endTime").doubleValue() * longestRtt;
    EV << "Running for " << par("endTime").doubleValue()\
            << "RTTs (1 RTT=" << longestRtt << "s) i.e.,"\
            << endTime << "s.";

    FlowInfo f(0, pairInt(0,1), par("duration"), par("initialRate"), 0.0, endTime);

    std::vector<FlowInfo> flows(3, f);
    int i = 0; for (auto& flow: flows) flow.flowId = i++;
    flows[0].ends = pairInt(0,2);
    flows[1].ends = pairInt(1,3);
    flows[2].ends = pairInt(2, 4);
    for (auto& f: flows) allFlows[f.flowId] = f;
    getMaxMinRates(allFlows);
    sendFlowMsgs(flows);
}

} /* namespace rcp */
