//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>

namespace rcp {

//
// Mohammad's example: NxN switch with 2N links, all with capacity 1.
// Let delay be relatively small.
// Separate into N hosts, N receivers.
// 1st host sends to 1st receiver,
// 2nd host sends to 1..2 receiver,
// ...
// nth host sends to n receivers.
// Expected dependency chain is 2N links ...?
void CharnyFlowGenerator::initializeCharnySim8() {
    // check all parameters are usable
    std::vector<FlowInfo> flows;
    assert(par("duration").doubleValue() == -1);
    assert(par("N").doubleValue() != -1);

    double endTime = getLongestRtt() * par("endTime").doubleValue();
    int N = par("N");

    int flowId = 0;
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < i; j++) {
            FlowInfo f(flowId++, std::pair<int, int>(i, N+j),
                     par("duration"), par("initialRate"), 0.0, endTime);
            flows.push_back(f);
        }
    }

    for (auto& f: flows) allFlows[f.flowId] = f;
}

} /* namespace rcp */
