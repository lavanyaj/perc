//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>

namespace rcp {


void CharnyFlowGenerator::initializeCharnySim11() {
    // check all parameters are usable
    assert(par("backgroundRate").doubleValue() != -1);
    assert(par("numBackgroundFlows").doubleValue() != -1);
    assert(par("endTime").doubleValue() != -1);

    assert(par("longDuration").doubleValue() == -1);
    assert(par("shortDuration").doubleValue() == -1);
    assert(par("duration").doubleValue() == -1);
    assert(par("numSessions").doubleValue() == -1);

    double simTime = par("endTime").doubleValue() * longestRtt;

    double simTimeInRTTs = par("endTime").doubleValue();
    double capacity = wf.linkCapacity[Link(0,1)];

    double backgroundRate = par("backgroundRate").doubleValue() * capacity;
    double numBackgroundFlows = par("numBackgroundFlows").doubleValue();
    assert(numBackgroundFlows/(1.0 - numBackgroundFlows) > 1);
    int numBackgroundFlowsInt = round(((numBackgroundFlows)/(1.0 - numBackgroundFlows)));
    assert(numBackgroundFlowsInt > 0);
    // (1.0 - 0.75) x is 1 --> 0.75 x is 0.75/(1.0-0.75)


    double longRate = capacity - backgroundRate; // bps
    double longSize = (simTime * longRate)/(8*(payload_size_+header_size_)); // packets over all RTTs
    double totalBackgroundFlowsSize = (longestRtt * backgroundRate)/(8*(payload_size_+header_size_)); // packets in an RTT
    int totalBackgroundFlowsSizeInt = round(totalBackgroundFlowsSize);
    double  backgroundFlowSize = totalBackgroundFlowsSizeInt/numBackgroundFlowsInt;
    double lastBackgroundFlowSize = totalBackgroundFlowsSizeInt%numBackgroundFlowsInt;
    assert(backgroundFlowSize > 0); // want at least numBackgroundFlows flows.



    // One long flow that goes on till end
    // Short flows of size .. start every RTT
    EV << "One long flow of size " << longSize << " packets (" << longRate << " Bps.)\n";
    EV << numBackgroundFlowsInt << " small flows of size " << backgroundFlowSize << " (and "\
            << lastBackgroundFlowSize << ") packets, start every RTT (" << backgroundRate << " Bps"\
            << " or " << totalBackgroundFlowsSize << " 8000b packets per RTT of " << longestRtt <<  "s .)"\
            << " and link capacity is " << capacity << "Bps. \n";

    // Add 10 long flows that go on till end
    FlowInfo longFlow(0, pairInt(0,1), longSize, par("initialRate"), 0.0, -1);
    std::vector<FlowInfo> flows(1, longFlow);

    // Every 4RTTs start xx 4RTT long short flows.
    for (double startTimeInRTTs = 0; startTimeInRTTs <= simTimeInRTTs; startTimeInRTTs += 1) {
        double startTime = startTimeInRTTs*longestRtt;
        for (int bg = 0; bg < numBackgroundFlowsInt-1; bg++) {
            FlowInfo shortFlow(0, pairInt(0,1), backgroundFlowSize, par("initialRate"), startTime, -1);
            flows.push_back(shortFlow);
        }
        if (lastBackgroundFlowSize > 0) {
            FlowInfo lastShortFlow(0, pairInt(0,1), lastBackgroundFlowSize, par("initialRate"), startTime, -1);
            flows.push_back(lastShortFlow);
        }
    }

    int i = 0; for (auto& flow: flows) flow.flowId = i++;

    for (auto& f: flows) allFlows[f.flowId] = f;

    EV << "Flow IDs: ";
    for (auto& af : allFlows) {
        FlowInfo flow = af.second;
        EV << flow.flowId << " (" << ((flow.duration == longSize) ? "long" : "background") << ")  ";
    }
    EV << ".\n";
}

} /* namespace rcp */
