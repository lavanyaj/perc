//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>

namespace rcp {


void CharnyFlowGenerator::initializeCharnySim10() {
    // check all parameters are usable
    assert(par("longDuration").doubleValue() != -1);
    assert(par("shortDuration").doubleValue() != -1);
    assert(par("duration").doubleValue() == -1);
    assert(par("numSessions").doubleValue() != -1);
    assert(par("endTime").doubleValue() != -1);
    int numSessions = par("numSessions").doubleValue();
    double longDuration = longestRtt * par("longDuration").doubleValue();
    double shortDuration = longestRtt * par("shortDuration").doubleValue();
    double simTime = par("endTime").doubleValue() * longestRtt;

    double simTimeInRTTs = par("endTime").doubleValue();
    double shortDurationInRTTs = par("shortDuration").doubleValue();
    double longDurationInRTTs = par("longDuration").doubleValue();

    // First RTT both long and short flows start
    // After short duration all short flows end.


    EV << "End times are " << simTime << "s (long flows) and " << shortDuration << "s (short flows).\n";
    EV << "Short flows start/end until " << longDuration << " RTTs.\n";


    // RTT 0 10 long flows, 10 short flows
    // RTT 4 short flows end 10 new short flows
    // RTT 8 short flows end 5 new short flows
    // RTT 12 .. 10 new short flows etc.


    // Add 10 long flows that go on till end
    FlowInfo longFlow(0, pairInt(0,1), par("duration"), par("initialRate"), 0.0, simTime);
    std::vector<FlowInfo> flows(numSessions, longFlow);
    assert(longDuration <= simTime);
    // Every 4RTTs start xx 4RTT long short flows.
    for (double startTimeInRTTs = 0; startTimeInRTTs <= longDurationInRTTs; startTimeInRTTs += shortDurationInRTTs) {
        double startTime = startTimeInRTTs*longestRtt;
        double endTime = startTime + shortDuration;
        if (endTime <= longDuration) {
            FlowInfo shortFlow(0, pairInt(0,1), par("duration"), par("initialRate"), startTime, endTime);
            std::vector<FlowInfo> shortFlows(numSessions, shortFlow);
            for (auto& flow: shortFlows) {
                flows.push_back(flow);
            }
        }
    }

    int i = 0; for (auto& flow: flows) {
        if (longDuration == shortDuration) {
            if (flow.endTime == simTime) flow.flowId = 2*i+1;
            else flow.flowId = 2*(i-numSessions);
        } else {
            flow.flowId = i;
        }
        i++;
    }


    for (auto& f: flows) allFlows[f.flowId] = f;

    EV << "Flow IDs: ";
    for (auto& af : allFlows) {
        FlowInfo flow = af.second;
        EV << flow.flowId << " (" << ((flow.endTime == simTime) ? "long" : "short") << ")  ";
    }
    EV << ".\n";
}

} /* namespace rcp */
