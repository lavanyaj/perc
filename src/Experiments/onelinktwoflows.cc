//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>

namespace rcp {

void CharnyFlowGenerator::initializeCharnySim1() {
    double endTime = par("endTime").doubleValue();
    int numRtts = endTime;
    if (numRtts > 0)
        endTime = getLongestRtt() * numRtts;
    EV << "End time is " << endTime << "s.\n";
    assert(endTime == -1);
    double duration = par("duration").doubleValue();
    int numRttsOfTraffic = duration;
    if (numRttsOfTraffic > 0)
        duration = par("ratio").doubleValue() * numRttsOfTraffic; // 10 BDPs?
    EV << "SIZE IS " << duration << ".\n";

    // RTT 12us
    // 1 pkt/ 1Gbps = 12us

    // rtt/(pkt/c) = (rtt*c)/pkt_size = bdp in # packets
    // 1pkt/ 10Gbps = 1.2us

    // duration = number of packets
    // 10000Gbps x 12us = 10000 packets .. lasts 1000 RTTs at 10Gbps, 100 at 100Gbps
    FlowInfo f0(0, pairInt(0,1), 10000, 0, 0.0, endTime);

    // starts at 50 x 12us aka 50 RTTs
    // 1000Gbps x 12us = 1000 packets .. lasts 100 RTTs at 10Gbps, 10 at 100Gbps
    FlowInfo f1(1, pairInt(0,1), 1000, 0, (12e-6 * 50), endTime);

    // To compare
    // effect of higher link speeds @12us .. at 10G, flow 1 lasts 1000, 2 lasts 100 long enough
    //                                      at 100G, flow 1 lasts 100, 2 lasts only 5
    //                              (will get too high rate .. queue .. or both too low .. stretched)



   // duration = number of packets
   // 10000Gbps x 12us = 10000 packets .. lasts 1000 RTTs at 10Gbps, 100 at 100Gbps
   // FlowInfo f0(0, pairInt(0,1), 100000, par("initialRate"), 0.0, endTime);

   // starts at 500 x 12us aka 500 RTTs at 12us, 50 RTTs at 120us
   // FlowInfo f1(1, pairInt(0,1), 1000, par("initialRate"), (1.2e-5 * 500), endTime);

    // To compare
    // effect of higher link speeds @12us .. at 10G, flow 1 lasts 10K, 2 lasts 100 long enough
    //                                      at 100G, flow 1 lasts 1K, 2 only 10 RTTs
    //                              (will get too high rate .. queue .. or both too low .. stretched)

   // To compare
   // effect of higher link speeds @120us .. at 10G, flow 1 lasts 1K, 2 lasts 10 not long enough
   //                                      at 100G, flow 1 lasts 100, 2 only 1 RTTs
   //                              (will get too high rate .. queue .. or both too low .. stretched)


    // (0.25 * numRttsOfTraffic)

    EV << "F1 starts at " << f1.startTime << ".\n";
    //FlowInfo f2(1, pairInt(0,1), duration, par("initialRate"), 0.0, endTime);
    //f2.startTime = (0.5 * numRttsOfTraffic) * getLongestRtt();
    //FlowInfo f3(1, pairInt(0,1), duration, par("initialRate"), 0.0, endTime);
    //f3.startTime = (0.75 * numRttsOfTraffic) * getLongestRtt();
    // if f0 by itself needs 400 RTTs, f1 starts at 100 RTTs
    // hopefully f0 has converged to link_capacity before 80 RTTs
    // and f0 won't end before both have converged to 0.5 link_capacity
    std::vector<FlowInfo> flows = {f0, f1};//, f2, f3};

    int i = 0; for (auto& flow: flows) flow.flowId = i++;
    for (auto& f: flows) allFlows[f.flowId] = f;
}

} /* namespace rcp */
