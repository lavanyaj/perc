//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "../Generic/CharnyFlowGenerator.h"

namespace rcp {
#include <assert.h>
#include <vector>

void CharnyFlowGenerator::initializeCharnySim7() {
    // check all parameters are usable
    std::vector<FlowInfo> flows;
    std::vector<int> endHosts;
    assert(par("numHosts").doubleValue() > 0);
    int numHosts = par("numHosts").doubleValue();
    for (int i = 0; i < numHosts; i++) endHosts.push_back(i);
    if (par("lambda").doubleValue() != -1) {
        EV << "Poisson flow arrival between all pairs.\n";
        makeAllPairsRandomFlows(endHosts, flows);
    } else if (par("numChanges").doubleValue() != -1 and\
            par("maxFlows").doubleValue() != -1 and\
            par("waitToConverge").doubleValue() > 0){
        int numChanges = par("numChanges").doubleValue();
        int maxFlows = par("maxFlows").doubleValue();
        makeRandomFlows(maxFlows, numChanges, endHosts, flows);
    } else {
        EV << "Long lived flows between all pairs.\n";
        makeAllPairsLongFlows(endHosts, flows);
    }
    for (auto& f: flows) allFlows[f.flowId] = f;

}

} /* namespace rcp */
