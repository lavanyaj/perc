//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "../Generic/CharnyFlowGenerator.h"

namespace rcp {
#include <assert.h>
#include <vector>

void CharnyFlowGenerator::initializeCharnySim70() {
    // check all parameters are usable
    int startTime = 0;
    int endTime = 20;
    int waitTime = 10;
    std::vector<FlowInfo> flows;
    int i = 0;
    FlowInfo f(0, Ends(2, 3), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(1, Ends(3, 0), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(2, Ends(2, 3), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(3, Ends(2, 3), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(4, Ends(3, 0), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(5, Ends(3, 2), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(6, Ends(0, 2), -1, 1, startTime, endTime); //flowId, start time i
    flows.push_back(f);

    f = FlowInfo(7, Ends(2, 1), -1, 1, startTime+waitTime, endTime); //flowId, start time i
    flows.push_back(f);

    for (auto& f: flows) allFlows[f.flowId] = f;

}

} /* namespace rcp */
