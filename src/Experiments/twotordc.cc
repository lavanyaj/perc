//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>

namespace rcp {

void CharnyFlowGenerator::initializeCharnySim6() {
    double endTime = par("endTime").doubleValue() * longestRtt;
    FlowInfo f(0, pairInt(0,1), par("duration"), par("initialRate"), 0.0, endTime);
    std::vector<FlowInfo> flows;

    /*
     *Max. iterations is 7 for up to 14 total sessions.
     *0 sessions from 0 to 4.
     *4 sessions from 0 to 5. #0..3
     *0 sessions from 0 to 6.
     *1 sessions from 0 to 7. #4
     *1 sessions from 1 to 4. #5
     *1 sessions from 1 to 5. #6
     *1 sessions from 1 to 6. #7
     *1 sessions from 1 to 7. #8
     *0 sessions from 2 to 4.
     *1 sessions from 2 to 5. #9
     *1 sessions from 2 to 6. #10
     *1 sessions from 2 to 7. #11
     *1 sessions from 3 to 4. #12
     *1 sessions from 3 to 5. #13
     *0 sessions from 3 to 6.
     *0 sessions from 3 to 7.
     */

    std::map<Ends, int> numSessions;
    //numSessions[Ends(0, 4)] = 0;
    numSessions[Ends(0, 5)] = 4;
    //numSessions[Ends(0, 6)] = 0;
    numSessions[Ends(0, 7)] = 1;
    numSessions[Ends(1, 4)] = 1;
    numSessions[Ends(1, 5)] = 1;
    numSessions[Ends(1, 6)] = 1;
    numSessions[Ends(1, 7)] = 1;
    //numSessions[Ends(2, 4)] 0 0;
    numSessions[Ends(2, 5)] = 1;
    numSessions[Ends(2, 6)] = 1;
    numSessions[Ends(2, 7)] = 1;
    numSessions[Ends(3, 4)] = 1;
    numSessions[Ends(3, 5)] = 1;
    //numSessions[Ends(3, 6)] = 0;
    //numSessions[Ends(3, 7)] = 0;


    for (auto& ns : numSessions) {
        f.ends = ns.first;
        for (int j = 0; j < ns.second; j++)
            flows.push_back(f);
    }

    int i = 0; for (auto& flow: flows) flow.flowId = i++;
    for (auto& f: flows) allFlows[f.flowId] = f;

}

} /* namespace rcp */
