#include "CharnyDataHost.h"
#include <algorithm>
#include <assert.h>

namespace rcp {
Define_Module(CharnyDataHost);

#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 if (false) std::cout

void CharnyDataHost::initialize()
{
    startSignal = registerSignal("start");
    endSignal = registerSignal("end");
    lengthSignal = registerSignal("length");
    numPacketsSentSignal = registerSignal("numPacketsSent");
    address = getParentModule()->getIndex();
    header_size_ = par("headerSize").doubleValue();
    payload_size_ = par("payloadSize").doubleValue();
    size_ = header_size_ + payload_size_;
    numPacketsSent = 0;

    const char *vinit[] = {"NumPkts", "TransmissionRate"};
    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));

}

void CharnyDataHost::handleEndFlowMessage(const CharnyMakeFlowMsg* flowmsg) {
    int flowId = flowmsg->getFlowId();
    EV2 << "Received end flow message " << flowmsg->getName() << ".\n";

    EV2 << "Changed number of packets to # packets sent so far + 1 (FIN to be sent).\n";
    sftable.at(flowId).flowmsg = *flowmsg;
    EV2 << "Stored end flow message " << sftable.at(flowId).flowmsg.getName() << ".\n";
    sftable.at(flowId).num_pkts_ = sftable.at(flowId).num_sent_+1;
    // need to send a FIN packet, num_pkts_ is total number of packets to send
    // num_sent is number of packets sent so far
    // aka sequence number of next packet to be sent (could be FIN)
}

void CharnyDataHost::handleStartFlowMessage(const CharnyMakeFlowMsg* flowmsg) {
    int flowId = flowmsg->getFlowId();

    SenderFlowEntry temp(flowmsg);
    if (temp.transmission_rate_ > 0) temp.interval_ = (size_*8.0)/temp.transmission_rate_;
    setupSignals(temp, flowId);
    sftable.insert(std::pair<int, SenderFlowEntry> (flowId, temp));
    EV2 << "Received start flow message " << sftable.at(flowId).flowmsg.getName() << ".\n";

    EV2 << "setting up " << sftable.at(flowId).signals.size()\
                 << " signals for flow # "  << flowId << ".\n";

    emit(startSignal, flowId);
    emit(lengthSignal, sftable.at(flowId).num_pkts_);
    EV2 << "Created flow entry for flow # " << flowId <<\
                 " to " << sftable.at(flowId).destination_ <<\
                 " with " << sftable.at(flowId).num_pkts_<< " packets" <<\
                 " and starting rate " << sftable.at(flowId).transmission_rate_ << " Bps.\n";

    // send to control host
    if (par("hasControlHost").boolValue()) {
        EV3 << "Sending start flow message to control host.";
        CharnyMakeFlowMsg * dupFlowMsg = new CharnyMakeFlowMsg(); // deleted by (control) host
        *dupFlowMsg = *flowmsg;
        assert(dupFlowMsg->getChangeType() == CHARNY_MAKEFLOW_START);
        send(dupFlowMsg, "controlGate$o");
    }

    // send back to Flow Generator JLT
    EV3 << "Sending start flow message to flow generator (I'm about to send SYN).";
    CharnyMakeFlowMsg * dupFlowMsg = new CharnyMakeFlowMsg(); // deleted by (control) host
    *dupFlowMsg = *flowmsg;
    dupFlowMsg->setChangeType(CHARNY_MAKEFLOW_SYN);
    send(dupFlowMsg, "flowAlert$o");

    // no control host- ideal mode- start sending as soon as you get a change rate.
    if (par("hasControlHost").boolValue()) {
    EV2 << "Sending SYN.\n";
    sendpkt(flowId, RCP_SYN);
    sftable.at(flowId).RCP_state = RCP_SYNSENT;
    } else {
        sftable.at(flowId).RCP_state = RCP_RUNNING;
    }
}

void CharnyDataHost::handleRateChangeFlowMessage(const CharnyMakeFlowMsg* flowmsg) {
    int flowId = flowmsg->getFlowId();
    EV3 << "Received change rate message for flow # " << flowId << ".\n";
    switch(sftable.at(flowId).RCP_state) {
        case RCP_SYNSENT:
            if (flowmsg->getRate() != sftable.at(flowId).transmission_rate_) {
                EV3 << "Changing rate only of flow # " << flowId << " from " <<\
                                                sftable.at(flowId).transmission_rate_ << " to " <<\
                                                flowmsg->getRate() << ".\n";

                double newRate = flowmsg->getRate();
                sftable.at(flowId).transmission_rate_ = newRate;
                sftable.at(flowId).interval_ = (size_*8)/newRate;
                EV3 << "interval is " << sftable.at(flowId).interval_ << "s, i.e., size "\
                        << size_ *8<< " bits divided by rate " << newRate\
                        << " bits per second.\n";
            }
            break;
        case RCP_RUNNING:
            if (flowmsg->getRate() != sftable.at(flowId).transmission_rate_) {
                EV3 << "Changing rate (and rescheduling timeouts) of flow # "\
                                            << flowId << " from " <<\
                                            sftable.at(flowId).transmission_rate_ << " to " <<\
                                            flowmsg->getRate() << ".\n";
                rate_change(flowId, flowmsg->getRate());
            }
            break;
        case RCP_INACT:
            EV3 << "flowmsg arrived for flow # " << flowId\
                                << " to change rate to " << flowmsg->getRate()\
                                << " Bps but flow finished already.\n";
            break;
        case RCP_FINSENT:
            EV3 << "flowmsg arrived for flow # " << flowId\
            << " to change rate to " << flowmsg->getRate()\
             << " Bps but flow sent FIN already.\n";
            break;
        default:
            EV3 << "Flow in unknown state.\n";
    }
}

void CharnyDataHost::handleMessage(cMessage *msg)
{
     if(msg->arrivedOn("flowAlert$i") ) {
        EV2 << "CharnyDataHost " << address << ": Arrived on Flow Alert\n";
        CharnyMakeFlowMsg *flowmsg = check_and_cast<CharnyMakeFlowMsg *>(msg);
        assert(flowmsg->getChangeType() == CHARNY_MAKEFLOW_START or\
                flowmsg->getChangeType() == CHARNY_MAKEFLOW_END or\
                CHARNY_MAKEFLOW_CHANGE_RATE);

        if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_START ) handleStartFlowMessage(flowmsg);
        else if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_END) handleEndFlowMessage(flowmsg);

        // Could get rate change messages from flow generator (ideal mode)
        if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_RATE) handleRateChangeFlowMessage(flowmsg);

        delete msg; msg = NULL;
     } else if (msg->arrivedOn("controlGate$i")) {
        CharnyMakeFlowMsg *flowmsg = check_and_cast<CharnyMakeFlowMsg *>(msg);
        if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_RATE) handleRateChangeFlowMessage(flowmsg);
        delete msg; msg = NULL;
     } else if(msg->isSelfMessage()) { // timeout event
        RcpTimeoutPacket *pkt = check_and_cast<RcpTimeoutPacket *>(msg);
        assert(pkt->getTimeoutType() == RCP_TIMEOUT);
        rcp_timeout(pkt->getFlowId());
     } else { // from other data host
        GenericDataPacket *oldpkt = check_and_cast<GenericDataPacket *>(msg);
        handleGenericDataPacket(oldpkt);
        delete msg; msg = NULL;
     }
     //if (!msg->isSelfMessage()) {delete msg; msg = NULL;}

}

void CharnyDataHost::setupSignals(FlowEntry& temp, int flowId) {
    for (const auto& statName : statNames ) {
        std::stringstream ss;
        ss.clear(); ss.str("");
        ss << "session-" << flowId << "-" << statName;
        temp.signals[ss.str()] = registerSignal(ss.str().c_str());

        std::stringstream ts;
        ts.clear(); ts.str("");
        ts << "session" << statName;

        cProperty *statisticTemplate =\
        getProperties()->get("statisticTemplate", ts.str().c_str());
        ev.addResultRecorders(this, temp.signals[ss.str()],\
                ss.str().c_str(), statisticTemplate);
    }

}

// void CharnyDataHost::reset() // not used in this version of RCP.

void CharnyDataHost::reset_timeout(int flowId, double next_interval) {
    EV2 << "-- Scheduling timeout for flow # "<< flowId << " after " << next_interval\
                        << " from now " << simTime() << ".\n";
    RcpTimeoutPacket *timeoutPkt = sftable.at(flowId).timeoutpkt_;
    if (timeoutPkt->isScheduled()) {
        EV2 << "Timeout packet is scheduled already, cancel before rescheduling.\n";
        cancelEvent(timeoutPkt);
    }
    scheduleAt(simTime() + next_interval, timeoutPkt);
}
void CharnyDataHost::cancel_timeout(int flowId) {
    RcpTimeoutPacket *timeoutPkt = sftable.at(flowId).timeoutpkt_;
    if (timeoutPkt) {
        if (timeoutPkt->isScheduled()) {
            EV2 << "Cancel timeout packet permanently for flow # "<< flowId << ".\n";
            cancelAndDelete(timeoutPkt);
            timeoutPkt = NULL;
            sftable.at(flowId).timeoutpkt_ = NULL;
        } else {
            delete timeoutPkt;
            timeoutPkt = NULL;
            sftable.at(flowId).timeoutpkt_ = NULL;
        }
    }
}


void CharnyDataHost::sendpkt(int flowId, enum RCP_PKT_T pktType) {
    char pktname[100];
    sprintf(pktname, "data-flow#-%d-from-%d-to-%d-seq-%d-of-%d",\
            flowId,\
            address,\
            sftable.at(flowId).destination_,\
            sftable.at(flowId).num_sent_,\
            sftable.at(flowId).num_pkts_);

    GenericDataPacket *pkt = new GenericDataPacket(pktname);
        pkt->setSource(address);
        pkt->setDestination(sftable.at(flowId).destination_);
        double size = header_size_;
        if (pktType == RCP_DATA or pktType == RCP_FIN) {
            size += payload_size_;
        }
        pkt->setByteLength(size);
        pkt->setSeqNo(sftable.at(flowId).num_sent_);

        pkt->setTs(SIMTIME_DBL(simTime()));
        pkt->setPktType(pktType);
        pkt->setFlowId(flowId);

        emitSignal(flowId, "NumPkts", sftable.at(flowId).num_sent_);
        emitSignal(flowId, "TransmissionRate", sftable.at(flowId).transmission_rate_);

        if (pktType == RCP_SYN)
            pkt->setKind(CHARNY_DATA_PKT_SYN);
        else
            pkt->setKind(CHARNY_DATA_PKT);
        forwardPacket(pkt);
        sftable.at(flowId).lastpkttime_ = SIMTIME_DBL(simTime());

        if (sftable.at(flowId).num_pkts_ != -1) {
            if (par("hasControlHost").boolValue()) {
                int numDataPacketsSent = sftable.at(flowId).num_sent_ + 1;
                // seq no. of SYN, first DATA is 0
                // num_sent_ is index of packet just sent ^
                int numDataPacketsLeft = sftable.at(flowId).num_pkts_ - numDataPacketsSent;

                if (numDataPacketsLeft > 0) {
                    CharnyMakeFlowMsg* demandChangeMsg = sftable.at(flowId).flowmsg.dup();
                    demandChangeMsg->setChangeType(CHARNY_MAKEFLOW_CHANGE_DEMAND);
                    demandChangeMsg->setDuration(numDataPacketsLeft);
                    send(demandChangeMsg, "controlGate$o");
                    EV3 << "Sending demand change message with duration (num_pkts_) " << demandChangeMsg->getDuration()\
                            << " packets to Control Host.\n";
                } else {
                    // TODO: fix switches to ignore demand if the packet is FIN
                    // if this was the last DATA packet, just sent a FIN, demand stays same ..
                    EV3 << "Not sending demand change message with duration (num_pkts_) " << numDataPacketsLeft\
                            << " packets to Control Host, already sent FIN hopefully.\n";
                }

            }
        }

}

void CharnyDataHost::emitSignal(int flowId, std::string name, double value) {
    std::stringstream ss; ss.clear(); ss.str("");
    ss << "session-" << flowId << "-" << name;
    emit(sftable.at(flowId).signals[ss.str()], value);
    EV2 << "At time "<< simTime() << ", emitting " << ss.str() << ": " << value << "\n";
 }

void CharnyDataHost::rcp_timeout(int flowId)
{
    if (sftable.at(flowId).RCP_state == RCP_RUNNING) {
        assert(sftable.at(flowId).num_pkts_ > 0 || sftable.at(flowId).num_pkts_ == -1);
        int numPacketsLeft = -1;
        if (sftable.at(flowId).num_pkts_ >= 0) {
            numPacketsLeft = sftable.at(flowId).num_pkts_ - sftable.at(flowId).num_sent_;
        }
        // here num_sent_ is seq. no of packet to be sent now, since seq. no. start at 0
        // this is also the number of packets sent so far
        if (sftable.at(flowId).num_pkts_ == -1\
                or (sftable.at(flowId).num_pkts_ >= 0 and numPacketsLeft >= 2)) {  // last packet must be FIN
            EV2 << "Sending data packet # " <<\
                    sftable.at(flowId).num_sent_\
                    << " of " << sftable.at(flowId).num_pkts_\
                    << " pkts of flow # " << flowId << ".\n";

            if (sftable.at(flowId).num_sent_ == 0){
                // Telling Flow-Gen I'm about to send FIRST DATA PKT.
                CharnyMakeFlowMsg *startMsg = new CharnyMakeFlowMsg();
                *startMsg = sftable.at(flowId).flowmsg;
                startMsg->setRate(sftable.at(flowId).transmission_rate_);
                startMsg->setChangeType(CHARNY_MAKEFLOW_FIRST_DATA);
                send(startMsg, "flowAlert$o");
            }

            sendpkt(flowId, RCP_DATA);
            sftable.at(flowId).num_sent_++;

            EV2 << "Scheduling timeout for after " << sftable.at(flowId).interval_\
                    << " from now " << simTime() << ".\n";
            reset_timeout(flowId, sftable.at(flowId).interval_);
            // setting this twice..?
        } else if (sftable.at(flowId).num_pkts_ >= 0 and numPacketsLeft == 1) {
            EV2 << "Canceled timeout for flow # " << flowId << ".\n";
            cancel_timeout(flowId);

            if (sftable.at(flowId).flowmsg.getChangeType() == CHARNY_MAKEFLOW_START) {\
                // Ending cuz packet size, not end flow message
                EV2 << "About to send FIN, flowmsg still says start, change to end.\n";
                std::stringstream msgName;
                msgName.clear();
                msgName.str("");
                msgName << "end flow # " << flowId << " from " << sftable.at(flowId).source_\
                        << " to " << sftable.at(flowId).destination_ << ".\n";
                sftable.at(flowId).flowmsg.setChangeType(CHARNY_MAKEFLOW_END);
                sftable.at(flowId).flowmsg.setName(msgName.str().c_str());
            }

            // so control host will send within an RTT of the last (FIN) data packet
            // being send.
            if (par("hasControlHost").boolValue()) {
                CharnyMakeFlowMsg *endMsg = new CharnyMakeFlowMsg(sftable.at(flowId).flowmsg);
                assert(endMsg->getChangeType() == CHARNY_MAKEFLOW_END);
                EV << "Sending end message to control host with duration " << endMsg->getDuration() << ".\n";
                send(endMsg, "controlGate$o");
            }

            {
                // here num_sent_ is seq. no of packet to be sent now, since seq. no. start at 0
                // this is also the number of packets sent so far
                if (sftable.at(flowId).num_sent_ == 0){
                    // Telling Flow-Gen I'm about to send FIRST DATA PKT.
                    CharnyMakeFlowMsg *startMsg = new CharnyMakeFlowMsg();
                    *startMsg = sftable.at(flowId).flowmsg;
                    startMsg->setRate(sftable.at(flowId).transmission_rate_);
                    startMsg->setChangeType(CHARNY_MAKEFLOW_FIRST_DATA);
                    send(startMsg, "flowAlert$o");
                }

                // Telling Flow-Gen I'm about to send FIN.
                CharnyMakeFlowMsg *endMsg = new CharnyMakeFlowMsg();
                *endMsg = sftable.at(flowId).flowmsg;
                endMsg->setRate(sftable.at(flowId).transmission_rate_);
                endMsg->setChangeType(CHARNY_MAKEFLOW_FIN);
                send(endMsg, "flowAlert$o");
            }

            EV3 << "Sending data packet FIN (seq. no " << sftable.at(flowId).num_sent_  << ") for flow " << flowId <<  " .\n";
            sendpkt(flowId, RCP_FIN);
            sftable.at(flowId).num_sent_++;
            sftable.at(flowId).RCP_state = RCP_FINSENT;



        } else {
            assert((sftable.at(flowId).num_pkts_ >= 0 and numPacketsLeft == 0));
            EV3 << "!! Total number of packets is " << sftable.at(flowId).num_pkts_ \
                    << ", number of packets left is " << numPacketsLeft\
                    << ", not sending DATA/ FIN.\n";

        }

    }
}


void CharnyDataHost::handleGenericDataPacket(const GenericDataPacket *pkt)
{
    int flowId = pkt->getFlowId();

    int RCP_state = -1;
    if (sftable.count(flowId) > 0) RCP_state = sftable.at(flowId).RCP_state;
    else if (rftable.count(flowId) > 0) RCP_state = rftable.at(flowId).RCP_state;
    else { EV2 << "No entry for flow # " << flowId << " in either flow table.\n";}
    //assert(RCP_state != -1);

    if ((pkt->getPktType() == RCP_SYN) ||\
            (RCP_state != RCP_INACT and RCP_state != -1) ||\
            ((pkt->getPktType() == RCP_DATA or pkt->getPktType() == RCP_FIN) and pkt->getSeqNo() == 0\
                    and !par("hasControlHost").boolValue())) // special case start w/o syn
    {
        switch (pkt->getPktType()) {
        case RCP_SYNACK:
            EV2 << "Received SYNACK.\n";
            // RCP updates interval_ but we do on interval_update msg from control
            if (sftable.at(flowId).RCP_state == RCP_SYNSENT) {
                sftable.at(flowId).RCP_state = RCP_RUNNING;
                EV2 << "Not yet starting packet transmission for flow # " << flowId << " at rate " <<\
                        sftable.at(flowId).transmission_rate_ << " Bps.\n";
                if (sftable.at(flowId).transmission_rate_ > 0)
                        rate_change(flowId, sftable.at(flowId).transmission_rate_); // rate in Bps or rate/1000 is packets per second

            }
            break;

        case RCP_FINACK:

        {
            // Telling Flow-Gen I just got FIN-ACK.
            CharnyMakeFlowMsg *endMsg = new CharnyMakeFlowMsg();
            *endMsg = sftable.at(flowId).flowmsg;
            endMsg->setRate(sftable.at(flowId).transmission_rate_);
            endMsg->setChangeType(CHARNY_MAKEFLOW_FINACK);
            send(endMsg, "flowAlert$o");
        }

        case RCP_ACK:
            EV2 << "Received " << ((pkt->getPktType() == RCP_FINACK) ? "FIN": "") << "ACK for packet # " << pkt->getNumPktsRecv()\
                << " of flow # " << flowId << ".\n";
            sftable.at(flowId).num_datapkts_acked_by_receiver_ = pkt->getNumPktsRecv();
            if (sftable.at(flowId).num_datapkts_acked_by_receiver_ == sftable.at(flowId).num_pkts_) {
                //EV2 << "Cancelled timeout for flow # " << flowId << ".\n";
                //cancel_timeout(flowId, RCP_TIMEOUT);
                sftable.at(flowId).RCP_state = RCP_INACT;
            }
            // RCP updates interval_ and calls rate_change
            break;
        case RCP_FIN:
            EV3 << "Received FIN.\n";

            // for ideal, FIN could be first and only packet (sends data instantly, no SYN)
            if (rftable.count(flowId) == 0) {
                assert(pkt->getSeqNo() == 0);
                assert(!par("hasControlHost").boolValue());
                rftable.insert(std::pair<int, ReceiverFlowEntry> (flowId, ReceiverFlowEntry(pkt)));
                assert(rftable.count(flowId) > 0);
                rftable.at(flowId).RCP_state = RCP_RUNNING;
            }

            rftable.at(flowId).num_datapkts_received_++; // because RCP_FIN is piggybacked on the last packet of flow
            EV3 << "Sending FINACK.\n";
            sendack(flowId, pkt, RCP_FINACK);
            break;
        case RCP_SYN:
            EV2 << "Received SYN.\n";
            rftable.insert(std::pair<int, ReceiverFlowEntry> (flowId, ReceiverFlowEntry(pkt)));
            assert(rftable.count(flowId) > 0);
            rftable.at(flowId).RCP_state = RCP_RUNNING; // Only the receiver changes state here
            EV2 << "Sending SYNACK.\n";
            sendack(flowId, pkt, RCP_SYNACK);
            break;
        case RCP_DATA:
            EV2 << "Received DATA packet # " << pkt->getSeqNo() << ".\n";

            if (rftable.count(flowId) == 0) {
                assert(pkt->getSeqNo() == 0);
                assert(!par("hasControlHost").boolValue());
                rftable.insert(std::pair<int, ReceiverFlowEntry> (flowId, ReceiverFlowEntry(pkt)));
                assert(rftable.count(flowId) > 0);
                rftable.at(flowId).RCP_state = RCP_RUNNING;
            }

            rftable.at(flowId).num_datapkts_received_++;
            EV2 << "Sending data ACK for #" <<\
                    rftable.at(flowId).num_datapkts_received_ << ".\n";
            sendack(flowId, pkt, RCP_ACK);
            break;
        default:
            EV2 << "Error: Unknown RCP packet type!\n";
            exit(1);
            break;
        }
    }

}

// TODO typedef enum in util.h
void CharnyDataHost::sendack(int flowId, const GenericDataPacket *pkt, enum RCP_PKT_T pktType) {
    GenericDataPacket *newpkt = pkt->dup();
    newpkt->setByteLength(header_size_);
    // RcpRate is just transmission rate, not read anywhere
    double copy_rate = pkt->getRcpRate(); // Can modify the rate here.
    newpkt->setRcpRate(copy_rate);
    newpkt->setPktType(pktType);
    newpkt->setSource(pkt->getDestination());
    newpkt->setDestination(pkt->getSource());
    newpkt->setNumPktsRecv(rftable.at(flowId).num_datapkts_received_);
    if (pktType == RCP_SYNACK) {
        newpkt->setKind(CHARNY_DATA_PKT_SYN);
    } else {
        newpkt->setKind(CHARNY_DATA_PKT);
    }
    forwardPacket(newpkt);
}

void CharnyDataHost::rate_change(int flowId, double newRate) // on receiver side
{
    if (sftable.at(flowId).RCP_state == RCP_RUNNING)
    {

        RcpTimeoutPacket *timeoutPkt = sftable.at(flowId).timeoutpkt_;
        if (timeoutPkt->isScheduled()) {
            EV2 << "Timeout packet is scheduled already, cancel before rescheduling.\n";
            cancelEvent(timeoutPkt);
        }
        double t = 0;
        if (newRate > 0) {
        sftable.at(flowId).transmission_rate_ = newRate;
        sftable.at(flowId).interval_ = (size_*8.0)/newRate; // size in Bytes, newRate in bits per second
        EV2 << "interval is " << sftable.at(flowId).interval_ << "s, i.e., size "\
                << size_ << " bytes divided by rate " << newRate << " bytes per second.\n";
        // looks like this is not working ^
        t = sftable.at(flowId).lastpkttime_ + (sftable.at(flowId).interval_)\
                - SIMTIME_DBL(simTime());
        if (t <= 0) t = 0;
        } else {
            EV2 << "interval is \infty s, since rate is " <<  newRate << " bytes per second "\
                    << " and size is " << size_ << " bytes.\n";
            //assert(size_ == 0);
        }

        CharnyMakeFlowMsg* rateChangeMsg = sftable.at(flowId).flowmsg.dup();
        rateChangeMsg->setChangeType(CHARNY_MAKEFLOW_CHANGE_RATE);
        rateChangeMsg->setRate(newRate);
        send(rateChangeMsg, "flowAlert$o");
        EV3 << "Data host " << this->getId() << " sending rate change message with rate " << rateChangeMsg->getRate()\
                        << " bps to Flow Generator.\n";

        if (newRate > 0) reset_timeout(flowId, t);
        else cancel_timeout(flowId);
     }
}


void CharnyDataHost::forwardPacket(GenericDataPacket *pkt)
{
    EV2 << "CharnyDataHost " << address << ": Forwarding message " << pkt << " on out\n";
    numPacketsSent++;
    emit(numPacketsSentSignal, numPacketsSent);
    //pkt->setKind(CHARNY_DATA_PKT);
    send(pkt, "gate$o");
}

#endif
#endif
}; // namespace
