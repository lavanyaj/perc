//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __CHARNY_CHARNYDATAHOST_H
#define __CHARNY_CHARNYDATAHOST_H

#include <omnetpp.h>
#include "util.h"
#include <map>
#include <utility>
namespace rcp {

class FlowEntry {
public:
    // both sender and receiver
    int flowId;
    int RCP_state;
    int source_;
    int destination_;
    bool isSender;
    std::map<std::string, simsignal_t> signals;

    FlowEntry(int flowId, int RCP_state, int source_, int destination_, bool isSender) :\
            flowId(flowId), RCP_state(RCP_state), source_(source_), destination_(destination_),\
            isSender(isSender) {}
};

class SenderFlowEntry : public FlowEntry {
public:
    // only sender
    int num_sent_;
    int num_datapkts_acked_by_receiver_;
    int num_pkts_;

    double interval_;
    double lastpkttime_;
    double transmission_rate_;

    CharnyMakeFlowMsg flowmsg;
    RcpTimeoutPacket *timeoutpkt_;

    SenderFlowEntry(const CharnyMakeFlowMsg *msg) :\
            FlowEntry(msg->getFlowId(),RCP_INACT, msg->getSource(),\
                    msg->getDestination(), true),\
                    num_sent_(0),num_datapkts_acked_by_receiver_(0),\
                    num_pkts_(int(msg->getDuration())),\
                    lastpkttime_(-1), transmission_rate_(msg->getRate()),\
                    flowmsg(*msg) {

            timeoutpkt_ = new RcpTimeoutPacket("timeout");
            timeoutpkt_->setFlowId(flowId);
            timeoutpkt_->setTimeoutType(RCP_TIMEOUT);
        }
};

class ReceiverFlowEntry : public FlowEntry {
public:
    // only receiver
    int num_datapkts_received_;

    ReceiverFlowEntry(const GenericDataPacket* synPkt) :\
            FlowEntry(synPkt->getFlowId(),RCP_INACT, synPkt->getSource(),\
            synPkt->getDestination(), false), num_datapkts_received_(0) {};
};

class CharnyDataHost : public cSimpleModule
{

  private:
    simsignal_t numPacketsSentSignal;
    simsignal_t startSignal;
    simsignal_t endSignal;
    simsignal_t lengthSignal;
    int address; // CharnyHost index for now
    int size_; // bytes in a data packet: header + payload
    double header_size_;
    double payload_size_;

    typedef std::map<int, SenderFlowEntry> SenderFlowTable;
    SenderFlowTable sftable;
    typedef std::map<int, ReceiverFlowEntry> ReceiverFlowTable;
    ReceiverFlowTable rftable;

    int numPacketsSent;
    std::vector<std::string> statNames;
  protected:
    virtual void initialize();

    virtual void handleMessage(cMessage *msg);

    // new CharnyMakeFlowMsg -> control host (deleted in CharnyHost::handleMessage)
    void handleStartFlowMessage(const CharnyMakeFlowMsg* flowmsg);
    void handleEndFlowMessage(const CharnyMakeFlowMsg* flowmsg);

    // new RcpTimeoutPkt -> self
    //virtual void createFlowEntry(const CharnyMakeFlowMsg *msg);
    //virtual void createFlowEntryForReceiver(const GenericDataPacket* synPkt);
    // new GenericDataPacket -> data host (deleted in CharnyDataHost::handleMessage)

    virtual void sendpkt(int flowId, enum RCP_PKT_T pktType);

    void handleRateChangeFlowMessage(const CharnyMakeFlowMsg* flowmsg);
    virtual void rate_change(int flowId, double rate);
    virtual void reset_timeout(int flowId, double nextInterval);

    void handleGenericDataPacket(const GenericDataPacket *pkt);
    // (dup) GenericDataPacket -> data host (deleted in CharnyDataHost::handleMessage)
    void sendack(int flowId, const GenericDataPacket *pkt, enum RCP_PKT_T pktType);


    // on timeout, send DATA/ FIN and reset/ cancel timeout
    // new CharnyMakeFlowMsg -> control host (deleted in CharnyHost::handleMessage
    virtual void rcp_timeout(int flowId);
    virtual void cancel_timeout(int flowId);

    void emitSignal(int flowId, std::string name, double value);
    void setupSignals(FlowEntry& temp, int flowId);

    virtual void forwardPacket(GenericDataPacket *pkt);



};


}; // namespace

#endif
