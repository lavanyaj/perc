#include "CharnyFlowGenerator.h"
#include "waterfilling.h"

#include <vector>
#include <sstream>
#include <string>
#include <algorithm>
#include <assert.h>
#include <cstring>

namespace rcp {

Define_Module(CharnyFlowGenerator);
#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 EV << "EV2: "
#ifndef EV1
#define EV1 if (false) std::cout

// get paths from topology
void CharnyFlowGenerator::getAllPathsForWaterfilling() {
    // From Routing.cc
        cTopology *topo = new cTopology("topo");

        std::vector<std::string> nedTypes;
        nedTypes.push_back(par("nodeType").stringValue());
        topo->extractByNedTypeName(nedTypes);
        EV3 << "FlowGen cTopology found " << topo->getNumNodes()\
                << " "  << par("nodeType").stringValue() << " nodes\n";


        for (int src=0; src<topo->getNumNodes(); src++)
            topoId[topo->getNode(src)->getModuleId()] = src;

        linkCapacity.clear();
        std::map<Link, double> linkDelay;
        for (int src=0; src<topo->getNumNodes(); src++) {
            cTopology::Node* node = topo->getNode(src);
            for (int l= 0; l < node->getNumOutLinks(); l++) {
                cTopology::LinkOut* linkOut = node->getLinkOut(l);
                cTopology::Node* otherNode = linkOut->getRemoteNode();
                cGate* localGate = linkOut->getLocalGate();
                cDatarateChannel *chan = check_and_cast<cDatarateChannel *> \
                        (localGate->getTransmissionChannel());
                double cap = chan->getDatarate();
                double delay = SIMTIME_DBL(chan->getDelay());
                Link link(topoId[node->getModuleId()], topoId[otherNode->getModuleId()]);
                linkCapacity[link] = cap;
                linkDelay[link] = delay;
            }
        }

        for (auto& lc : linkCapacity)
            EV1 << "Link (" << lc.first.first << ", " << lc.first.second << ")"\
            << " has capacity " <<  lc.second << " Bps.\n";

        std::map<Ends, std::vector<Link>> sessionPaths;


        for (int src=0; src<topo->getNumNodes(); src++) {
            topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(src));
            for(int dst=0; dst<topo->getNumNodes(); dst++) {
                std::vector<Link> path;
                if (topo->getNode(dst)->getNumPaths() == 0) continue;
                cTopology::Node *tmp = topo->getNode(dst);
                int length = 0;
                while (tmp != topo->getNode(src) and length < 10) {
                    cTopology::Node *outNode = tmp->getPath(0)->getRemoteNode();
                    path.push_back(Link(topoId[tmp->getModuleId()],\
                            topoId[outNode->getModuleId()]));
                    tmp = outNode;
                    length++;
                }

                sessionPaths[Ends(dst, src)] = path;
            }
        }

        struct MaxDelay {
            Ends ends;
            double value;
            MaxDelay(Ends ends, double value) : ends(ends), value(value) {}
        } maxDelay(Ends(-1, -1), -1);
        std::map<Ends, double> sessionPathsDelay;
        for (auto& sp: sessionPaths) {
            assert(sp.second.size() > 0);
            double totalDelay = 0;
            for (auto& link : sp.second) {
                double transmissionDelay = 0; //(header_size_*8/linkCapacity.at(link));
                EV1 << "Transmission delay of Link (" << link.first << ", " << link.second\
                            << ") : " << transmissionDelay << " s.\n";
                double propagationDelay = linkDelay.at(link);
                EV1 << "Propagation delay of Link (" << link.first << ", " << link.second\
                                            << ") : " << propagationDelay << " s.\n";
                totalDelay += transmissionDelay+propagationDelay;
            }
            if (sessionPathsDelay.count(sp.first) == 0) {
                sessionPathsDelay[sp.first]  = totalDelay;
                if (maxDelay.value == -1 or totalDelay > maxDelay.value)
                    maxDelay = MaxDelay(sp.first, totalDelay);
                EV1 << "Total RTT from " << maxDelay.ends.first << " to "\
                << maxDelay.ends.second << ": " << 2 * totalDelay << ".\n";
            }

        }
        EV2 << "Longest RTT is for path from " << maxDelay.ends.first << " to "\
                << maxDelay.ends.second << ": " << 2 * maxDelay.value << " s.\n";
        longestRtt = 2 * maxDelay.value;

        /*
        for (auto& sp : sessionPaths) {
            EV1 << "Path from " << sp.first.first\
                    << " to " << sp.first.second << ": ";
            for (auto& l : sp.second) {
                EV1 << "(" << l.first << ", " << l.second << ") ";
            }
            EV1 << ".\n";
        }
         */
        std::map<Session, std::vector<Link>> pathsById;
        int id = 0;
        for (auto& sp : sessionPaths)
            pathsById[id++] = sp.second;

        wf = Waterfilling(pathsById, linkCapacity);
        EV3 << "WF has " << wf.numLinks << " links and " << wf.numSessions << " sessions.\n";
        delete topo;
        topo = NULL;
}

void CharnyFlowGenerator::setupSignals() {

    /*
     * waterfillingIterationsForFlowChangeSignal = registerSignal("waterfilling");
        bottleneckNumberForFlowChangeSignal = registerSignal("bottleneckNumber");

      sendFlowMsgSignal = registerSignal("sendFlowMsg");
    sendFlowMsgTimeSignal = registerSignal("sendFlowMsgTime");

    flowCompletionTimeSignal = registerSignal("flowCompletionTime");
    flowCompletionSizeSignal = registerSignal("flowCompletionSize");
     *
     */
    for (const auto& statName : statNames ) {
        signals[statName] = registerSignal(statName.c_str());
    }

}

// initialize signals, sendFlowMsgs() for self, setup check optimal timeouts
void CharnyFlowGenerator::initialize()
{

    const char *vinit[] = {"waterfilling", "bottleneck", "waterfillingInc",\
            "activeFlows", "flowSynTime", "flowSynId", "flowFirstDataTime",\
            "flowFirstDataId", "flowFinTime", "flowFinId", "flowFinackTime",\
            "flowFinackId", "flowSynSize", "activeFlowsTime", \
            "flowCompletionTime", "optimalWithinXRtts", "optimalWithinXTime",\
            "optimalRtts", "optimalTime", "flowStats", "totalSendingRate",\
            "totalSendingRatePerRtt", "flowArrival", "flowArrivalRate", "dupSyn", "dupFin", "dupFinack",\
            "dupFirstData", "flowSynWaterfillingInc", "flowFinWaterfillingInc",\
            "flowSynRttsToXConverge", "flowSynRttsToConverge",\
            "flowFinRttsToXConverge", "flowFinRttsToConverge", \
            "flowSynBottleneck", "flowFinBottleneck", "buggyEvent"};

    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));
    setupSignals();

    statsTimeoutInterval = par("statsTimeoutInterval").doubleValue();

    header_size_ = par("headerSize").doubleValue();
    payload_size_ = par("payloadSize").doubleValue();

    lastFlowChangeTime = -1;
    lastFlowChangeId = -1;
    lastFlowChangeType = -1;
    notYetOptimalWithinX = true;
    notYetOptimal = true;
    beenOptimalWithinXForLast = 0;
    beenOptimalForLast = 0;
    lastSendTime = 0;
    rttsSinceLastFlowChange = -1;


    WATCH(lastFlowChangeTime);
    WATCH(lastFlowChangeId);
    WATCH(lastFlowChangeType);
    WATCH(rttsSinceLastFlowChange);

    int sim = par("sim");

    getAllPathsForWaterfilling();

    assert(linkCapacity.size() > 0);
    if (sim == 1)
        initializeCharnySim1();
    else if (sim == 2)
        initializeCharnySim2();
    else if (sim == 3)
        initializeCharnySim3();
    else if (sim == 5)
         initializeCharnySim5();
    else if (sim == 6)
        initializeCharnySim6();
    else if (sim == 7)
        initializeCharnySim7(); // simple DC topology, all long lived flows, start at 0
    else if (sim == 70) // tricky example, long convergence time for last flow
            initializeCharnySim70(); // simple DC topology, all long lived flows, start at 0
    else if (sim == 8)
        initializeCharnySim8();
    else if (sim == 10)
            initializeCharnySim10(); // 50-50 mix of short and large flows, all start at 0.
    else if (sim == 11)
               initializeCharnySim11(); // one long flow and background traffic of rate xx with yy number of flows.
    else if (sim == 15)
                   initializeCharnySim15(); // uniform size flows one one link
    //else if (sim == 20)
    //        initializeCharnySim20();


    //getMaxMinRates(allFlows);
    std::vector<FlowInfo> flows;

    for (auto& af : allFlows) {
        flows.push_back(af.second);
    }
    sendFlowMsgs(flows);

    statsTimeout = new cMessage("statsTimeout");
    if (statsTimeout->isScheduled()) {}
    else scheduleAt(simTime(), statsTimeout);

    checkOptimalMsg = new cMessage("checkOptimalMsg");
    checkOptimalsPerRTT = par("checkOptimalsPerRTT").doubleValue();

    if (checkOptimalMsg->isScheduled()) {
        EV3 << "Check optimal already scheduled.\n";
    } else {
        scheduleAt(simTime()+longestRtt/float(checkOptimalsPerRTT), checkOptimalMsg);
    }

    EV3 << "Last send time is " << lastSendTime << ".\n";


}

void CharnyFlowGenerator::stats_timeout() {
    double flowArrivalRate = -1;
    if (statsTimeoutInterval > 0)
        flowArrivalRate = flowArrival/statsTimeoutInterval;
    emitSignal("flowArrivalRate", flowArrivalRate);
    flowArrival = 0;

    // in FCT experiments
    // if we've sent the last pre-scheduled flow (it started) and no active flows
    bool noMoreTimeout = false;
    if (par("waitToConverge").doubleValue() == 0) {
        if (lastSendTime < simTime().dbl() and activeFlows.size() == 0) {
            noMoreTimeout = true;
        }
    } else {
        // in Convergence Time experiments
        // if we've sent the last (after waiting to converge) flow message
        // and no active flows
        if (flowMsgs.empty() and activeFlows.size() == 0) {
            noMoreTimeout = true;
        }
    }

    if (!noMoreTimeout) scheduleAt(simTime() + statsTimeoutInterval, statsTimeout);

}
// delete unsent messages, timeouts
void CharnyFlowGenerator::finish() {
    cancelAndDelete(checkOptimalMsg);
    checkOptimalMsg = NULL;

    cancelAndDelete(statsTimeout);
    statsTimeout = NULL;

    // Delete unsent flow messages
    while (!flowMsgs.empty()) {
        CharnyMakeFlowMsg* nextMsg = flowMsgs.back();
        flowMsgs.pop_back();
        delete nextMsg;
        nextMsg = NULL;
    }

}

// schedule start/end flows messages for self to fwd
void CharnyFlowGenerator::sendFlowMsgs(std::vector<FlowInfo> flows) {

    flowMsgs.clear();

    for (const auto& f: flows) {
        if (true) {


             assert((f.endTime >= 0 and f.duration == -1) or\
                    (f.endTime == -1 and f.duration > 0));

             if (f.endTime >= 0) {
                 std::stringstream msgName;
                 msgName.clear();
                 msgName.str("");
                 msgName << "end flow # " << f.flowId << " from " << f.ends.first << " to " << f.ends.second << ".\n";

                 CharnyMakeFlowMsg* flowMsg = new CharnyMakeFlowMsg(msgName.str().c_str());
                 int src = f.ends.first; int dest = f.ends.second;
                 flowMsg->setSource(src);
                 flowMsg->setDestination(dest);
                 flowMsg->setFlowId(f.flowId);
                 flowMsg->setChangeType(CHARNY_MAKEFLOW_END);
                 flowMsg->setSendTime(f.endTime);
                 flowMsg->setDuration(f.duration);
                 flowMsg->setRate(f.initialRate);
                 flowMsg->setMinRtt(longestRtt);
                 flowMsgs.push_back(flowMsg);

                 EV3 << "Scheduling " << flowMsg->getName() << " to send at time " << f.endTime << ".\n";

          }
            if (f.startTime >= 0) {
                std::stringstream msgName;
                msgName.clear();
                msgName.str("");
                msgName << "start flow # " << f.flowId << " from " << f.ends.first << " to " << f.ends.second << ".\n";

                CharnyMakeFlowMsg* flowMsg = new CharnyMakeFlowMsg(msgName.str().c_str());
                int src = f.ends.first; int dest = f.ends.second;
                flowMsg->setSource(src);
                flowMsg->setDestination(dest);
                flowMsg->setFlowId(f.flowId);
                flowMsg->setChangeType(CHARNY_MAKEFLOW_START);
                flowMsg->setSendTime(f.startTime);
                flowMsg->setDuration(f.duration);
                flowMsg->setRate(0);
                flowMsg->setMinRtt(longestRtt);
                flowMsgs.push_back(flowMsg);

                 EV3 << "Scheduling " << flowMsg->getName() << " to send at time " << f.startTime << ".\n";

            }
     }
    }
    if (!flows.empty()) {
        FlowInfo f = flows.back();
        EV1 << "Flow # " << f.flowId << " starts at " << f.startTime << " and ends at " << f.endTime << ".\n";
    } else {
        assert(false);
    }

    // Sort descending, and pop from back..
    std::sort(flowMsgs.begin(), flowMsgs.end(), \
            [](const CharnyMakeFlowMsg* a, const CharnyMakeFlowMsg* b) -> bool
            { return a->getSendTime() > b->getSendTime(); }); // if a's time is bigger return true


    //showNumberOfFlowsOverTime();


    scheduleNextFlowMsg();
    if (par("waitToConverge").doubleValue() == 0) {
        // also schedule others now
        EV3 << "Also scheduling others now.\n";
       while(!flowMsgs.empty()) {
           scheduleNextFlowMsg();
        }
    } else EV3 << "Not scheduling more flow msgs, wait "\
        << par("waitToConverge").doubleValue() <<" to converge first.\n";

}


// do waterfilling from scratch for active flows, return number of iterations
int CharnyFlowGenerator::getMaxMinRates(std::map<int, FlowInfo> flows) {
    std::map<ActiveSessionId, Ends> activeSessionMap;
            for (auto& f: flows) {
                //EV1 << "Flow #" << f.flowId << " from "\
                //        << f.ends.first << " to " << f.ends.second << ".\n";
                activeSessionMap[f.first] = f.second.ends;
            }

       WaterfillingIterationState wfis = wf.doWaterfilling(activeSessionMap);
       optimalRate = wfis.rate;
       bottleneckRound  = wfis.sessionSaturatedInIter;
       EV1 << "Waterfilling took " << wfis.iter+1 << " iterations-\n";
       EV1 << wfis.show() << ".\n";
       return wfis.iter + 1 ;

}


// every RTT, checks if last received rates are optimal
// if start or end flow message from self sends it out
// if rate change message from a data host
// if wanna start or just ended message from data host, does water-filling updates optimal
// and if in ideal mode, sends out optimal rates to hosts
// TODO: update all hosts to send messages on start and FIN-ACK
void CharnyFlowGenerator::handleMessage(cMessage *msg)
{

    // if RCP .. then allRatesWithinOptimal(0.1)  --> scheduleNextFlowMsg
    // if MP ..  then allRatesWithinOptimal(0.10) --> maybe emit within 10% signal
    //               ...                   (0.0) --> scheduleNextFlowMsg
    // (decouple convergence time from new flow time)

    // emit converged message exactly once after flow change
    // when allRatesWithin(0.1) for last 20 RTTs
    // okay if multiple converged -- what's convergence time then?

    if (!std::strcmp(msg->getName(), "statsTimeout")) {
        stats_timeout();
        return;
    } else if (!std::strcmp(msg->getName(), "checkOptimalMsg")) {

        double total = 0;
        for (const auto& ar: actualRate) {
            total += ar.second;
        }
        emitSignal("totalSendingRatePerRtt", total);

        //cancelEvent(checkOptimalMsg);
        double nowTime = simTime().dbl();

        rttsSinceLastFlowChange = (nowTime - lastFlowChangeTime)/longestRtt;
        if (lastFlowChangeTime >= 0 and notYetOptimalWithinX and\
                allRatesWithinOptimal(par("epsc").doubleValue())) {

            beenOptimalWithinXForLast++;

            EV << "They've been within " << par("epsc").doubleValue()\
                    << " of optimal for the last " <<\
                    beenOptimalWithinXForLast/float(checkOptimalsPerRTT)<< "RTTs.\n";

            if (beenOptimalWithinXForLast/float(checkOptimalsPerRTT) >=\
                    par("waitToConverge").doubleValue()) {
                double timeToXConvergeAndWait = (nowTime -lastFlowChangeTime);
                double numRttsToXConvergeAndWait = timeToXConvergeAndWait/longestRtt;
                notYetOptimalWithinX = false;
                emitSignal("optimalWithinXRtts", numRttsToXConvergeAndWait);
                emitSignal("optimalWithinXTime", timeToXConvergeAndWait);

                double numRttsToXConverge = numRttsToXConvergeAndWait - par("waitToConverge").doubleValue();
                // Flow rates converged to optimal after lastFlowChange..
                if (lastFlowChangeType != -1) {
                    if (lastFlowChangeType == CHARNY_MAKEFLOW_SYN)
                        flowStats.at(lastFlowChangeId).synRttsToXConverge = numRttsToXConverge;
                    else flowStats.at(lastFlowChangeId).finRttsToXConverge = numRttsToXConverge;
                }
            }
        } else {
            beenOptimalWithinXForLast = 0;
        }

        double eps = par("eps").doubleValue();
        if (eps == -1) eps = 0;
        double epsc = par("epsc").doubleValue();
        if (epsc == -1) eps = 0;
        if (eps < epsc) {
                if (allRatesWithinOptimal(par("eps").doubleValue())) {
                    assert(allRatesWithinOptimal(par("epsc").doubleValue()));
                }

        if (notYetOptimalWithinX) {
            assert(notYetOptimal);
            }
        }


        if (lastFlowChangeTime >= 0 and notYetOptimal and\
                allRatesWithinOptimal(par("eps").doubleValue())) {
            beenOptimalForLast++;

            EV << "They've been within " << par("eps").doubleValue()\
                    << " of optimal for the last " <<\
                    beenOptimalForLast/float(checkOptimalsPerRTT)<< "RTTs.\n";

            if (beenOptimalForLast/float(checkOptimalsPerRTT)  >=\
                    par("waitToConverge").doubleValue()) {
                double timeToConvergeAndWait = (nowTime -lastFlowChangeTime);
                double numRttsToConvergeAndWait = timeToConvergeAndWait/longestRtt;

                notYetOptimal = false;
                emitSignal("optimalRtts", numRttsToConvergeAndWait);
                emitSignal("optimalTime", timeToConvergeAndWait);

                double numRttsToConverge = numRttsToConvergeAndWait - par("waitToConverge").doubleValue();
                // Flow rates converged to optimal after lastFlowChange..
                if (lastFlowChangeType != -1) {
                    if (lastFlowChangeType == CHARNY_MAKEFLOW_SYN)
                        flowStats.at(lastFlowChangeId).synRttsToConverge = numRttsToConverge;
                    else {
                        flowStats.at(lastFlowChangeId).finRttsToConverge = numRttsToConverge;
                        FlowInfo& f = flowStats.at(lastFlowChangeId);
                        emitSignal("flowFinId", f.flowId);
                        emitSignal("flowFinRttsToXConverge", f.finRttsToXConverge);
                        emitSignal("flowFinRttsToConverge", f.finRttsToConverge);
                        emitSignal("flowFinBottleneck", f.finBottleneck);
                    }
                }

                scheduleNextFlowMsg();

            }
        } else {
            beenOptimalForLast = 0;
        }

        if (actualRate.size() > 0 or optimalRate.size() > 0)
            scheduleAt(nowTime+longestRtt/float(checkOptimalsPerRTT), checkOptimalMsg);
        return;
    }

    CharnyMakeFlowMsg *flowMsg = check_and_cast<CharnyMakeFlowMsg *>(msg);



    int flowId = flowMsg->getFlowId();

    EV3 << "Received MakeFlow message of type "\
                << charnyMakeflowType[flowMsg->getChangeType()]\
                << " for flow " << flowId << ".\n";

    if (msg->isSelfMessage() and\
            (flowMsg->getChangeType() == CHARNY_MAKEFLOW_START or\
                        flowMsg->getChangeType() == CHARNY_MAKEFLOW_END)) {
        send(flowMsg, "gate$o", flowMsg->getSource());
        return;
        } else if (!msg->isSelfMessage() and\
            flowMsg->getChangeType() ==  CHARNY_MAKEFLOW_CHANGE_RATE and\
            finSent.count(flowId) == 0) {
            // but doesn't mean anything if it sent FIN already.
        double rate = flowMsg->getRate();
        if (optimalRate.count(flowId) > 0 and\
                rateWithin(optimalRate.at(flowId), rate, par("eps").doubleValue())) {
            double diff = fabs(optimalRate.at(flowId) - rate)/optimalRate.at(flowId);
            EV3 << "Flow # " << flowId\
                    << "[" << bottleneckRound.at(flowId) << "]"\
                    << " just converged to "\
                    << rate\
                    <<" which is the optimal rate "\
                    << optimalRate.at(flowId) << " after "\
                    << prd(SIMTIME_DBL(simTime())/(longestRtt), 2)\
                    << " RTTs! (within "\
                    << prd(diff*100, 1) << "% of optimal) \n";
        } else {
            int bottleneckNumber = -1;
            if (bottleneckRound.count(flowId) > 0)
                bottleneckNumber = bottleneckRound.at(flowId);
            EV3 <<  "Flow # " << flowId\
                    << " [" <<  bottleneckNumber<< "]"\
                    << " just changed rate to " << rate << " after "\
                    << prd(SIMTIME_DBL(simTime())/(longestRtt), 2)\
                    << " RTTs ";
            if (optimalRate.count(flowId) > 0) {
                double diff = fabs(optimalRate.at(flowId) - rate);
                double fraction = diff/optimalRate.at(flowId);
                EV3 <<  "but  is within " << prd(fraction*100,1)\
                        << "% of optimal " << optimalRate.at(flowId)
                << " (" <<  diff << ") apart.\n";
            }
            else EV3 << " but flow not in set of optimal rates.\n";
        }


        if (actualRate.count(flowId) == 0)
            actualRate.insert(std::pair<int, double>(flowId, rate));
        else
            actualRate.at(flowId) = rate;

        double total = 0;
        for (const auto ar: actualRate) {
            total += ar.second;
        }
        emitSignal("totalSendingRate", total);


    } else if (!msg->isSelfMessage() and\
               (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN\
                       or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIRST_DATA\
                       or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN\
                       or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK)) {

        bool valid = true;

        // just about to send FIN .. so Flow Gen should start seeing effects in next
        // 1-2 min RTTs.
        double time = simTime().dbl();
        std::string rtts = prd(time/(longestRtt), 2);
        switch(flowMsg->getChangeType()) {

            case CHARNY_MAKEFLOW_SYN:
            {
                EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " about to send SYN.\n";

                if (flowStats.count(flowId) == 0) {
                    flowStats[flowId] = FlowInfo();
                    flowStats.at(flowId).flowId = flowId;
                    flowStats.at(flowId).duration = flowMsg->getDuration();
                }

                if (flowStats.at(flowId).synTime == -1)
                    flowStats.at(flowId).synTime = time;
                else emitSignal("dupSyn", flowId);

                 flowArrival += flowMsg->getDuration();
                 emitSignal("flowArrival", flowMsg->getDuration());
                break;
            }
            case CHARNY_MAKEFLOW_FIRST_DATA:
            {
                if (flowStats.at(flowId).startTime == -1)
                    flowStats.at(flowId).startTime = time;
                else emitSignal("dupFirstData", flowId);
                EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " about to send first data.\n";

                break;
            }
            case CHARNY_MAKEFLOW_FIN:
            {
                if (flowStats.at(flowId).finTime == -1)
                    flowStats.at(flowId).finTime = time;
                else emitSignal("dupFin", flowId);
                EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " about to send FIN.\n";

                break;
            }
            case CHARNY_MAKEFLOW_FINACK:
            {
                if (flowStats.at(flowId).endTime == -1)
                    flowStats.at(flowId).endTime = time;
                else emitSignal("dupFinack", flowId);
                EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " just got FINACK.\n";

                bool stopRcpSwitch = false;
                if (par("nodeType").stdstringValue().find("Rcp")  != std::string::npos) {
                    // in FCT experiments
                    // if we've sent the last pre-scheduled flow (it started) and no active flows
                    if (par("waitToConverge").doubleValue() == 0) {
                        if (lastSendTime < simTime().dbl() and activeFlows.size() == 0) {
                            stopRcpSwitch = true;
                        }
                    } else {
                        // in Convergence Time experiments
                        // if we've sent the last (after waiting to converge) flow message
                        // and no active flows
                        if (flowMsgs.empty() and activeFlows.size() == 0) {
                            stopRcpSwitch = true;
                        }
                    }

                if (stopRcpSwitch) {
                    for (int i = 0; i < topoId.size(); i++) {
                        cMessage* stopMsg = new cMessage("StopSwitch");
                        EV3 << "sending stop messages to RCP switches.\n";
                        send(stopMsg, "gate$o", i);
                    }
                }
                }
                const FlowInfo& f = flowStats.at(flowId);

                emitSignal("flowSynId", f.flowId);
                emitSignal("flowSynTime", f.synTime);
                emitSignal("flowSynSize", f.duration);
                emitSignal("flowFirstDataTime", f.startTime);
                emitSignal("flowFinTime", f.finTime);
                emitSignal("flowFinackTime", f.endTime);
                emitSignal("flowSynWaterfillingInc", f.synWaterfillingInc);
                emitSignal("flowFinWaterfillingInc", f.finWaterfillingInc);
                emitSignal("flowSynRttsToXConverge", f.synRttsToXConverge);
                emitSignal("flowSynRttsToConverge", f.synRttsToConverge);

                emitSignal("flowSynBottleneck", f.synBottleneck);



                double fct = f.endTime-f.synTime;
                if (f.endTime == -1 or f.synTime == -1)
                    fct = -1;
                emitSignal("flowCompletionTime", fct);
                break;
            }
        }


        // update actual rates: FIN means no more packets after this
        if ((flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN and !par("idealMode").boolValue()) or\
                (flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK and par("idealMode").boolValue())) {
            valid &= (activeFlows.count(flowId)> 0);
            if (activeFlows.count(flowId)> 0) {
                actualRate.erase(flowId); // might already have been erased when we sent END msg

                double total = 0;
                for (const auto ar: actualRate) {
                    total += ar.second;
                }
                emitSignal("totalSendingRate", total);

                finSent[flowId] = simTime().dbl();
            }
        }

        // compute optimal rates on SYN or FIN
        // (whoever said they want to start or they want to send last packet)
        if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN) {
            valid &= (activeFlows.count(flowId) == 0);
            if (valid) {
                activeFlows.insert(std::pair<int, FlowInfo>(flowId, allFlows.at(flowId)));
                emitSignal("activeFlows", activeFlows.size());
                emitSignal("activeFlowsTime", simTime().dbl());
                EV3 << "Added " << flowId << " to active flows.\n";
            }

        }

        if (((flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN and !par("idealMode").boolValue())\
                or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK and par("idealMode").boolValue())) {
            valid &= (activeFlows.count(flowId)> 0);
        if (valid) {
            assert(activeFlows.count(flowId)> 0);
            activeFlows.erase(flowId);

            emitSignal("activeFlows", activeFlows.size());
            emitSignal("activeFlowsTime", simTime().dbl());
            EV3 << "Removed " << flowId << " to active flows.\n";
        }

        }

        // do waterfilling only on SYN or FIN
        if ((\
                (flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN and !par("idealMode").boolValue())\
                or ((flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK and par("idealMode").boolValue()))\
                or flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN) and valid) {
            double now = simTime().dbl();
            lastFlowChangeTime = now;
            lastFlowChangeId = flowMsg->getFlowId();
            lastFlowChangeType = flowMsg->getChangeType();

            notYetOptimalWithinX = true;
            notYetOptimal = true;
            beenOptimalWithinXForLast = 0;
            beenOptimalForLast = 0;
            EV3 << "Waterfilling.\n";
            // record waterfillingInc info
            int bottleneckNumber = -1;
            if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN\
                    or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK) {
                assert(bottleneckRound.count(flowId) > 0);
                bottleneckNumber = bottleneckRound.at(flowId);
                flowStats.at(flowId).finBottleneck = bottleneckNumber;
            }

            int waterfillingIterations = getMaxMinRates(activeFlows);
            emitSignal("waterfilling", waterfillingIterations);

            if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN ) {
                assert(bottleneckRound.count(flowId) > 0);
                bottleneckNumber = bottleneckRound.at(flowId);
                flowStats.at(flowId).synBottleneck = bottleneckNumber;
            }

            if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN )
                flowStats.at(flowId).synWaterfillingInc = waterfillingIterations-bottleneckNumber;
            else flowStats.at(flowId).finWaterfillingInc = waterfillingIterations-bottleneckNumber;


            emitSignal("bottleneck", bottleneckNumber);
            emitSignal("waterfillingInc", waterfillingIterations-bottleneckNumber);

            // Sending messages if in ideal mode
            if (par("idealMode").boolValue() and\
                    (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN\
                            or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK)) {
                std::vector<CharnyMakeFlowMsg*> rateChangeMsgs;
                getRateChangeMessages(flowMsg, rateChangeMsgs);


                if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN) {
                    flowMsg->setChangeType(CHARNY_MAKEFLOW_CHANGE_RATE);
                    EV3 << "Set initial rate for flow " << flowMsg->getFlowId() << " to " << flowMsg->getRate() << ".\n";
                    send(flowMsg, "gate$o", flowMsg->getSource());
                }
                double jitterMax = longestRtt * par("jitterByRttForIdealRateChange").doubleValue();
                while (!rateChangeMsgs.empty()) {
                    CharnyMakeFlowMsg* nextMsg = rateChangeMsgs.back();
                    rateChangeMsgs.pop_back();
                    double jitter = uniform(0, jitterMax, 2); // rtt  >= 10x transmission time
                    nextMsg->setSendTime(simTime().dbl()+jitter);
                    EV3 << "Sending rate change message to other flow "  << nextMsg->getFlowId() << " with rate " << nextMsg->getRate() << ".\n";
                    sendDelayed(nextMsg, jitter, "gate$o", nextMsg->getSource());
                }
               } // ideal Mode
            } // SYN OR FIN/ FINACK

        if (!valid) {
            emitSignal("buggyEvent", flowMsg->getFlowId());
        }
    } // SYN FIRST_DAT FIN OR FINACK

    if (!flowMsg->isScheduled())
        delete flowMsg;
    // should flow-gen compute optimal rates when it sends out start messages
    // or at computed end times
    // or when it receives start messages and end messages?
    // when there's zero delay in ideal mode .. think it doesn't matter
    // channel to flow-gen should have zero delay anyways

}


void CharnyFlowGenerator::emitSignal(std::string name, double value) {
    emit(signals.at(name), value);
    EV << "At time "<< simTime() << ", emitting " << name << ": " << value << "\n";
}

void CharnyFlowGenerator::emitSignal(std::string name, const char* value) {
    emit(signals.at(name), value);
    EV << "At time "<< simTime() << ", emitting " << name << ": " << value << "\n";
}


void CharnyFlowGenerator::getRateChangeMessages(CharnyMakeFlowMsg* flowMsg,\
        std::vector<CharnyMakeFlowMsg*>& rateChangeMsgs) {

    assert(par("idealMode").boolValue());
    int flowId = flowMsg->getFlowId();
    if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN)
        flowMsg->setRate(optimalRate.at(flowId));


    for (const auto& pr: optimalRate) {
        if (pr.first != flowId) {
            FlowInfo& f = allFlows.at(pr.first);
            std::stringstream msgName;
            msgName.clear();
            msgName.str("");
            msgName << "start flow # " << f.flowId << " from " << f.ends.first << " to " << f.ends.second << ".\n";
            CharnyMakeFlowMsg* rateChangeMsg = new CharnyMakeFlowMsg(msgName.str().c_str());
            int src = f.ends.first; int dest = f.ends.second;
            rateChangeMsg->setSource(src);
            rateChangeMsg->setDestination(dest);
            rateChangeMsg->setFlowId(f.flowId);
            rateChangeMsg->setChangeType(CHARNY_MAKEFLOW_CHANGE_RATE);

            rateChangeMsg->setDuration(f.duration);
            rateChangeMsg->setRate(pr.second);
            rateChangeMsg->setMinRtt(longestRtt);
            rateChangeMsgs.push_back(rateChangeMsg);
        }
    }



}

bool CharnyFlowGenerator::allRatesWithinOptimal(double eps) {
    double nowRTTs = simTime().dbl()/longestRtt;
    EV2 << "At " << nowRTTs << " RTTs, checking if rates within " << eps << " of optimal.\n";
    for (auto& ar : actualRate) {
        if (!optimalRate.count(ar.first) and ar.second > 0) {
            EV2 << "Flow # " << ar.first << " has actual rate " << ar.second << " but not in optimal rates.\n";
            return false;
        }
        if (!rateWithin(optimalRate.at(ar.first), ar.second, eps)) {
            // !rateEqual(optimalRate.at(ar.first), ar.second)
            EV2 << "Flow # " << ar.first << " has actual rate " << ar.second << " far from optimal rate " << optimalRate.at(ar.first) << ".\n";
            return false;
        }
    }
    for (auto& pr : optimalRate) {
            if (!actualRate.count(pr.first) and pr.second > 0) {
                EV2 << "Flow # " << pr.first << " has optimal rate " << pr.second << " but not in actual rates.\n";
                return false;
            }
            if (!rateWithin(pr.second, actualRate.at(pr.first), eps)) {
                // !rateEqual(actualRate.at(pr.first), pr.second)
                EV2 << "Flow # " << pr.first << " has optimal rate " << pr.second << " far from actual rate " << actualRate.at(pr.first) << ".\n";
                return false;
            }
    }
    return true;
}

void CharnyFlowGenerator::scheduleNextFlowMsg() {
    if (flowMsgs.empty()) return;
    CharnyMakeFlowMsg* nextMsg = flowMsgs.back();
    flowMsgs.pop_back();
    if (par("waitToConverge").doubleValue() > 0) {
        nextMsg->setSendTime(SIMTIME_DBL(simTime()));
        scheduleAt(simTime(), nextMsg);

    } else {
        if (nextMsg->getSendTime() > lastSendTime)
            lastSendTime = nextMsg->getSendTime();
        scheduleAt(nextMsg->getSendTime(), nextMsg);
    }

    std::stringstream ss;
    ss.clear(); ss.str("");
    ss << nextMsg->getSendTime() << ": " << charnyMakeflowType[nextMsg->getChangeType()] << " "\
            << nextMsg->getFlowId() << ": " << nextMsg->getName();
    EV3 << "At time " << prd(SIMTIME_DBL(simTime())/(longestRtt), 2) <<\
            " RTTs scheduling " << ss.str() << " (" << flowMsgs.size() << " flow messages left.)\n";
}
#endif // EV1
#endif // EV2
#endif // EV3
}; // namespace

//python -c 'import fileinput; time = [0]; map(lambda line: time.append(float(line.split()[3])-time[-1]), fileinput.input()); print time'
