//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __RCP_CHARNYFLOWGENERATOR_H
#define __RCP_CHARNYFLOWGENERATOR_H

#include <omnetpp.h>
#include "waterfilling.h"
#include "util.h"


namespace rcp {

typedef std::pair<int, int> pairInt;

class FlowInfo {
public:
    int flowId;
    pairInt ends;
    double duration; // size in flow stats
    double initialRate;
    double startTime; // FIRST_DATA in flow stats
    double endTime; // FIN_ACK in flow stats

    double synTime;
    double finTime;

    // on start: RTTs to converge, after this was added
    // .. its bottleneck number in new setup
    // .. total bottlenecks in new setup - its bottleneck number
    double synWaterfillingInc;
    double synBottleneck;
    double synRttsToConverge;
    double synRttsToXConverge;

    double finWaterfillingInc;
    double finBottleneck;
    double finRttsToConverge;
    double finRttsToXConverge;

    FlowInfo() : flowId(-1), ends(Ends(-1,-1)), duration(-1), initialRate(-1),\
            startTime(-1), endTime(-1), synTime(-1), finTime(-1), synWaterfillingInc(-1),\
            synBottleneck(-1), synRttsToConverge(-1), synRttsToXConverge(-1), finWaterfillingInc(-1), finBottleneck(-1),\
            finRttsToConverge(-1), finRttsToXConverge(-1){}
    FlowInfo(int flowId, pairInt ends, double duration, double initialRate, double startTime,\
            double endTime = -1) :\
            flowId(flowId), ends(ends), duration(duration), initialRate(initialRate),\
            startTime(startTime), endTime(endTime), synTime(-1), finTime(-1), synWaterfillingInc(-1),\
            synBottleneck(-1), synRttsToConverge(-1), synRttsToXConverge(-1), finWaterfillingInc(-1), finBottleneck(-1),\
            finRttsToConverge(-1), finRttsToXConverge(-1) {}
};


class CharnyFlowGenerator : public cSimpleModule
{
    private:
        std::map<std::string, simsignal_t> signals;

        double header_size_;
        double payload_size_;

        double rttsSinceLastFlowChange;
        double lastFlowChangeTime;
        int lastFlowChangeId;
        int lastFlowChangeType;

        double notYetOptimalWithinX;
        double notYetOptimal;
        double beenOptimalWithinXForLast;
        double beenOptimalForLast;
        double lastSendTime;

        double flowArrival;
        cMessage* statsTimeout;
        SimTime statsTimeoutInterval;

        double longestRtt;
        std::map<Link, double> linkCapacity;
        std::map<int, int> topoId;
        Waterfilling wf;
        std::map<int, double> optimalRate;
        std::map<int, int> bottleneckRound;
        std::map<int, double> actualRate;
        std::map<int, double> finSent;

        std::map<int, FlowInfo> flowStats;

        std::map<int, FlowInfo> activeFlows;
        std::vector<CharnyMakeFlowMsg*> flowMsgs;
        int beenOptimalSince; // n RTTs

    protected:
      double getLongestRtt() {return longestRtt;}
      std::map<int, FlowInfo> allFlows;
      virtual void initialize();
      void finish();
      virtual void handleMessage(cMessage *msg);
      void getAllPathsForWaterfilling();
      // populates optimalRate, bottleneckRound, returns number of rounds
      int getMaxMinRates(std::map<int, FlowInfo> flows);

      // gets rate change messages with optimal rates
      void getRateChangeMessages(CharnyMakeFlowMsg* flowMsg,\
              std::vector<CharnyMakeFlowMsg*>& rateChangeMsgs);

      void desynchronizeFlows(std::vector<FlowInfo>& flows);
      void makeAllPairsLongFlows(std::vector<int> endHosts, std::vector<FlowInfo>& flows);
      void makeAllPairsRandomFlows(std::vector<int> endHosts, std::vector<FlowInfo>& flows);
      void makePseudoStaticWorkload(std::vector<int> endHosts, std::vector<FlowInfo>& flows); // TO TEST
      void makeRandomFlows(int maxFlows, int totalChanges, const std::vector<int>& endHosts, std::vector<FlowInfo>& flows);
      Ends getRandomEnds(const std::vector<int>& endHosts);

      void checkParameters(std::vector<std::string> defaultParams);
      void showNumberOfFlowsOverTime();
      void scheduleNextFlowMsg();
      bool allRatesWithinOptimal(double eps);


      void initializeCharnySim1(); // one 80kbps 100ms delay link, n long flows start at 0, end at endTime
      void initializeCharnySim10(); // one 80kbps 100ms delay link, n long flows start at 0, end at 50-50 long/short duration
      void initializeCharnySim2(); // linear 5 switches/hosts, 3 long flows 2-hops from 0, 1, 2 so three links have 2 flows each.
      void initializeCharnySim3(); // Lisa's example, pairs of end hosts (0,1) and (3,4) connected through a 80Gbps 20us 2-3 link.
                                   // Start with two long lived flows from 0 to 3 and 4, then add third from 1 to 5.
      void initializeCharnySim5(); // 2-level bottleneck example from Anna Charny's thesis
      void initializeCharnySim6(); // 2 TORs connected to an Agg via 200 Gbps/100us; 4 hosts to each TOR via 25 Gbps/100us
      void initializeCharnySim7(); // 4 end hosts connected to a TOR via a 25Gbps link, simplest DC topology
      void initializeCharnySim70(); // 4 end hosts connected to a TOR via a 25Gbps link, simplest DC topology

      void initializeCharnySim8();
      void initializeCharnySim9();
      void initializeCharnySim11(); // one long flow and background traffic with rate xx Mbps and yy flows
      void initializeCharnySim15(); // for flow completion times experiment

      void initializeCharnySim20(); // microb many linsk flows

      void sendFlowMsgs(std::vector<FlowInfo> flows);

      void emitSignal(std::string name, double value);
      void emitSignal(std::string name, const char* value);

      std::vector<std::string> statNames;
      void setupSignals();

      CharnyMakeFlowMsg* getCharnyMakeFlowMsg(const char *msgName, double sendTime, bool start, int flowId,\
              int src, int dest);
      cMessage* checkOptimalMsg;
      int checkOptimalsPerRTT;

      void stats_timeout();
};


void orderFlowsByStartTime(std::vector<FlowInfo>& flows);
void orderFlows(std::vector<FlowInfo>& flows);

}; // namespace

#endif
