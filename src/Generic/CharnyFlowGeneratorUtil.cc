//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <algorithm>
#include "RandomSet.h"
namespace rcp {

bool flowStartsBefore(const FlowInfo& f1, const FlowInfo& f2) {
    return (f1.startTime < f2.startTime);
}

void CharnyFlowGenerator::desynchronizeFlows(std::vector<FlowInfo>& flows) {
    assert(par("startDatarate").doubleValue() != -1);
    assert(par("startDelay").doubleValue() != 1);

    double controlTxTime = header_size_/ par("startDatarate").doubleValue();;
    double dataTxTime = (payload_size_+header_size_)/ par("startDelay").doubleValue();;
    double linkRtt = par("startDelay").doubleValue() * 2;

    double mean = std::min(linkRtt, 10*dataTxTime);
    double sd = 0.5 * mean;

    EV << "datarate of start link: " << par("startDatarate").doubleValue() << " Bps.\n";
    EV << "delay of start link: " << par("startDelay").doubleValue() << " s.\n";
    EV << "Link RTT: "  << linkRtt\
            << " and 10 * controlTxTime: " << 10 * controlTxTime\
            << " and 10 * dataTxTime: " << 10 * dataTxTime << ".\n";
    EV << "Mean delay: " << mean << " and sd " << sd << ".\n";

    for (auto& f: flows) {

        double delay = normal(mean, sd, 0); // 0 is random seed
        while(not(\
                f.startTime < f.startTime+delay\
                and f.startTime+delay < f.startTime+linkRtt)) delay = normal(mean, sd, 0);
        f.startTime += delay;
        EV << "Delayed flow # " << f.flowId << " by " << delay\
                << "s, to start at " << f.startTime\
                << "s instead of " << f.startTime - delay << "s.\n";

    }
    std::sort(flows.begin(), flows.end(), flowStartsBefore);
}

  void CharnyFlowGenerator::makeAllPairsLongFlows(std::vector<int> endHosts, std::vector<FlowInfo>& flows) {
      assert(par("duration").doubleValue() == -1);
      assert(par("initialRate").doubleValue() == 1);
      assert(par("endTime").doubleValue() != -1);

    std::map<Ends, int> numSessions;
    int numEndHosts = endHosts.size();
    double endTime = par("endTime").doubleValue() * longestRtt;

    for (int sourceIndex = 0; sourceIndex < numEndHosts; sourceIndex++) {
          for (int destinationIndex = 0; destinationIndex < numEndHosts; destinationIndex ++) {
	    int source = endHosts[sourceIndex];
	    int destination = endHosts[destinationIndex];
	    if (destination == source) continue;
	    numSessions[Ends(source, destination)] = 1;
          }
    }

    for (auto& ns : numSessions) {
        FlowInfo f(0, pairInt(0,1), par("duration"), par("initialRate"), 0.0, endTime);
        f.ends = ns.first;
        for (int j = 0; j < ns.second; j++)
            flows.push_back(f);
    }

    int i = 0; for (auto& flow: flows) flow.flowId = i++;

}


Ends CharnyFlowGenerator::getRandomEnds(const std::vector<int>& endHosts) {
            int numEndHosts = endHosts.size();
            int sourceIndex = intuniform(0, numEndHosts-1, 0); // rng-0
            int destIndex = intuniform(0, numEndHosts-2, 1); // rng-1
            if (destIndex >= sourceIndex) destIndex += 1;
            int source = endHosts.at(sourceIndex);
            int destination = endHosts.at(destIndex);
            return Ends(source, destination);
   }

  void CharnyFlowGenerator::makeRandomFlows(int maxFlows, int totalChanges, const std::vector<int>& endHosts, std::vector<FlowInfo>& flows) {
      int numChanges = 0;

      std::map<int, FlowInfo> flowsMap;
      RandomSet<int> activeFlows;

      while(numChanges < totalChanges) {
          bool addFlow = false;
          if (activeFlows.size() < maxFlows || (activeFlows.size() == maxFlows and bernoulli(0.5,2))) addFlow = true; // rng-2

          if (addFlow) {
              FlowInfo f(flowsMap.size(), getRandomEnds(endHosts), par("duration"), par("initialRate"), -1, -1);
              f.startTime = numChanges;
              assert(flowsMap.count(f.flowId) == 0);
              flowsMap[f.flowId] = f;
              activeFlows.insert(f.flowId);
              EV << "Adding random flow # " << f.flowId << " from " << f.ends.first << " to " << f.ends.second << ".\n";
          } else {
              int pos = intuniform(0, activeFlows.size()-1, 3);
              int flowId = activeFlows.getElement(pos); // rng-3
              activeFlows.remove(flowId);
              FlowInfo& f = flowsMap.at(flowId);
              f.endTime = numChanges;
              EV << "Removing flow # " << flowId << " from " << f.ends.first << " to " << f.ends.second << ".\n";
          }
          numChanges++;
      }
      for (auto& fm : flowsMap) {
          if (fm.second.endTime == -1) fm.second.endTime = numChanges++;
          assert(fm.second.startTime != -1 and fm.second.endTime != -1);
          flows.push_back(fm.second);
      }

  }

  void CharnyFlowGenerator::makeAllPairsRandomFlows(std::vector<int> endHosts, std::vector<FlowInfo>& flows) {
      assert(par("duration").doubleValue() == -1);
      assert(par("initialRate").doubleValue() == 1);
      assert(par("endTime").doubleValue() != -1);
      assert(par("lambda").doubleValue() != -1);
      double lambda = par("lambda").doubleValue()/longestRtt;
      // inter-arrival time in longestRTTs say 2longestRTTs, then lambdaInPerLongestRTT is 0.5
      // lambdaInPerS = 0.5/longestRTT


      bool mixed = (par("longDuration").doubleValue() != -1 and par("shortDuration").doubleValue() != -1);
      int numEndHosts = endHosts.size();
      double simTime = par("endTime").doubleValue() * longestRtt;

    for (int sourceIndex = 0; sourceIndex < numEndHosts; sourceIndex++) {
        for (int destinationIndex = 0; destinationIndex < numEndHosts; destinationIndex ++) {
            std::stringstream ss; ss.clear(); ss.str("");
            int numFlows = 0;
            int source = endHosts[sourceIndex];
            int destination = endHosts[destinationIndex];
            if (destination == source) continue;
            double startTime = 0;
            double duration = -1;
            double endTime = startTime + duration;
            int tries = 0;
            while(startTime <= simTime and tries <= 10) { // one flow every 2RTTs .. number of flows around 10/2 etc.
                startTime += exponential(1.0/lambda);

                if (mixed) {
                    if (random()%2 == 0) duration = par("longDuration").doubleValue() * longestRtt;
                        else duration = par("shortDuration").doubleValue() * longestRtt;
                    endTime = startTime + duration;
                } else {
                    endTime = simTime;
                }


                if (startTime < endTime and endTime <= simTime) {
                    FlowInfo f(0, pairInt(source, destination), par("duration"),\
                            par("initialRate"), startTime, endTime);
                    ss << "(" << startTime << "s to  " << endTime << "s)";
                    flows.push_back(f);
                    numFlows++;
                } else {
                    EV << "Start time is " << startTime\
                            << ", and duration is "<< duration\
                            << " but sim must end by " << simTime  << ".\n";
                }
            }
            EV << numFlows << " flows between " << source << " and " << destination << " have start times :" \
                        << ss.str() << ".\n";

        }
    }

    orderFlowsByStartTime(flows);
}

void CharnyFlowGenerator::makePseudoStaticWorkload(std::vector<int> endHosts, std::vector<FlowInfo>& flows) {
       assert(par("duration").doubleValue() == -1);
       assert(par("initialRate").doubleValue() == 1);
       assert(par("endTime").doubleValue() != -1);
       assert(par("lambda").doubleValue() != -1);
       double lambda = par("lambda").doubleValue()/longestRtt;

       bool mixed = (par("longDuration").doubleValue() != -1 and par("shortDuration").doubleValue() != -1);
       int numEndHosts = endHosts.size();
       double simTime = par("endTime").doubleValue() * longestRtt;

       double startTime = 0;
       double duration = -1;
       double endTime = startTime + duration;

       while(startTime < endTime and endTime <= simTime) {
           int sourceIndex = random()%numEndHosts;
           int destIndex = random()%(numEndHosts-1);
           if (destIndex <= sourceIndex) destIndex += 1;
           int source = endHosts[sourceIndex];
           int destination = endHosts[destIndex];
           startTime += exponential(1.0/lambda);
           if (mixed) {
               if (random()%2 == 0) duration = par("longDuration").doubleValue() * longestRtt;
                   else duration = par("shortDuration").doubleValue() * longestRtt;
               endTime = startTime + duration;
           } else {
               endTime = simTime;
           }

           if (startTime < endTime and endTime <= simTime) {
               FlowInfo f(0, pairInt(source, destination), par("duration"),\
                       par("initialRate"), 0.0, endTime);
               flows.push_back(f);
           }
       }

     orderFlowsByStartTime(flows);
 }

void CharnyFlowGenerator::showNumberOfFlowsOverTime() {

    std::stringstream ss;
    ss.clear(); ss.str();
    /*
    for (std::vector<CharnyMakeFlowMsg*>::reverse_iterator f = flowMsgs.rbegin(); f != flowMsgs.rend(); f++)  {
        CharnyMakeFlowMsg* flowMsg = *f;
        ss << flowMsg->getSendTime() << ": " << (flowMsg->getStart() ? "start " : "end ")\
              << flowMsg->getFlowId() << ", ";
    }
    EV << "Flow start/ end times " << ss.str() << ".\n";
     */
    ss.clear(); ss.str();

    int count = 0;

    std::map<Link, int> flowsOnLink;
    std::map<Link, std::stringstream> flowsOnLinkSs;

    for (auto& link : wf.allLinks) {
        flowsOnLink[link] = 0;
        flowsOnLinkSs[link].clear(); flowsOnLinkSs.at(link).str("");
    }


    for (std::vector<CharnyMakeFlowMsg*>::reverse_iterator f = flowMsgs.rbegin(); f != flowMsgs.rend(); f++) {
        if ((*f)->getChangeType() == CHARNY_MAKEFLOW_START) count++;
        else count--;
        ss << count << " ";


        Ends ends = Ends((*f)->getSource(), (*f)->getDestination());
        Session s = wf.endsToSessionMap.at(ends);
        std::vector<Link> sessionPath = wf.sessionPaths.at(s);


        for (auto& link: sessionPath) {

            if ((*f)->getChangeType() == CHARNY_MAKEFLOW_START) flowsOnLink.at(link)++;
                    else flowsOnLink.at(link)--;
            flowsOnLinkSs.at(link) << flowsOnLink.at(link) << " (" << (*f)->getSendTime() << ")  ";

        }
    }

    EV << "Number of flows in network over time: " << ss.str() << ".\n";
    EV << "Number of flows on each link over time: \n";
    for (auto& link : wf.allLinks) {
        EV << "Link (" << link.first << ", " << link.second << "): " << flowsOnLinkSs.at(link).str() << ".\n";
    }
    // TODO number of flows on each link over time
}
void orderFlows(std::vector<FlowInfo>& flows) {
    int i = 0; for (auto& flow: flows) flow.flowId = i++;
}
void orderFlowsByStartTime(std::vector<FlowInfo>& flows) {
    std::sort(flows.begin(), flows.end(),\
            [](const FlowInfo& a, const FlowInfo& b) -> bool
            { return a.startTime > b.startTime; });
    int i = 0; for (auto& flow: flows) flow.flowId = i++;
    return;
}

} /* namespace rcp */
