//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package src.Generic;

simple CharnyDataHost
{
    parameters:
        bool hasControlHost = default(true);
        int headerSize;
        int payloadSize;
        
        @signal[session*-NumPkts](type=int); // note the wildcard
		@statisticTemplate[sessionNumPkts](record=vector);
		@signal[session*-TransmissionRate](type=double); // note the wildcard
		@statisticTemplate[sessionTransmissionRate](record=vector);

        @signal[numPacketsSent](type="double");
        @signal[start](type="long");
        @signal[end](type="long");
        @signal[length](type="int");

		@statistic[totalNumPacketsSent](title="total num packets sent"; source = "numPacketsSent"; record=vector; interpolationmode=none);
        @statistic[flowStart](title="flow start"; source="start"; record=vector,stats; interpolationmode=none);
        @statistic[flowEnd](title="flow end"; source="end"; record=vector,stats; interpolationmode=none);
        @statistic[packetLength](title="packetLength"; source="length"; record=vector,stats; interpolationmode=none);


        @display("i=block/routing");
        // int capacity = default(-1);    // negative capacity means unlimited queue
        // bool fifo = default(true);     // whether the module works as a queue (fifo=true) or a stack (fifo=false)

    gates:
        inout gate;
        inout flowAlert;
        inout controlGate;
        //inout dataGate;
}
simple CharnyFlowGenerator
{
    parameters:
        int headerSize;
        int payloadSize;
    
    		double load = default(0.8);
		string cdfFile = default("DCTCP_CDF");   
        // number of waterfilling iterations for change

		double statsTimeoutInterval @unit(s);

		@signal[dupSyn](type=int);
        @statistic[dupSyn](title="dupSyn"; source="dupSyn"; record=vector; interpolationmode=none);

		@signal[dupFin](type=int);
        @statistic[dupFin](title="dupFin"; source="dupFin"; record=vector; interpolationmode=none);

		@signal[dupFinack](type=int);
        @statistic[dupFinack](title="dupFinack"; source="dupFinack"; record=vector; interpolationmode=none);

		@signal[dupFirstData](type=int);
        @statistic[dupFirstData](title="dupFirstData"; source="dupFirstData"; record=vector; interpolationmode=none);


		@signal[buggyEvent](type=double);
        @statistic[buggyEvent](title="buggyEvent"; source="buggyEvent"; record=vector; interpolationmode=none);

		@signal[flowArrival](type=double);
        @statistic[flowArrival](title="flowArrival"; source="flowArrival"; record=vector; interpolationmode=none);
   
   		@signal[flowArrivalRate](type=double);
        @statistic[flowArrivalRate](title="flowArrivalRate"; source="flowArrivalRate"; record=vector; interpolationmode=none);
  
		
        @signal[totalSendingRate](type=double);
        @statistic[totalSendingRate](title="totalSendingRate"; source="totalSendingRate"; record=vector; interpolationmode=none);
        
        @signal[totalSendingRateRtt](type=double);
        @statistic[totalSendingRatePerRtt](title="totalSendingRatePerRtt"; source="totalSendingRatePerRtt"; record=vector; interpolationmode=none);

        @signal[waterfilling](type=int);
        @statistic[waterfilling](title="waterfilling"; source="waterfilling"; record=vector; interpolationmode=none);

        @signal[optimalWithinXRtts](type=int);
        @statistic[optimalWithinXRtts](title="optimalWithinXRtts"; source="optimalWithinXRtts"; record=vector; interpolationmode=none);

		@signal[optimalWithinXTime](type=int);
        @statistic[optimalWithinXTime](title="optimalWithinXTime"; source="optimalWithinXTime"; record=vector; interpolationmode=none);

        @signal[optimalRtts](type=int);
        @statistic[optimalRtts](title="optimalRtts"; source="optimalRtts"; record=vector; interpolationmode=none);

		@signal[optimalTime](type=int);
        @statistic[optimalTime](title="optimalTime"; source="optimalTime"; record=vector; interpolationmode=none);

       
        @signal[bottleneck](type=int);
        @statistic[bottleneck](title="bottleneck"; source="bottleneck"; record=vector; interpolationmode=none);
 
 // "waterfilling", "bottleneck", "waterfillingInc",\
//            "activeFlows", "flowSynTime", "flowFirstDataTime", "flowFinTime",\
//            "flowFinackTime","flowSynSize", "activeFlowsTime"
 
  		// number of new waterfilling iterations (waterfilling-bottleneck)
        @signal[waterfillingInc](type=int);
        @statistic[waterfillingInc](title="waterfillingInc"; source="waterfillingInc"; record=vector; interpolationmode=none);
        
        @signal[activeFlows](type="long");
        @statistic[activeFlows](title="activeFlows"; source="activeFlows"; record=vector; interpolationmode=none);

		@signal[activeFlowsTime](type="long");
        @statistic[activeFlowsTime](title="activeFlowsTime"; source="activeFlowsTime"; record=vector; interpolationmode=none);

 		@signal[flowSynSize](type="long");
        @statistic[flowSynSize](title="flowSynSize"; source="flowSynSize"; record=vector; interpolationmode=none);
        
		@signal[flowSynTime](type="long");
        @statistic[flowSynTime](title="flowSynTime"; source="flowSynTime"; record=vector; interpolationmode=none);

		@signal[flowSynId](type="long");
        @statistic[flowSynId](title="flowSynId"; source="flowSynId"; record=vector; interpolationmode=none);

		@signal[flowFirstDataTime](type="long");
        @statistic[flowFirstDataTime](title="flowFirstDataTime"; source="flowFirstDataTime"; record=vector; interpolationmode=none);
 
 		@signal[flowFirstDataId](type="long");
        @statistic[flowFirstDataId](title="flowFirstDataId"; source="flowFirstDataId"; record=vector; interpolationmode=none);
 		
  
 		@signal[flowFinTime](type="long");
        @statistic[flowFinTime](title="flowFinTime"; source="flowFinTime"; record=vector; interpolationmode=none);
  
		@signal[flowFinId](type="long");
        @statistic[flowFinId](title="flowFinId"; source="flowFinId"; record=vector; interpolationmode=none);
  
  
  		@signal[flowFinackTime](type="long");
        @statistic[flowFinackTime](title="flowFinackTime"; source="flowFinackTime"; record=vector; interpolationmode=none);
     
    		@signal[flowFinackId](type="long");
        @statistic[flowFinackId](title="flowFinackId"; source="flowFinackId"; record=vector; interpolationmode=none);
       	
       	@signal[flowCompletionTime](type="long");
       	@statistic[flowCompletionTime](title="flowCompletionTime"; source="flowCompletionTime"; record=vector; interpolationmode=none);
       	
       	@signal[flowSynWaterfillingInc](type="double");
		@statistic[flowSynWaterfillingInc](title="flowSynWaterfillingInc"; source="flowSynWaterfillingInc"; record=vector; interpolationmode=none);

		@signal[flowFinWaterfillingInc](type="double");
		@statistic[flowFinWaterfillingInc](title="flowFinWaterfillingInc"; source="flowFinWaterfillingInc"; record=vector; interpolationmode=none);

		@signal[flowSynRttsToXConverge](type="double");
		@statistic[flowSynRttsToXConverge](title="flowSynRttsToXConverge"; source="flowSynRttsToXConverge"; record=vector; interpolationmode=none);

		@signal[flowSynRttsToConverge](type="double");
		@statistic[flowSynRttsToConverge](title="flowSynRttsToConverge"; source="flowSynRttsToConverge"; record=vector; interpolationmode=none);

		@signal[flowFinRttsToXConverge](type="double");
		@statistic[flowFinRttsToXConverge](title="flowFinRttsToXConverge"; source="flowFinRttsToXConverge"; record=vector; interpolationmode=none);

		@signal[flowFinRttsToConverge](type="double");
		@statistic[flowFinRttsToConverge](title="flowFinRttsToConverge"; source="flowFinRttsToConverge"; record=vector; interpolationmode=none);

		@signal[flowSynBottleneck](type="double");
		@statistic[flowSynBottleneck](title="flowSynBottleneck"; source="flowSynBottleneck"; record=vector; interpolationmode=none);

		@signal[flowFinBottleneck](type="double");
		@statistic[flowFinBottleneck](title="flowFinBottleneck"; source="flowFinBottleneck"; record=vector; interpolationmode=none);
       	@signal[flowStats](type="char*"; source="flowStats"; record=scalar);
       	
       	
        //@statistic[flowStats](title="flowStats"; source="flowStats"; record=vector; interpolationmode=none);
       		        
        
        int duration = default(-1);
        double initialRate = default(0);
        int sim;
       
       
       	double ratio = default(-1); // RTT to Packet Transmission time ratio for those experiments
       	string nodeType = default("rcp.CharnyNode");
        int numSessions = default(-1);
        
        double endTime = default(-1);
        double longDuration = default(-1);
        double shortDuration = default(-1);
        
        double lambda = default(-1);
     
     	int numHosts = default(-1);
        int maxFlows = default(-1);
        int numChanges = default(-1);
        int waitToConverge = default(0);
       	double eps = default(-1); // converge means equal to or within eps fraction of optimal
       	double epsc = default(-1); // converge means equal to or within eps fraction of optimal
       	
        int checkOptimalsPerRTT = default(5);
        bool desynchronize = default(false);
        
        bool idealMode = default(false);
        double jitterByRttForIdealRateChange = default(0);
        
        int N = default(-1); // size for Charny8/ Mohammad's example

		double backgroundRate = default(-1); // as a fraction of total rate
		double numBackgroundFlows = default(-1); // as a fraction of total number of flows
        
    gates:
        inout gate[];
}


simple GenericControlHost
{
    parameters:
        int conservative;
        int headerSize;
        int payloadSize;
              
        @signal[sourceReceivePacket](type="double");
        @signal[start](type="long");
        @signal[end](type="long");
        @signal[length](type="int");
        
        @signal[session*-StampedRate](type=double); // note the wildcard
		@statisticTemplate[sessionStampedRate](record=vector);
        

		@statistic[stampedRate](title="stamped rate"; source="sourceReceivePacket"; record=vector; interpolationmode=none);
        @statistic[flowStart](title="flow start"; source="start"; record=vector,stats; interpolationmode=none);
        @statistic[flowEnd](title="flow end"; source="end"; record=vector,stats; interpolationmode=none);
        @statistic[packetLength](title="packetLength"; source="length"; record=vector,stats; interpolationmode=none);


        @display("i=block/routing");
        // int capacity = default(-1);    // negative capacity means unlimited queue
        // bool fifo = default(true);     // whether the module works as a queue (fifo=true) or a stack (fifo=false)

    gates:
        inout gate;
        inout flowAlert;
        //inout dataGate;
 }
simple GenericSwitch
{
    parameters:
        @class(GenericSwitch);
        @display("i=block/queue;q=queue");
    
        @signal[switch*-outputGateTo*-QueueLength](type=double); // note the wildcard
		@statisticTemplate[switchOutputGateToQueueLength](record=vector?,stats,histogram);
		
		@signal[switch*-outputGateTo*-ControlMsgsInQueue](type=double); // note the wildcard
		@statisticTemplate[switchOutputGateToControlMsgsInQueue](record=vector?,stats,histogram);
		
		@signal[switch*-outputGateTo*-DataMsgsInQueue](type=double); // note the wildcard
		@statisticTemplate[switchOutputGateToDataMsgsInQueue](record=vector?,stats,histogram);
		  
  
        //int buffercap = 20; // not sure - not lav
        bool prioritizeControl=default(true);
        
    gates:
        inout local;
        inout dataLocal;
        inout port[];
}



