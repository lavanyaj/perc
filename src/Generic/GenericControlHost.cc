#include "GenericControlHost.h"
#include <assert.h>

namespace rcp {
Define_Module(GenericControlHost);


void GenericControlHost::initialize()
{
    sourceReceivePacketSignal = registerSignal("sourceReceivePacket");
    startSignal = registerSignal("start");
    endSignal = registerSignal("end");
    lengthSignal = registerSignal("length");
    address = getParentModule()->getIndex();
    header_size_ = par("headerSize").doubleValue();
    payload_size_ = par("payloadSize").doubleValue();
    pktSize = header_size_ + payload_size_;

    const char *vinit[] = {"StampedRate"};
    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));

}

// TRY WITHOUT TIMEOUTS
void GenericControlHost::handleMessage(cMessage *msg)
{
    if (msg->arrivedOn("flowAlert$i")) { // from Data Host to start or end a flow
        CharnyMakeFlowMsg *flowmsg = check_and_cast<CharnyMakeFlowMsg *>(msg);
        EV << "Local " << address << ": Arrived on Flow Alert for flow # " << flowmsg->getFlowId() << "\n";
        assert( flowmsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_DEMAND or\
                flowmsg->getChangeType() == CHARNY_MAKEFLOW_START or\
                flowmsg->getChangeType() == CHARNY_MAKEFLOW_END);
        if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_START) setupSession(flowmsg);
        else if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_END) tearDownSession(flowmsg);
        else if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_DEMAND) updateDemand(flowmsg);
    } else { // from other (control) host
        GenericControlPacket *oldpkt = check_and_cast<GenericControlPacket *>(msg);
        // HELLO or DATA or FIN packet
        if (oldpkt->getPktType() == CHARNY_HELLO || oldpkt->getPktType() == CHARNY_FIN) {
            recvHello(oldpkt);
        }
    }
    delete msg;

}

void GenericControlHost::tearDownSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    if (pIActiveSessions.count(flowId) == 0) {
        EV << "Session # " << flowId << " doesn't have any state.\n";
        return;
    }
    // send FIN next time
    pIActiveSessions[flowId].nextPacket = CHARNY_FIN;
}

void GenericControlHost::updateDemand(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    if (pIActiveSessions.count(flowId) == 0) {
        EV << "Session # " << flowId << " doesn't have any state.\n";
        return;
    }
    // update number of packets left to sent
    pIActiveSessions[flowId].numPacketsLeft = msg->getDuration();
    double packetSizeBits = 8 * (pktSize);
    assert(pIActiveSessions[flowId].minRtt > 0);
    double demand = round((msg->getDuration() * packetSizeBits)/pIActiveSessions[flowId].minRtt);
    assert(demand < INFINITE_RATE);
    EV << "Update demand of flow # " << flowId << " to "\
                << demand << " Bps "\
                << " since " << msg->getDuration() << " packets left."\
                << " (last demand was " << pIActiveSessions[flowId].demand << ").\n";
    pIActiveSessions[flowId].demand = demand;
}

void GenericControlHost::setupSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();


    GenericControlHostSessionState session(INFINITE_RATE, true, flowId);

    session.destination = msg->getDestination();
    session.src = msg->getSource();
    session.flowmsg = *msg;
    session.minRtt = msg->getMinRtt();

    setupSignals(session, flowId);
    EV << "setting up " << session.signals.size() << " signals for flow # "  << flowId << ".\n";

    protocolSpecificSetupSession(msg);
    pIActiveSessions[flowId] = session;
    if (msg->getDuration() != -1) updateDemand(msg);

    EV << "Setup session for flow # " << flowId << " with demand "\
            << pIActiveSessions.at(flowId).demand << ".\n";
    // also need to setup session in destination, when it gets first Hello


    EV << "Session # " << flowId << " now has  state.\n";
    sendHello(flowId, pIActiveSessions.at(flowId).nextPacket);
}

void GenericControlHost::setupSignals(GenericControlHostSessionState& temp, int flowId) {
    for (const auto& statName : statNames ) {
        std::stringstream ss;
        ss.clear(); ss.str("");
        ss << "session-" << flowId << "-" << statName;
        temp.signals[ss.str()] = registerSignal(ss.str().c_str());

        std::stringstream ts;
        ts.clear(); ts.str("");
        ts << "session" << statName;

        cProperty *statisticTemplate =\
        getProperties()->get("statisticTemplate", ts.str().c_str());
        ev.addResultRecorders(this, temp.signals[ss.str()],\
                ss.str().c_str(), statisticTemplate);
    }

}

void GenericControlHost::emitSignal(int flowId, std::string name, double value) {
    std::stringstream ss; ss.clear(); ss.str("");
    ss << "session-" << flowId << "-" << name;
    emit(pIActiveSessions[flowId].signals[ss.str()], value);
    EV << "At time "<< simTime() << ", emitting " << ss.str() << ": " << value << "\n";
 }


void GenericControlHost::sendHello(int flowId, int pktType) {
    // source generates packet with session.stamped rate, session.markedBit
    // destination creates feedback packet with its ...

    GenericControlHostSessionState &session = pIActiveSessions[flowId];
    char pktname[200];
    int packetDestination = session.isForward ? session.destination :\
                                    session.src;
    std::string direction = session.isForward ? "fwd": "fb";
    sprintf(pktname, "control-%s-flow#-%d-from-%d-to-%d-%s",\
            charnyPktType[pktType].c_str(),\
            flowId,\
            address,\
            packetDestination,\
            direction.c_str());
            /* //-rate-%.2f-%smarked",\
            session.stampedRate,\
            session.isForward ? "": "un");
             */
    GenericControlPacket *pkt = newProtocolSpecificControlPacket(pktname, flowId);
    pkt->setFlowId(flowId);
    pkt->setSource(address);
    pkt->setDestination(packetDestination);

    pkt->setPktType(pktType);
    pkt->setByteLength(header_size_); // syn length

    pkt->setIsForward(session.isForward);
    pkt->setIsExit((pktType == CHARNY_FIN));

    EV << "Host " << pkt->getSource() << " sending " << charnyPktType[pkt->getPktType()]\
            << " for flow # " << flowId << " to "\
                << pkt->getDestination() << ".\n";
    session.numSent++;
    forwardPacket(pkt);
}


void GenericControlHost::recvHello(const GenericControlPacket *pkt) {
    int flowId = pkt->getFlowId();

    if (pIActiveSessions.count(flowId) == 0) {
        GenericControlHostSessionState session(INFINITE_RATE, false, flowId);
        session.src = pkt->getSource();
        session.destination = pkt->getDestination();
        pIActiveSessions[pkt->getFlowId()] = session;
        addProtocolSpecificActiveSession(pkt);
    }

    GenericControlHostSessionState &session = pIActiveSessions[flowId];
    // If this is the destination, update state at destination and send feedback
    // If this is the source, update state at source
    EV << "Host " << pkt->getDestination() << " received " << charnyPktType[pkt->getPktType()]\
                << " for flow # " << flowId << " from "\
                    << pkt->getSource() << ".\n";

    if (session.isForward) {
       EV << "Host " << pkt->getDestination() << " is the source for this flow.\n";
       sourceReceivePacketGeneral(pkt);
       // Source keeping pinging back until it receives a FIN (ACK)
       if (pkt->getPktType() != CHARNY_FIN)
           sendHello(flowId, session.nextPacket);
    } else if (!session.isForward) {
       EV << "Host " << pkt->getDestination() << " is the destination for this flow.\n";
       if (destinationReceivePacket(pkt)) {
         sendHello(flowId, pkt->getPktType());
        }
     }
     if (pkt->getPktType() == CHARNY_FIN) {
          pIActiveSessions.erase(pkt->getFlowId());
          removeProtocolSpecificActiveSession(pkt->getFlowId());
     }

}

void GenericControlHost::sendFlowMsg(int flowId, double newRate) {
    GenericControlHostSessionState &session = pIActiveSessions[flowId];
    CharnyMakeFlowMsg* msg = new CharnyMakeFlowMsg(session.flowmsg);
    msg->setRate(newRate);
    msg->setChangeType(CHARNY_MAKEFLOW_CHANGE_RATE);
    EV << "After " << session.numRecvd << " packets, sending "\
    << (session.dataRate > newRate ? "decrease" : "increase")\
    << " rate message for flow # " << flowId\
    << " from " << session.dataRate << " Bps to "\
    << newRate << " Bps at time " << simTime().dbl() << ".\n" ;
    session.dataRate = newRate;
    send(msg, "flowAlert$o");
}

void GenericControlHost::forwardPacket(GenericControlPacket *pkt)
{
    EV << "CharnyHost " << address << ": Forwarding message " << pkt << " on out\n";
    pkt->setKind(CHARNY_CONTROL_PKT);

    egressAction(pkt);
    send(pkt, "gate$o");
}


}; // namespace
