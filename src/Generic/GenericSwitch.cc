#include "GenericSwitch.h"

#include <assert.h>

namespace rcp {

#ifndef EV2
#define EV2 EV
#ifndef EV1
#define EV1 EV //if (false) std::cout

Define_Module(GenericSwitch);

PILinkState::PILinkState():
        linkCapacity(-1), controlMsgsInQueue(0), dataMsgsInQueue(0),\
                dequeueMessage(NULL) {

            WATCH(linkCapacity);
            WATCH(controlMsgsInQueue);
            WATCH(dataMsgsInQueue);
        }

PILinkState::~PILinkState() {
}

void PILinkState::makeId() {
    std::stringstream id;

    id.clear();
    id.str("");
    id << "switch-" << switchId\
                    << "-outputGate-" << gateIndex\
                    << "-ToSwitch-" << otherSwitchId;

    idStr = id.str();

}

std::string PILinkState::getId() {
    return idStr;
}

GenericSwitch::GenericSwitch() {
}

GenericSwitch::~GenericSwitch() {



}

int compareMessagePriority(cObject *obj1, cObject *obj2) {
    cMessage *msg1 = (cMessage *) obj1;
    cMessage *msg2 = (cMessage *) obj2;
    std::pair<int, simtime_t> pair1(msg1->getKind(), msg1->getTimestamp());
    std::pair<int, simtime_t> pair2(msg2->getKind(), msg2->getTimestamp());
    if (pair1 < pair2) return -1;
    else if (pair1 == pair2) return 0;
    else return 1;
}

cMessage* GenericSwitch::protocolSpecificProcessIngress(int outGateIndex, cMessage* msg){return msg;}
cMessage* GenericSwitch::protocolSpecificProcessEgress(int outGateIndex, cMessage* msg){
    EV << "In GenericSwitch::protocolSpecificProcessEgress(" << outGateIndex << ", ..)\n";
    return msg;}

void GenericSwitch::finish() {
    EV << "Clearing Link State for Forwarding/ Switching (PI).\n";
    for (auto ls : ltable) {
        cMessage* dequeueMessage = ls.second->dequeueMessage;
        if (dequeueMessage) {
            if (dequeueMessage->isScheduled()) {
                EV << "Cancel dequeue packet permanently.\n";
                cancelAndDelete(dequeueMessage);
             } else {
                delete dequeueMessage;
             }
            ls.second->dequeueMessage = NULL;
        }

        delete ls.second;
        ls.second = NULL;
    }
    ltable.clear();
}
void GenericSwitch::initialize()
{
    address = getParentModule()->getIndex();
    numLinks = gateSize("port");

    // From Routing.cc
    cTopology *topo = new cTopology("topo");

    std::vector<std::string> nedTypes;
    nedTypes.push_back(getParentModule()->getNedTypeName());

    topo->extractByNedTypeName(nedTypes);
    EV << "Node " << address << ": cTopology found " << topo->getNumNodes() << " nodes\n";

    cTopology::Node *thisNode = topo->getNodeFor(getParentModule());
    int thisTopoId = -1;
    // find and store next hops
    for (int i=0; i<topo->getNumNodes(); i++)
    {
        if (topo->getNode(i)==thisNode) {thisTopoId = i; continue;} // skip ourselves
        topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(i));

        if (thisNode->getNumPaths()==0) continue; // not connected

        cGate *parentModuleGate = thisNode->getPath(0)->getLocalGate();
        int gateIndex = parentModuleGate->getIndex();
        //int address = topo->getNode(i)->getModule()->par("address");
        int address = topo->getNode(i)->getModule()->getIndex();
        rtable[address] = gateIndex;
        EV1 << "  towards address " << address << " gateIndex is " << gateIndex << endl;
    }

    const char *vinit[] = {"QueueLength", "ControlMsgsInQueue", "DataMsgsInQueue"};
    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));
    assert(thisTopoId >= 0);


    std::string thisNodeType = thisNode->getModule()->getParentModule()->getNedTypeName();
    for (int l= 0; l < thisNode->getNumOutLinks(); l++) {
        cTopology::LinkOut* linkOut = thisNode->getLinkOut(l);
        assert(linkOut);
        cTopology::Node* otherNode = linkOut->getRemoteNode();
        assert(otherNode);
        std::string otherNodeType = otherNode->getModule()->getParentModule()->getNedTypeName();
        if (otherNodeType.compare(thisNodeType) != 0) continue;
        int otherTopoId = otherNode->getModule()->getIndex();
        PILinkState *ls = new PILinkState();
        ls->switchId = address;
        ls->gateIndex = l;
        ls->otherSwitchId = otherTopoId;
        ls->makeId();
        setupSignals(*ls);
        ltable[l] = ls;
        EV << "Added new link state for link " << l << " from " << thisNodeType << " # " << address << " to "\
                << otherNodeType << " # " << otherTopoId << ".\n";
    }

    for (int gateIndex = 0; gateIndex < numLinks; gateIndex++) {
        cDatarateChannel *chan = check_and_cast<cDatarateChannel *> \
                    (getParentModule()->gate("port$o", gateIndex)->getChannel());
        double linkCapacity = chan->getDatarate();
        PILinkState *ls = ltable[gateIndex];
        ls->linkCapacity = linkCapacity;
        ls->dropSignal = registerSignal("drop");
        ls->outputIfSignal = registerSignal("outputIf");
        ls->dequeueMessage = new CharnyDequeueMessage("dequeueMessage");
        ls->dequeueMessage->setOutGateIndex(gateIndex);

        EV << "data rate of port # " << gateIndex << " is " << chan->getDatarate() << "Bps,"\
                << " delay is " << SIMTIME_DBL(chan->getDelay()) << "s.\n";
        EV << "setting link capacity of gate # " << gateIndex << " to " << linkCapacity << ".\n";

    }


    for (int gateIndex = 0; gateIndex < numLinks; gateIndex++) {
        CharnyDequeueMessage *dMsg = ltable[gateIndex]->dequeueMessage;
        EV2 << "outGateIndex # " << gateIndex\
                << " corresponds to link (" << ltable[gateIndex]->switchId\
                <<  ", "<< ltable[gateIndex]->otherSwitchId << "). "\
                << " Message has gateIndex " <<\
                dMsg->getOutGateIndex() << ".\n";

        outputQueues.push_back(cQueue("", compareMessagePriority));
    }


    //nextOutGateIndex = 0;
    delete topo;
}

void GenericSwitch::setupSignals(PILinkState& temp) {
    for (const auto& statName : statNames ) {
        std::stringstream ss;
        ss.clear(); ss.str("");
        ss << "switch-" << temp.switchId\
                << "-outputGateTo-" << temp.otherSwitchId
                << "-" << statName;
        temp.signals[ss.str()] = registerSignal(ss.str().c_str());

        std::stringstream ts;
        ts.clear(); ts.str("");
        ts << "switchOutputGateTo" << statName;

        cProperty *statisticTemplate =\
        getProperties()->get("statisticTemplate", ts.str().c_str());
        ev.addResultRecorders(this, temp.signals[ss.str()],\
                ss.str().c_str(), statisticTemplate);
    }

}

void GenericSwitch::emitSignal(int gateIndex, std::string name, double value) {
    std::stringstream ss; ss.clear(); ss.str("");
    ss << "switch-" << ltable[gateIndex]->switchId << "-outputGateTo-" << ltable[gateIndex]->otherSwitchId << "-" << name;
    emit(ltable[gateIndex]->signals[ss.str()], value);
    EV1 << "At time "<< simTime() << ", emiting " << ss.str() << ": " << value << "\n";
 }

// TODO dequeueMessage has output queue #, vector of dequeue messages
// on dequeueMessage .. call dequeue(output queue #)
// on dequeue(output queue #), check if queue is empty ..
// on fromPort or fromHost .. and !dequeue[output queue #] scheduled..
void GenericSwitch::handleMessage(cMessage *msg)
{
    EV1 << "GenericSwitch"<< this->getId() <<": handleMessage(" << msg->getName() << ").\n";
    // isSelfMessage not working..
    if (strcmp(msg->getName(), "dequeueMessage") == 0) {
          EV1 << "Got a dequeue message for link (..)!! ";


          int outGateIndex, destination;
          getOutInfo(msg, outGateIndex, destination);
          assert(destination == -1);
          EV1 << "Got a dequeue message for link (" << ltable[outGateIndex]->switchId\
                  <<  ", "<< ltable[outGateIndex]->otherSwitchId << ")!! ";

          cMessage *newmsg = dequeue(outGateIndex);

          if (newmsg) {
              getOutInfo(newmsg, outGateIndex, destination);

              EV1 << "QueueOut " << ltable[outGateIndex]->getId() << " -> Switch (" << this->getId()
                           << "): Send from Switch to Switch " << destination << ".\n";
              cMessage *newpkt = newmsg;

              if (newmsg->getKind() == CHARNY_CONTROL_PKT) {
                  newpkt = protocolSpecificProcessEgress(outGateIndex, newmsg);

              }
              simtime_t finishTime = sendToOther(newpkt);
              assert(finishTime >= simTime());
              finishTime = std::max(finishTime, simTime()); // why? finishTime always in the future unless not transmitting
              if (!ltable[outGateIndex]->dequeueMessage->isScheduled()) {
                  EV1 << "On sendToOther, scheduled dequeue "\
                          "from link (" << ltable[outGateIndex]->switchId\
                          <<  ", "<< ltable[outGateIndex]->otherSwitchId\
                          << ") at " << SIMTIME_DBL(finishTime) << ".\n";
                  scheduleAt(finishTime, ltable[outGateIndex]->dequeueMessage);
              }

          } else { // otherwise dequeueMessage is no longer scheduled
              EV1 << "Output queue is empty.\n";
          }
          return;
    } else {
        EV1 << "Not a dequeue message.\n";
    }

    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);


    if (fromPort(msg) and toHost(msg)) { // came from queue
        EV1 << "Port -> Switch (" << this->getId() << "): Send from Switch to Host.\n";
        sendToHost(msg);
        return;
    } else if ((fromPort(msg) or fromHost(msg)) and toOther(msg)) {
        EV1 << "Port xx -> Switch (" << this->getId()
                << "): Send from Switch to QueueIn " << ltable[outGateIndex]->getId() << ".\n";
        enqueue(msg);
        if (!ltable[outGateIndex]->dequeueMessage->isScheduled()) {
            EV1 << "On enqueue, scheduled dequeue "\
                    << "from link (" << ltable[outGateIndex]->switchId\
                    <<  ", "<< ltable[outGateIndex]->otherSwitchId << ")"\
                    " at " << SIMTIME_DBL(simTime()) << ".\n";
            scheduleAt(simTime(), ltable[outGateIndex]->dequeueMessage);
        } else {
            EV1 << "Dequeue from link (" << ltable[outGateIndex]->switchId\
                  <<  ", "<< ltable[outGateIndex]->otherSwitchId << ") already scheduled.\n";
        }
    } else {

        cGate * gate = msg->getArrivalGate();
        EV << "Don't know what to do with packet arrived on "\
                << gate->getFullName() << " going to " << destination\
                << " through output queue # " << outGateIndex << ".\n";

    }

    //delete pkt;
    }


bool GenericSwitch::fromPort(cMessage* msg) {
    bool ret = msg->arrivedOn("port$i");
    if (ret) EV1 << "Packet from input queue/ port.\n";
    return ret;
}
bool GenericSwitch::fromHost(cMessage* msg) {
    bool ret = msg->arrivedOn("local$i") || msg->arrivedOn("dataLocal$i");
    if (ret) EV1 << "Packet from host.\n";
    return ret;
}
bool GenericSwitch::fromFlowGen(cMessage* msg) {
    bool ret = msg->arrivedOn("flowAlert");
    if (ret) EV1 << "Packet from flow generator.\n";
    return ret;
}

void GenericSwitch::getOutInfo(cMessage* msg, int &outGateIndex, int &destination) {
    outGateIndex = -1;
    destination = -1;
    if (strcmp(msg->getName(), "dequeueMessage") == 0) {
        CharnyDequeueMessage *dMsg = check_and_cast<CharnyDequeueMessage *>(msg);
        outGateIndex = dMsg->getOutGateIndex();
    } else if (msg->getKind() == CHARNY_CONTROL_PKT) {
        GenericPacket *pkt = check_and_cast<GenericPacket *>(msg);
        outGateIndex = findOutGate(pkt->getDestination());
        destination = pkt->getDestination();
    } else if (msg->getKind() == CHARNY_DATA_PKT or msg->getKind() == CHARNY_DATA_PKT_SYN) {
        GenericPacket *pkt = check_and_cast<GenericPacket *>(msg);
        outGateIndex = findOutGate(pkt->getDestination());
        destination = pkt->getDestination();
    }

}

bool GenericSwitch::toOther(cMessage* msg) {
    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);
    bool ret = (destination != address) and outGateIndex != -1;
    if (ret) EV1 << "Packet to other.\n";
    return ret;
}

bool GenericSwitch::toHost(cMessage* msg) {
    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);
    bool ret = (destination == address);
    if (ret) EV1 << "Packet to host.\n";
    return ret;
}

simtime_t GenericSwitch::sendToOther(cMessage* msg) {
    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);

    cDatarateChannel *chan = check_and_cast<cDatarateChannel *> \
                            (getParentModule()->gate("port$o", outGateIndex)->getChannel());
    send(msg, "port$o", outGateIndex);
    return chan->getTransmissionFinishTime();
}

void GenericSwitch::sendToHost(cMessage* msg) {
    if (msg->getKind() == CHARNY_DATA_PKT or msg->getKind() == CHARNY_DATA_PKT_SYN)
        send(msg, "dataLocal$o");
    else send(msg, "local$o");
}

void GenericSwitch::enqueue(cMessage* msg) {
    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);

    msg->setTimestamp(simTime());
    EV1 << "Enqueued " << charnyPktTypeStr[msg->getKind()] << " message " <<\
            " onto output queue # " << outGateIndex <<\
            " with timestamp " << msg->getTimestamp() << ".\n";
    outputQueues[outGateIndex].insert(msg);
    emitSignal(outGateIndex, "QueueLength", outputQueues[outGateIndex].getLength());
            EV1 << "At time "<< simTime() << ", emitting outputGate"<< outGateIndex\
                    <<"-QueueLengthSignal: " << outputQueues[outGateIndex].getLength() << "\n";

    if (msg->getKind() == CHARNY_CONTROL_PKT or msg->getKind() == CHARNY_DATA_PKT_SYN) {
        ltable[outGateIndex]->controlMsgsInQueue++;
        emitSignal(outGateIndex, "ControlMsgsInQueue", ltable[outGateIndex]->controlMsgsInQueue);
    } else if (msg->getKind() == CHARNY_DATA_PKT) {
        ltable[outGateIndex]->dataMsgsInQueue++;
        emitSignal(outGateIndex, "DataMsgsInQueue", ltable[outGateIndex]->dataMsgsInQueue);
      }

    //send(msg, "ingress_end", outGateIndex); // store in queue
}

cMessage *GenericSwitch::dequeue(int nextOutGateIndex) {

    cMessage *ret;

    if (outputQueues[nextOutGateIndex].isEmpty()) {
        EV1 << "Output queue # "<< nextOutGateIndex << "  is empty.\n";
        ret = NULL;
    } else {

        ret = (cMessage *) outputQueues[nextOutGateIndex].pop();
        emitSignal(nextOutGateIndex, "QueueLength", outputQueues[nextOutGateIndex].getLength());
        EV1 << "At time "<< simTime() << ", emiting outputGate"<< nextOutGateIndex\
                <<"-QueueLengthSignal: " << outputQueues[nextOutGateIndex].getLength() << "\n";

        if (ret->getKind() == CHARNY_CONTROL_PKT or ret->getKind() == CHARNY_DATA_PKT_SYN) {
                ltable[nextOutGateIndex]->controlMsgsInQueue--;
                emitSignal(nextOutGateIndex, "ControlMsgsInQueue", ltable[nextOutGateIndex]->controlMsgsInQueue);
         } else if (ret->getKind() == CHARNY_DATA_PKT) {
                ltable[nextOutGateIndex]->dataMsgsInQueue--;
                emitSignal(nextOutGateIndex, "DataMsgsInQueue", ltable[nextOutGateIndex]->dataMsgsInQueue);
        }

        EV1 << "Dequeued " << charnyPktTypeStr[ret->getKind()] << " message " <<\
                " from output queue # " << nextOutGateIndex <<\
                " with timestamp " << ret->getTimestamp() << ".\n";
    }
    return ret;
}

int GenericSwitch::findOutGate(int destAddr)
{
    RoutingTable::iterator it = rtable.find(destAddr);
    if (it==rtable.end())
    {
        return -1;
    }
    return (*it).second; //outGateIndex
}

#endif // EV1
#endif // EV2
}; // namespace


// CharnySim8 original simulation ended at 157427, t=0.02054944
