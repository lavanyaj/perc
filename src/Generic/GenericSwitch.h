//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GENERIC_SWITCH_H
#define __GENERIC_SWITCH_H

#include <omnetpp.h>
#include "util.h"
#include "GenericSwitch.h"
#include "generic_m.h"
#include <map>

namespace rcp {


class SwitchSessionState {
public:
    double recordedRate;
    int numPackets;
    bool marked;
    int flowId;

    SwitchSessionState(double rr, int np, bool m, int fid) :
        recordedRate(rr), numPackets(np), marked(m), flowId(fid) {
    }
    SwitchSessionState() :
            recordedRate(0), numPackets(0), marked(false), flowId(-1) {
        }
};

class PILinkState {
    public:
    simsignal_t dropSignal;
    simsignal_t outputIfSignal;

    int switchId;
    int gateIndex;
    int otherSwitchId;
    std::string idStr;

    // Charny state
    double linkCapacity;
    bool eagerSwitchMode;

    int controlMsgsInQueue;
    int dataMsgsInQueue;
    CharnyDequeueMessage *dequeueMessage;

    std::map<std::string, simsignal_t> signals;

    PILinkState();
    ~PILinkState();
    std::string getId();
    void makeId();
};
/*
class ProtocolSpecificPILinkState {
    public:
    std::string idStr;

    typedef std::map<int, ProtocolSpecificSessionState> SessionTable;
    SessionTable activeSessions;

    double advertisedRate;
    int numForwardSessions;
    int numFeedbackSessions;



    ProtocolSpecificPILinkState();
    ~ProtocolSpecificPILinkState();

    CharnyPacket *ProtocolSpecificProcessIngress(cMessage *pkt);
    CharnyPacket *ProtocolSpecificProcessEgress(cMessage *pkt);


    void calculateAdvRate();
    double calculateAdvRateAndRevert();
    double calculateRate();
    double getMaxRate();
    double getTotalRate();

    void ProtocolSpecificLinkUpdateSessionExit(const cMessage *pkt);
    void ProtocolSpecificLinkUpdateNewSession(cMessage *pkt);
    void ProtocolSpecificLinkUpdateKnownSession(cMessage *pkt);
    void ProtocolSpecificLinkAction(cMessage *pkt);

};
*/


/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class GenericSwitch : public cSimpleModule
{

public:
    typedef std::map<int,int> RoutingTable; // destaddr -> gateindex
    RoutingTable rtable;
    int address; // CharnyHost index for now


    int numLinks;

    std::vector<cQueue> outputQueues;

    //int nextOutGateIndex;


    bool prioritizeControl;
    std::vector<std::string> statNames;


    typedef std::map<int,PILinkState*> PILinkStateTable; // output queue # -> link state
    PILinkStateTable ltable;
    // and a protocol specific link state table ..

    void initialize();
    void finish();
    void handleMessage(cMessage *msg);
    int findOutGate(int destAddr);
    void getOutInfo(cMessage* msg, int &outGateIndex, int &destination);

    bool fromPort(cMessage *msg);
    bool fromHost(cMessage *msg);
    bool fromFlowGen(cMessage *msg);
    bool toHost(cMessage *msg);
    bool toOther(cMessage *msg);
    simtime_t sendToOther(cMessage *msg);
    void sendToHost(cMessage *msg);

    void enqueue(cMessage *msg);
    cMessage *dequeue(int outGateIndex);
    //void scheduleDequeue();

    void setupSignals(PILinkState& temp);
    void emitSignal(int gateIndex, std::string name, double value);

    virtual cMessage* protocolSpecificProcessIngress(int outGateIndex, cMessage* msg);
    virtual cMessage* protocolSpecificProcessEgress(int outGateIndex, cMessage* msg);

public:
    GenericSwitch();
    ~GenericSwitch();
};

}; // namespace

#endif
