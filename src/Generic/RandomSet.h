#include <vector>
#include <map>
#include <assert.h>
#include <iostream>

// add, remove rand items from a set, all ops in O(1) time
// http://stackoverflow.com/questions/5682218/data-structure-insert-remove-contains-get-random-element-all-at-o1
template <typename T>
class RandomSet {
 private:
  std::map<T, int> positions;
  std::vector<T> values;

 public:
  void insert(T item);
  size_t size() {return values.size();}
  void remove(T item);
  bool contains(T item) {return (positions.count(item) > 0); }
  T getRandomElement() {int pos = rand()%values.size(); return values.at(pos);} // don't use
  T getElement(int pos) {assert (pos < values.size()); return values.at(pos);}
};

template <typename T>
void RandomSet<T>::insert(T item) {
    assert(positions.count(item) == 0);
    values.push_back(item);
    positions.insert(std::pair<T, int>(item, values.size()-1));
}


template <typename T>
void RandomSet<T>::remove(T item) {
    assert(positions.count(item) != 0);
    T lastItem = values.back();
    int pos = positions.at(item);
    values.at(pos) = lastItem;
    positions.at(lastItem)  = pos;
    values.pop_back();
    positions.erase(item);
}
