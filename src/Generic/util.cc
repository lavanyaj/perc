#include "util.h"
#include <cmath>

namespace rcp { // charny
bool rateGreaterThan(double rate1, double rate2) {
    if (fabs(rate1-rate2) < 10) return false;
    return (round(rate1)) > (round(rate2));
}

bool rateEqual(double rate1, double rate2) {
    return (fabs(rate1-rate2) < 10);
}

bool rateWithin(double rate1, double rate2, double eps) {
    if (eps == -1) return rateEqual(rate1, rate2);
    // e.g., eps = 0.1 means difference in rates within 10% of rate1
    double diff = fabs(rate1-rate2);
    double fraction = diff/rate1;

    return (fraction <= eps);
}

bool rateLessThan(double rate1, double rate2) {
    if (fabs(rate1-rate2) < 10) return false;
    return (round(rate1)) < (round(rate2));
}

bool rateGreaterThanOrEqual(double rate1, double rate2) {
    return rateEqual(rate1, rate2) or rateGreaterThan(rate1, rate2);
}

bool rateLessThanOrEqual(double rate1, double rate2) {
    return rateEqual(rate1, rate2) or rateLessThan(rate1, rate2);
}

// from http://stackoverflow.com/questions/1343890/rounding-number-to-2-decimal-places-in-c
std::string prd(const double x, const int decDigits) {
  std::stringstream ss;
  ss << std::fixed;
  ss.precision(decDigits); // set # places after decimal
  ss << x;
  return ss.str();
}
}; //namespace

