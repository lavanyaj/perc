#ifndef __CHARNY_UTIL_H
#define __CHARNY_UTIL_H

#include <omnetpp.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <cfloat> // DBL_MAX;
#include "charny_m.h"
#include "charnymakeflow_m.h"
#include <cmath>

namespace rcp { // charny
//#define CHARNY_HDR_BYTES 40  //same as TCP for fair comparison

enum CHARNY_MAKEFLOW_T {CHARNY_MAKEFLOW_START, CHARNY_MAKEFLOW_END,\
    CHARNY_MAKEFLOW_CHANGE_RATE, CHARNY_MAKEFLOW_CHANGE_DEMAND,\
    CHARNY_MAKEFLOW_SYN, CHARNY_MAKEFLOW_FIRST_DATA,\
    CHARNY_MAKEFLOW_FIN, CHARNY_MAKEFLOW_FINACK};

const std::string charnyMakeflowType[] = {"CHARNY_MAKEFLOW_START", "CHARNY_MAKEFLOW_END",\
        "CHARNY_MAKEFLOW_CHANGE_RATE", "CHARNY_MAKEFLOW_CHANGE_DEMAND",\
        "CHARNY_MAKEFLOW_SYN", "CHARNY_MAKEFLOW_FIRST_DATA",\
        "CHARNY_MAKEFLOW_FIN", "CHARNY_MAKEFLOW_FINACK"};

enum CHARNY_PKT_T {CHARNY_HELLO, CHARNY_DATA, CHARNY_FIN};
const std::string charnyPktType[] = {"CHARNY_HELLO", "CHARNY_DATA", "CHARNY_FIN"};
const double INFINITE_RATE = DBL_MAX;
const int K = 1; // K feedback sessions packets per forward session
const double HELLO_INTERVAL = 0.5;

enum CHARNY_PACKET_TYPE {
    CHARNY_DATA_PKT_SYN,
    CHARNY_CONTROL_PKT,
    CHARNY_DATA_PKT

};

const std::string charnyPktTypeStr[] = {"CHARNY_DATA_PKT_SYN", "CHARNY_CONTROL_PKT", "CHARNY_DATA_PKT"};
}; //namespace

namespace rcp { // rcp

//#define PKT_SIZE 1460

//static const int size_ = PKT_SIZE + RCP_HDR_BYTES;
const double bitsPerByte = 8*1e-6;
enum RCP_PKT_T {RCP_OTHER,
        RCP_SYN,
        RCP_SYNACK,
        RCP_REF,
        RCP_REFACK,
        RCP_DATA,
        RCP_ACK,
        RCP_FIN,
        RCP_FINACK};

const std::string rcpPktTypeStr[] ={"RCP_OTHER",
        "RCP_SYN",
        "RCP_SYNACK",
        "RCP_REF",
        "RCP_REFACK",
        "RCP_DATA",
        "RCP_ACK",
        "RCP_FIN",
        "RCP_FINACK"};

enum RCP_HOST_STATE {RCP_INACT,
             RCP_SYNSENT,
             RCP_CONGEST,
             RCP_RUNNING,
             RCP_RUNNING_WREF,
             RCP_FINSENT,
             RCP_RETRANSMIT,
             RCP_FINTRIGGER}; // if flowGenerator causes fin

const std::string rcpHostStateStr[] = {"RCP_INACT",
        "RCP_SYNSENT",
        "RCP_CONGEST",
        "RCP_RUNNING",
        "RCP_RUNNING_WREF",
        "RCP_FINSENT",
        "RCP_RETRANSMIT",
        "RCP_FINTRIGGER"};

enum RCP_TIMEOUT_STATE { NO_TIMEOUT,
    RCP_TIMEOUT,
    REF_TIMEOUT,
    RTO_TIMEOUT};

const std::string rcpTimeoutStateStr[] = { "NO_TIMEOUT",
        "RCP_TIMEOUT",
        "REF_TIMEOUT",
        "RTO_TIMEOUT"};

enum FLOW_STATE { RTT_, // double
    INTERVAL_, // double
    NUM_SENT_,
    RCP_STATE,
    NUMOUTREFS_,
    MIN_RTT_, //double
    REF_SEQNO_,

    NUM_DATAPKTS_ACKED_BY_RECEIVER_,
    NUM_DATAPKTS_RECEIVED_,
    NUM_PKTS_TO_RETRANSMIT_,
    NUM_PKTS_RESENT_,
    NUM_ENTER_RETRANSMIT_MODE_,

    SEQNO_,
    NUMPKTS_,
    LASTPKTTIME_,

    DESTINATION_
    //INIT_REFINTV_FIX_
    };





bool rateGreaterThan(double rate1, double rate2);

bool rateEqual(double rate1, double rate2);
bool rateWithin(double rate1, double rate2, double eps);

bool rateLessThan(double rate1, double rate2);

bool rateGreaterThanOrEqual(double rate1, double rate2);

bool rateLessThanOrEqual(double rate1, double rate2);

std::string prd(const double x, const int decDigits);

}; //namespace

#endif
