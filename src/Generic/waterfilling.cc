#include "waterfilling.h"
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <sstream>
#include <assert.h>
#include "util.h"

using rcp::prd;

WaterfillingIterationState::WaterfillingIterationState\
(const std::map<ActiveSessionId, ActiveSession>& activeSessionMap,
 const std::map<Session, std::vector<Link> >& sessionPaths):\
  iter(-1), activeSessionMap(activeSessionMap) {
  for (auto& as : activeSessionMap) {
    //std::cout << "Path from " << as.second.ends.first << " to " << as.second.ends.second << ": ";
    assert(sessionPaths.count(as.second.session) > 0);
    for (auto& link : sessionPaths.at(as.second.session)) {
      //std::cout << "(" << link.first << ", " << link.second << ") ";
      linkActiveSessions[link].push_back(as.first);
    }
    //std::cout << ".\n";
  }
}

std::string WaterfillingIterationState::show() {
  std::stringstream ss;
  ss << "Session rates ";
  for (auto& sessionRate : rate) {
      int iter = -1;
      if (sessionSaturatedInIter.count(sessionRate.first) > 0)
          iter = sessionSaturatedInIter.at(sessionRate.first);
      ss << prd(sessionRate.second, 2) << "  for # " << sessionRate.first << "  ["\
      <<  iter << "] ";
  }
  ss << ".\n";

  ss << "Link total rates ";
  for (auto& linkFlow : totalFlow) 
    ss << prd(linkFlow.second, 2) << " on "	\
	      << "(" << linkFlow.first.first		\
	      << ", " << linkFlow.first.second		\
	      << ")  ";
  ss << ".\n";
  return ss.str();
}


WaterfillingIterationState Waterfilling::doWaterfilling(std::vector<Ends> ends) {
  std::map<ActiveSessionId, ActiveSession> activeSessionMap;
  int id = 0;
  for (auto& e: ends) {
    ActiveSession as;
    as.ends = e;
    assert(endsToSessionMap.count(e) > 0);
    as.session = endsToSessionMap.at(e);
    as.id = id++;
    activeSessionMap[as.id] = as;
  }
  return WF(activeSessionMap);
}

WaterfillingIterationState Waterfilling::doWaterfilling(std::map<int, Ends> ends) {
  std::map<ActiveSessionId, ActiveSession> activeSessionMap;
    for (auto& idEnds: ends) {
      int id = idEnds.first;
      Ends e = idEnds.second;
      ActiveSession as;
      as.ends = e;
      assert(endsToSessionMap.count(e) > 0);
      as.session = endsToSessionMap.at(e);
      as.id = id;
      activeSessionMap[as.id] = as;
    }
    return WF(activeSessionMap);
}

WaterfillingIterationState Waterfilling::WF(std::map<ActiveSessionId, ActiveSession>\
					    activeSessionMap) {
  std::set<Session> uniquePaths;
  for (auto& as : activeSessionMap)
      uniquePaths.insert(as.second.session);

  /*EV << "Setting up Waterfilling algorithm for " << activeSessionMap.size() << " active sessions" \
          << " that use "<< uniquePaths.size() <<" paths from " << sessionPaths.size()\
          << " possible paths in the topology.\n";*/
  WaterfillingIterationState wfis(activeSessionMap, sessionPaths); // active sessions
  //EV << "Constructed wfis.\n";

  int iter = 0;


  initialize(wfis);
  //EV << "Initialized wfis, " << wfis.unsaturatedSessions.size()	\
  //        << " unsaturated sessions.\n";

  while (wfis.unsaturatedSessions.size() > 0) {
    //std::cout << "iteration " << iter << "...\n";
    update(iter++, wfis);
    wfis.show();
  }
  //EV << "Finished waterfilling after " << wfis.iter << " iterations...\n";
  //EV << wfis.show();
  return wfis;
}

void Waterfilling::initialize(WaterfillingIterationState& wfis) {
  wfis.iter = 0;

  // update rate
  for (const auto& as : wfis.activeSessionMap) 
    wfis.rate[as.first] = 0;
  
  // update totalFlow
  for (auto& las : wfis.linkActiveSessions)
    wfis.totalFlow[las.first] = 0;

  // update unsaturatedLinks
  wfis.unsaturatedLinks.clear();
  for (auto& las : wfis.linkActiveSessions)
          wfis.unsaturatedLinks.insert(las.first);

  // sessions that can still increase
  wfis.unsaturatedSessions.clear();
  for (auto& as : wfis.activeSessionMap)
    wfis.unsaturatedSessions.insert(as.first);

  // number of unsaturated sessions using link
  for (auto& las : wfis.linkActiveSessions) {
      EV << "Sessions using link (" << las.first.first << ", " << las.first.second << "): ";
    assert(wfis.linkActiveSessions.count(las.first) > 0);
    for (auto& as : las.second)
        EV << as << " ";
    EV << ".\n";
      wfis.numUnsaturatedSessionsUsingLink[las.first] =\
              wfis.linkActiveSessions.at(las.first).size();
  }
}

void Waterfilling::update(int iter, WaterfillingIterationState& wfis) {
  double rateIncrement;

  struct FairShare{
    double value;
    Link link;
    FairShare(double value, Link link) : value(value), link(link) {}
  } minFairShare (-1, Link(-1, -1));

  for (auto& link : wfis.unsaturatedLinks) {
    //std::cout << "Checking link from " << link.first << " to " << link.second << ".";
    assert(linkCapacity.count(link) > 0);
    //std::cout << ".. done!\n";
    assert(wfis.totalFlow.count(link) > 0);
    assert(wfis.numUnsaturatedSessionsUsingLink.count(link) > 0);
    double value = (linkCapacity.at(link) - wfis.totalFlow.at(link))/	\
      wfis.numUnsaturatedSessionsUsingLink.at(link);
    EV << wfis.numUnsaturatedSessionsUsingLink.count(minFairShare.link) << " unsaturated sessions using link"
            << "(" << link.first << ", " << link.second << ").\n";

    if (minFairShare.value == -1 or value < minFairShare.value) {
      minFairShare = FairShare(value, link);
    }
  }

  if (minFairShare.value == -1) {
      EV << "Warning!! No link has extra capacity and unsaturated sessions.\n";
      minFairShare.value = 0;
  }

  EV << "Iter # " << iter << ": min fair share is "\
          << minFairShare.value << " on link "\
	    << "(" << minFairShare.link.first << ", "\
	    << minFairShare.link.second << ").\n";


  // update rate
  rateIncrement = minFairShare.value;
  for (auto& as : wfis.activeSessionMap) {
    if (wfis.unsaturatedSessions.count(as.first) > 0)
      wfis.rate[as.first] += rateIncrement;
    EV << "Iter # " << iter << ": flow #" << as.first << " has rate " << wfis.rate[as.first] << ".\n";
  }

  // update totalFlow
  for (auto& las : wfis.linkActiveSessions) {
    wfis.totalFlow[las.first] = 0;
    for (auto& sessionId : las.second)
            wfis.totalFlow[las.first] += wfis.rate[sessionId];
  }

  // update unsaturatedLinks
  wfis.unsaturatedLinks.clear();
  for (auto& las : wfis.linkActiveSessions) {
    assert(linkCapacity.count(las.first) > 0);
    assert(wfis.totalFlow.count(las.first) > 0);
    if (linkCapacity.at(las.first) - wfis.totalFlow.at(las.first) >= 1) { // floating point..
      wfis.unsaturatedLinks.insert(las.first);
      EV << "Link (" << las.first.first << ", " << las.first.second\
                      << ") has spare "\
                      <<  linkCapacity.at(las.first) - wfis.totalFlow.at(las.first)\
                      << " bps in iter # " << iter\
                      << ".\n";
    }
    else if (wfis.linkSaturatedInIter.count(las.first) == 0) {
        EV << "Link (" << las.first.first << ", " << las.first.second\
                << ") saturated in iter # " << iter << ".\n";
        wfis.linkSaturatedInIter[las.first] = iter;
    }
  }

  /*  std::copy(allLinks.begin(), allLinks.end(),	\
	    std::inserter(wfis.unsaturatedLinks),\
	    [] (const Link& link)\
	    {return (linkCapacity[link] - wfis.totalFlow[link] > 0)});
  */

  // sessions that can still increase
  wfis.unsaturatedSessions.clear();
  for (auto& as: wfis.activeSessionMap) {
    bool passesThroughSaturatedLink = false;
    assert(sessionPaths.count(as.second.session) > 0);
    for (auto& link: sessionPaths.at(as.second.session))
      if (wfis.unsaturatedLinks.count(link) == 0)
          passesThroughSaturatedLink = true;

    if (not passesThroughSaturatedLink) {
        EV << "In iter " << iter << " Flow # "\
                << as.first << " does not pass through "\
                << " any saturated link.\n";
      wfis.unsaturatedSessions.insert(as.first);
    } else if (wfis.sessionSaturatedInIter.count(as.first) == 0) {
        EV << "In iter " << iter << " Flow # "\
                        << as.first << " is saturated!\n";
        wfis.sessionSaturatedInIter[as.first] = iter;
    }
  }


  // number of unsaturated sessions using link
  wfis.numUnsaturatedSessionsUsingLink.clear();
  for (auto& link : wfis.unsaturatedLinks)  {
    int count = 0;
    assert(wfis.linkActiveSessions.count(link) > 0);
    for (const auto& sessionId : wfis.linkActiveSessions.at(link)) {
      if (wfis.unsaturatedSessions.count(sessionId) > 0)
	count++;
    }
    wfis.numUnsaturatedSessionsUsingLink[link] = count;
  }

  wfis.iter = iter;
}

Waterfilling::Waterfilling(std::map<Session, std::vector<Link> > sessionPaths,\
          std::map<Link, double> linkCapacity) :\
                  linkCapacity(linkCapacity), sessionPaths(sessionPaths) {
    for (auto& sessionPath : sessionPaths) {
      if (sessionPath.second.size() >= 1) {
        Ends ends(sessionPath.second.front().first, sessionPath.second.back().second);
        endsToSessionMap[ends] = sessionPath.first;
      }
      allSessions.insert(sessionPath.first);
      for (auto& link : sessionPath.second) {
        linkSessions[link].push_back(sessionPath.first);
      }
    }

    for (auto& ls : linkSessions)
      allLinks.insert(ls.first);
    numLinks = allLinks.size();
    numSessions = allSessions.size();
}


