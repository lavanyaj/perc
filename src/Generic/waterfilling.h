#ifndef _WATERFILLING_H_
#define _WATERFILLING_H_

#include <omnetpp.h>
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <sstream>

typedef std::pair<int, int> Link;
typedef std::pair<int, int> Ends;
typedef int Session;
typedef int ActiveSessionId;

typedef struct ActiveSessionS{
  int id;
  Session session;
  Ends ends;
  bool operator <(const struct ActiveSessionS & y) const {
    return this->id < y.id;
  }

} ActiveSession;

// Stores state of waterfilling algorithm after some number of iterations.
// Use active session id to get rates, saturation iteration etc.
// Use link tuple to get totalFlow, saturation iteration etc.

class WaterfillingIterationState {
 public:
  int iter;


  std::set<Link > unsaturatedLinks;
  std::set<ActiveSessionId> unsaturatedSessions;

  // Defined for all active links.
  std::map<Link, int> numUnsaturatedSessionsUsingLink;
  std::map<Link, double> totalFlow;
  std::map<Link, std::vector<ActiveSessionId> > linkActiveSessions; // const once initialized

  // Defined for all active sessions
  std::map<ActiveSessionId, double> rate;
  std::map<ActiveSessionId, ActiveSession> activeSessionMap; // const once initialized

  // Defined only for active sessions and links that have been saturated.
  std::map<ActiveSessionId, int> sessionSaturatedInIter;
  std::map<Link, int> linkSaturatedInIter;


  WaterfillingIterationState(const std::map<ActiveSessionId, ActiveSession>& activeSessions, \
			     const std::map<Session, std::vector<Link> >& sessionPaths);    
  std::string show();
};


// Use waterfilling to store the fixed state for a 
// topology with given link capacities- initialize
// it with all possible paths through the topology

// Then for each set of active sessions, call doWaterfilling
// to get optimal rates.
class Waterfilling {
public:
  int numLinks;
  int numSessions;
  std::map<Link, double> linkCapacity;
  std::map<Session, std::vector<Link> > sessionPaths;
  std::map<Link, std::vector<Session> > linkSessions;
  std::set<Link> allLinks;
  std::set<Session> allSessions;
  std::map<Ends, Session> endsToSessionMap;


 Waterfilling(std::map<Link, double> linkCapacity,			\
	      std::map<Session, std::vector<Link> > sessionPaths,	\
	      std::map<Link, std::vector<Session> > linkSessions,	\
	      std::set<Link> allLinks, std::set<Session> allSessions,	\
	      std::map<Ends, Session> endsToSessionMap):\
	numLinks(linkSessions.size()),\
	numSessions(sessionPaths.size()),\
	linkCapacity(linkCapacity), sessionPaths(sessionPaths),		\
    linkSessions(linkSessions), allLinks(allLinks), allSessions(allSessions), \
    endsToSessionMap(endsToSessionMap) {}

  WaterfillingIterationState WF(std::map<ActiveSessionId, ActiveSession> ends);
  void initialize(WaterfillingIterationState& wfis);
  void update(int iter, WaterfillingIterationState& wfis);


public:
  Waterfilling() {};

  // sessionPaths: all possible paths indexed by unique ids, each path is 
  // a list of links, where a link from node 1 to node 3 is a tuple (2,3)
  // linkCapacity: map from link to capacity

  Waterfilling(std::map<Session, std::vector<Link> > sessionPaths,\
                        std::map<Link, double> linkCapacity);

  // ends is a map of endpoints of active sessions indexed by session id
  WaterfillingIterationState doWaterfilling(std::map<int, Ends> ends);
  // ends is an ordered list of endpoints of active sessions
  WaterfillingIterationState doWaterfilling(std::vector<Ends> ends);

};

#endif // ifndef _WATERFILLING_H_
