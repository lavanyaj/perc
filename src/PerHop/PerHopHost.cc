#include "PerHopHost.h"
#include <assert.h>

namespace rcp {
Define_Module(PerHopHost);

#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 if (false) std::cout

GenericControlPacket* PerHopHost::newProtocolSpecificControlPacket(const char* pktname, int flowId) {
    PerHopPacket* pkt = new PerHopPacket(pktname);
    GenericControlHostSessionState &session = pIActiveSessions.at(flowId);
    PerHopHostSessionState &sMSession = sMActiveSessions.at(flowId);
    sMSession.maxHops = par("maxHops").doubleValue();
    pkt->setMaxHops(sMSession.maxHops);
    for (int hop = 0; hop < sMSession.maxHops; hop++) {
        pkt->setStampedRate(hop, sMSession.stampedRate[hop]);
    }
    return pkt;
}

void PerHopHost::protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    PerHopHostSessionState session(flowId);
    session.linkState.singleFairShareMode = par("singleFairShareMode");
    sMActiveSessions[flowId] = session;
}


void PerHopHost::sourceReceivePacket(const GenericControlPacket *msg) {
    const PerHopPacket *pkt = check_and_cast<PerHopPacket *>(msg);
    GenericControlHostSessionState &session = pIActiveSessions[pkt->getFlowId()];
    session.numRecvd++;

    if (pkt->getPktType() == CHARNY_FIN) {
        EV3 << "Source received FIN for flow " << pkt->getFlowId() << ", do nothing.\n";
        // since switches don't really add any info. to packets, just delete flow from state.
        return;
    }


    double minRate = -1;
    for (int hop = 0; hop < pkt->getMaxHops(); hop++) {
        double rate = pkt->getStampedRate(hop);
        if (minRate == -1 or  rate < minRate) minRate = rate;
     }

    emitSignal(pkt->getFlowId(), "StampedRate", minRate);
    EV2 << "At time "<< simTime() << ", emitting sourceReceivePacketSignal: "\
              << minRate << ".\n";

     PerHopHostSessionState &sMSession = sMActiveSessions[pkt->getFlowId()];
    // incoming stamped rate, maybe add one for outgoing stamped rate

   // NOTE THIS IS THE VERSION WHERE WE ASSUME DEMANDS ARE INFINITE

    int maxHops = pkt->getMaxHops();

    double newStampedRates[maxHops];
    for (int hop = 0; hop < maxHops; hop++) {
        double minRate = INFINITE_RATE;
        int minHop = -1;
        for (int otherHop = 0; otherHop < maxHops; otherHop++) {
            if (otherHop != hop) {
                double otherRate = pkt->getStampedRate(otherHop);
                if (otherRate < minRate) {minRate = otherRate; minHop = otherHop;}
            }
        }
        EV2 << "At source, setting stamped rate of flow # "  << pkt->getFlowId() \
                << " for hop " << hop << " to min of others " << minRate << ", hop # " << minHop << ".\n";


        EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                << " and hop " << hop\
                << ", sets packet.rate to "<< minRate << " because of other_hop # " << minHop << ".\n";
        assert(minRate < INFINITE_RATE);
        assert(minRate >= 0);
        newStampedRates[hop] = minRate;
    }
    EV3 << "Max hops in packets " << pkt->getMaxHops();
    EV3 << "Max hops in session " << sMSession.maxHops;
    assert(pkt->getMaxHops() == sMSession.maxHops);
    for (int hop = 0; hop < maxHops; hop++) {
        sMSession.stampedRate[hop] = newStampedRates[hop];
    }
    updateTransmissionRate(msg);
}

void PerHopHost::sourceReceivePacketGeneral(const GenericControlPacket *msg) {
    sourceReceivePacket(msg);
}

void PerHopHost::updateTransmissionRate(const GenericControlPacket *msg) {
    const PerHopPacket *pkt = check_and_cast<PerHopPacket *>(msg);
    GenericControlHostSessionState& session = pIActiveSessions.at(pkt->getFlowId());
    PerHopHostSessionState& sMSession = sMActiveSessions.at(pkt->getFlowId());

    // UPDATE ACTUAL TRANSMISSION RATE
    EV3 << "Max hops in packets " << pkt->getMaxHops();
    EV3 << "Max hops in session " << sMSession.maxHops;
    assert(pkt->getMaxHops() == sMSession.maxHops);
    double minRate = -1;
    for (int hop = 0; hop < pkt->getMaxHops(); hop++) {
        double rate = pkt->getStampedRate(hop);
        if (minRate == -1 or  rate < minRate) minRate = rate;
     }

    assert(minRate < INFINITE_RATE);
    assert(minRate >= 0);
    struct MarkedState {
        double stampedRate;
        MarkedState(double stampedRate) : stampedRate(stampedRate) {}
    } newIncoming(minRate);
    EV3 << "Update transmission rate for flow  " << session.flowId\
            << ", incoming rate is  " << newIncoming.stampedRate\
            << ", existing rate is "  << session.dataRate << ".\n";

    // How do data rates follow stamped rates? Some subset, in order, slightly delayed. I think.

    if (par("conservative").doubleValue() == 0\
            or session.dataRate == 0) {
        EV2 << "Being aggressive.\n";
        if (! rateEqual(newIncoming.stampedRate, session.dataRate)) {
                EV3 << "incoming rate "<< newIncoming.stampedRate\
                        <<" is "\
                        << (rateGreaterThan(newIncoming.stampedRate, session.dataRate) ? "more" : "less")\
                        << " than actual rate " << session.dataRate << ", ";
                EV3 << "schedule rate change now  "  << session.numRecvd \
                        << " at time " << simTime().dbl() << ".\n";
                session.sendAtPktNum = session.numRecvd;
                session.sendDataRate = newIncoming.stampedRate;
            } else {
                EV3 << "Incoming stamped rate (min) is " << newIncoming.stampedRate\
                        << " but it is same as session data rate "\
                        << session.dataRate << ".\n";
            }
    }

    if (par("conservative").doubleValue() > 0) {
        double numRtts = par("conservative").doubleValue();
        EV3 << "Being conservative.";
        if (! rateEqual(newIncoming.stampedRate, session.dataRate)) { // marked and new rate
          if (rateGreaterThan(newIncoming.stampedRate, session.dataRate)) {
              EV3 << "incoming rate is more than actual rate " << session.dataRate << ", ";
              if (session.sendAtPktNum >= session.numRecvd) { // rate increase already scheduled
                  if (rateGreaterThan(newIncoming.stampedRate, session.sendDataRate)) { // stick to schedule
                      EV3 << "incoming rate "<< newIncoming.stampedRate\
                              << "is more than existing rate increase "\
                              << session.sendDataRate << " scheduled (at RTT "\
                              << session.sendAtPktNum << " vs now " << session.numRecvd << ")"\
                              << " reschedule to " << numRtts << " RTTs from now at "\
                              << session.numRecvd+numRtts << ".\n";
                      session.sendAtPktNum = session.numRecvd+numRtts;
                      session.sendDataRate = newIncoming.stampedRate;
                    } else {
                        EV3 << "incoming rate "<< newIncoming.stampedRate\
                        << " is not more than existing rate increase "\
                        << session.sendDataRate << " scheduled (at RTT "\
                        << session.sendAtPktNum << " vs now " << session.numRecvd << ")"\
                        << " stick to schedule.\n";
                      session.sendDataRate = newIncoming.stampedRate;
                    }
                } else { // reschedule
                    EV3 << "No rate increase scheduled yet, schedule new rate increase to incoming rate "\
                            << newIncoming.stampedRate << " " << numRtts << " RTTs from now at "\
                            << session.numRecvd+numRtts << ".\n";
                    session.sendAtPktNum = session.numRecvd+numRtts;
                    session.sendDataRate = newIncoming.stampedRate;
                }
          } else { // schedule rate decrease
              EV3 << "incoming rate is less than actual rate " << session.dataRate\
                      << ", schedule rate decrease now  "  << session.numRecvd << ".\n";
              session.sendAtPktNum = session.numRecvd;
              session.sendDataRate = newIncoming.stampedRate;
          }
        } else if (rateEqual(newIncoming.stampedRate, session.dataRate)) {
            EV3 << "rate is same as actual rate " << session.dataRate << ".\n";
        }
    }


    if (session.sendAtPktNum == session.numRecvd) {
        assert(session.sendDataRate >= 0);
        sendFlowMsg(session.flowId, session.sendDataRate);
        //session.sendAtPktNum = -1;
    }

}
bool PerHopHost::destinationReceivePacket(const GenericControlPacket *msg) {
    const PerHopPacket *pkt = check_and_cast<PerHopPacket *>(msg);
    PerHopHostSessionState &session = sMActiveSessions.at(pkt->getFlowId());
    session.count += 1;
    if (session.count == K) {
        //assert(pkt->getMaxHops() == session.maxHops);
        for (int hop = 0; hop < pkt->getMaxHops(); hop++) {
            session.stampedRate[hop] = pkt->getStampedRate(hop);
        }
        session.count = 0;
        return true;
    } else {
        return false;
    }
}

void PerHopHost::egressAction(GenericControlPacket *msg) {
    PerHopPacket *pkt = check_and_cast<PerHopPacket *>(msg);
    int flowId = pkt->getFlowId();

    double oldCapacity = sMActiveSessions.at(pkt->getFlowId()).linkState.linkCapacity;
    double newCapacity = pIActiveSessions[flowId].demand;
    // starts with infinite rate
    if (oldCapacity != newCapacity and newCapacity < INFINITE_RATE) {
        EV3 << "Updated host's demand/link capacity for flow # " << pkt->getFlowId() << " from "\
                << oldCapacity << " to " << newCapacity << ".\n";
        sMActiveSessions.at(pkt->getFlowId()).linkState.linkCapacity = pIActiveSessions[flowId].demand;
    }

    sMActiveSessions.at(pkt->getFlowId()).linkState.linkAction(pkt);
    if(pkt->getIsForward()) {
        pkt->setHop(pkt->getHop()+1);
        EV2 << "After link " << sMActiveSessions.at(pkt->getFlowId()).linkState.getId() \
            << ", set packet hop to " << pkt->getHop() << ".\n";
    }
}
void PerHopHost::addProtocolSpecificActiveSession(const GenericControlPacket *pkt) {
    int flowId = pkt->getFlowId();
    PerHopHostSessionState session(flowId);
    session.linkState.singleFairShareMode = par("singleFairShareMode");
    sMActiveSessions[flowId] = session;
}; // at destination
void PerHopHost::removeProtocolSpecificActiveSession(int flowId) {\
    sMActiveSessions.erase(flowId);
}; // on FIN

#endif // EV2
#endif // EV3
}; // namespace
