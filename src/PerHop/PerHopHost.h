//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __PERHOP_HOST_H
#define __PERHOP_HOST_H

#include <omnetpp.h>
#include "util.h"
#include "GenericControlHost.h"
#include "PerHopSwitch.h"
#include "perhop_m.h"
#include <map>
#include <utility>

namespace rcp {

/**
 * Implements the Txc simple module. See the NED file for more information.
 */


class PerHopHostSessionState {
public:
    int flowId;
    int maxHops;
    double stampedRate[MAX_HOPS];
    int count;
    PerHopLinkState linkState;
    PerHopHostSessionState(int fid) : flowId(fid), maxHops(-1), count(0), linkState(std::to_string(fid)) {
        linkState.linkCapacity = 100000000000;
        for (int i = 0; i < MAX_HOPS; i++) {
            stampedRate[i] = INFINITE_RATE;
        }
    }
    PerHopHostSessionState(): flowId(-1), maxHops(-1), count(0), linkState(std::to_string(-1)) {
        linkState.linkCapacity = 100000000000;
        for (int i = 0; i < MAX_HOPS; i++) {
                    stampedRate[i] = INFINITE_RATE;
        }
    }
};

/*
 *  To have flows with finite demands..
 *  For each session, switch.linkCapacity, switch.advertisedRate, switch.numForwardSessions,..
 *  so maybe to host session state, add a PerHopLinkState(idStr) and linkAction every packet
 *  going out.
 *  When flow's ending, change switch linkCapacity to 0..
 */


class PerHopHost : public GenericControlHost
{

  private:
     // Charny state
    typedef std::map<int, PerHopHostSessionState> SessionTable;
    SessionTable sMActiveSessions;

  protected:
    virtual GenericControlPacket *newProtocolSpecificControlPacket(const char* pktname, int flowId);
    virtual void sourceReceivePacket(const GenericControlPacket *pkt); // assumes demand is always INFINITE
    virtual void sourceReceivePacketGeneral(const GenericControlPacket *pkt); // demand could be FINITE, changing
    virtual bool destinationReceivePacket(const GenericControlPacket *pkt);
    virtual void updateTransmissionRate(const GenericControlPacket *pkt);
    virtual void egressAction(GenericControlPacket *msg);
    virtual void protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg); // at source
    virtual void addProtocolSpecificActiveSession(const GenericControlPacket *pkt); // at destination
    virtual void removeProtocolSpecificActiveSession(int flowId); // on FIN


};


}; // namespace

#endif
