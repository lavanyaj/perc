#include "PerHopSwitch.h"

#include <assert.h>
#include <algorithm>
namespace rcp {

Define_Module(PerHopSwitch);

#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 if (false) std::cout
#ifndef EV1
#define EV1 if (false) std::cout

PerHopLinkState::PerHopLinkState(std::string idStr):
        linkCapacity(-1), numForwardSessions(0),\
                numFeedbackSessions(0), idStr(idStr), singleFairShareMode(false) {

            WATCH(numForwardSessions);
            WATCH(numFeedbackSessions);

        }

PerHopLinkState::~PerHopLinkState() {
    activeSessions.clear();
}


PerHopSwitch::PerHopSwitch() {
}

PerHopSwitch::~PerHopSwitch() {



}

void PerHopSwitch::finish() {
    EV << "Clearing Link State for PerHop.\n";
    for (auto ls : cltable) {
        delete ls.second;
        ls.second = NULL;
    }
    cltable.clear();
    GenericSwitch::finish();
}
void PerHopSwitch::initialize()
{
    GenericSwitch::initialize();
   for (auto linkState: ltable) {
        cltable[linkState.first] = new PerHopLinkState(linkState.second->getId());
        EV << "Added new PerHop link state for link " << linkState.first\
                << ".\n";
        cltable[linkState.first]->linkCapacity = linkState.second->linkCapacity;
        cltable[linkState.first]->header_size_ = par("headerSize").doubleValue();
        cltable[linkState.first]->payload_size_ = par("payloadSize").doubleValue();
        cltable[linkState.first]->minRtt = par("minRtt").doubleValue();
        cltable[linkState.first]->maxSessionsPerRtt = par("maxSessionsPerRtt").doubleValue();
        cltable[linkState.first]->singleFairShareMode = par("singleFairShareMode");

        //cltable[linkState.first]->eagerSwitchMode = linkState.second->eagerSwitchMode;
    }
}

cMessage* PerHopSwitch::protocolSpecificProcessEgress(int outGateIndex, cMessage* msg) {
    EV << "Calling cltable.at("<< outGateIndex << ")->processEgress\n";
    PerHopPacket * pkt = check_and_cast<PerHopPacket *>(msg);
    return cltable.at(outGateIndex)->processEgress(pkt);

}

// actually ingate index etc.
cMessage* PerHopSwitch::protocolSpecificProcessIngress(int outGateIndex, cMessage* msg) {
    return cltable[outGateIndex]->processIngress(check_and_cast<PerHopPacket *>(msg));

}

PerHopPacket *PerHopLinkState::processIngress(PerHopPacket *pkt)
{
     return pkt;
     //->dup();
}

PerHopPacket *PerHopLinkState::processEgress(PerHopPacket *pkt)
{

    if (pkt->getPktType() == CHARNY_HELLO || pkt->getPktType() == CHARNY_FIN) {
        EV << "Link action HELLO/FIN\n";
        linkAction(pkt);
    } else {
        EV << "Link action: Unknown packet type.\n";
    }
    if(pkt->getIsForward()) {
        pkt->setHop(pkt->getHop()+1);
        EV << "Link " << this->getId() << " set packet hop to " << pkt->getHop() << ".\n";
    }
    return pkt;
    //->dup();
}




void PerHopLinkState::calculateAdvRate() {
    if (singleFairShareMode) {

        double singleFairShareRate = calculateRate();
        double oldAdvertisedRate = -1;

        std::map<int, double> smallFlows;
        double totalSmallFlowDemand = 0;
        double totalBigFlowDemand = 0;
        for (auto& session : activeSessions) {
            if (session.second.recordedRate < singleFairShareRate) {
                smallFlows[session.first] = session.second.recordedRate;
                totalSmallFlowDemand += session.second.recordedRate;
            } else totalBigFlowDemand += singleFairShareRate;

            // some sessions might not yet have an advertised rate?
            oldAdvertisedRate = session.second.advertisedRate;
            // checking that every session gets the same advertised rate.
            session.second.advertisedRate = singleFairShareRate;
            EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                    " for flow " << session.first <<\
                    ", computes fair_share as " << singleFairShareRate << " also"\
                    << " old session rate was " << oldAdvertisedRate << ".\n";
        }
        EV2 << "Switch " << this->getId() << " updated rate for all flows from "\
                         << oldAdvertisedRate << " -> " << singleFairShareRate << ".\n";

        double totalFlowDemand = (totalSmallFlowDemand+totalBigFlowDemand);
        double linkCapacityForControl = (((header_size_*8)/minRtt) * (maxSessionsPerRtt));
        double linkCapacityForData = linkCapacity - linkCapacityForControl;
        assert(totalFlowDemand <= linkCapacityForData);
        std::stringstream ss;
        ss << smallFlows.size() << " smallFlows with total demand " << totalSmallFlowDemand/1.0e+9 << " Gbps, ";
        ss << activeSessions.size() - smallFlows.size() << " bigFlows with demand " << totalBigFlowDemand/1.0e+9 << " Gbps, ";
        ss << activeSessions.size() << " totalFlows with demand " << totalFlowDemand/1.0e+9 << " Gbps, ";
        ss << "link capacity for control " << linkCapacityForControl/1.0e+9 << " Gbps, ";
        ss << "link capacity for data " << linkCapacityForData/1.0e+9 << " Gbps.\n";
        for (const auto& sf : smallFlows) {
            ss << sf.first << ": " << sf.second/1.0e+9 << " Gbps, ";
        }
        EV3 << ss.str();
        return;
    } else {

        for (auto& session : activeSessions) {
            double originalRecordedRate = session.second.recordedRate;
            double oldAdvertisedRate = session.second.advertisedRate;
            session.second.recordedRate = INFINITE_RATE;
            session.second.advertisedRate = calculateRate();
            session.second.recordedRate = originalRecordedRate;

            EV2 << "Switch " << this->getId() << " updated rate for flow # " << session.second.flowId << " from "\
                    << oldAdvertisedRate << " -> " << session.second.advertisedRate << ".\n";

            EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                    " for flow " << session.first <<\
                    ", computes fair_share[flow] as " <<\
                    session.second.advertisedRate << " because ...\n";
        }
    }
 }

double PerHopLinkState::getDiff(double fairShare, double capacity) {
    double total = 0;
      for (const auto& session : activeSessions) {
          double min = session.second.recordedRate;
          if (fairShare <= min) min = fairShare;
          total += min;
      }
      double diff = capacity - total;
      return floor(diff);
}

double PerHopLinkState::calculateRate() {
    //return linkCapacity;
    double linkCapacityForControl = (((header_size_*8)/minRtt) * (maxSessionsPerRtt));
    double linkCapacityForData = linkCapacity - linkCapacityForControl;
    if (linkCapacityForControl < 1) {
        linkCapacityForControl = 0.0;
        linkCapacityForData = linkCapacity;
    }


    double totalDemand = 0;
    double maxDemand = -1;
    bool greedy = false;
    std::stringstream rr;

    for (const auto& session : activeSessions) {
        if (session.second.recordedRate == INFINITE_RATE) // Demand exceeds capacity
            greedy = true;
      totalDemand += session.second.recordedRate;
      rr << session.first << ": " << session.second.recordedRate/1.0e9 << " Gbps, ";
      if (maxDemand == -1 or maxDemand < session.second.recordedRate)
          maxDemand = session.second.recordedRate;
    }
    EV3 << "Demands at " << this->getId() << " are " << rr.str() << ". (that's why)\n";
    if (not greedy and rateLessThan(totalDemand, linkCapacityForData)) {
        EV3 << "total demand " << totalDemand/1.0e+9 << " Gbps is less than capacity "\
                << linkCapacityForData/1.0e+9<< " Gbps, max Demand = " << maxDemand/1.0e+9\
                << " Gbps so return link capacity "\
                << linkCapacityForData/1.0e+9 << " Gbps (that's why)\n";
        return linkCapacityForData;
    }

    // total demand exceeds capacity
    double fairShare = 0;
    double bestFairShare = 0;
    double diff = linkCapacityForData;
    double bestDiff = -1;

    // When fair share is too low .. diff >= 0
    // When fair share is too high .. diff < 0
    double upper = linkCapacityForData;
    double lower = 0;
    while(upper - lower > 2) {
        fairShare = floor((upper+lower)/2);
        diff = getDiff(fairShare, linkCapacityForData);
        //EV2 << "Fair share between " << lower << " and " << upper << ", diff: " << diff << "\n";
        if (diff < 0) upper = fairShare;
        else if (diff > 0) lower = fairShare;
        else {upper = fairShare; lower = fairShare;}
    }
    fairShare = floor((upper+lower)/2);
    diff = getDiff(fairShare, linkCapacityForData);
    //EV2 << "Fair share between " << lower << " and " << upper << ", diff: " << diff << "\n";

    if (rateLessThan(maxDemand, linkCapacityForData)\
            and getDiff(ceil(maxDemand), linkCapacityForData) <=\
            getDiff(fairShare, linkCapacityForData)\
            and getDiff(ceil(maxDemand), linkCapacityForData) >= 0) {
        EV3 << "Check if"\
                ", fair share should be max. demand " << maxDemand <<\
                ", so diff would be " << getDiff(floor(maxDemand), linkCapacityForData) <<\
                ", or maybe using ceil maxDemand " << getDiff(ceil(maxDemand), linkCapacityForData) <<\
                ", but if it were less than capacity"\
                ", shouldn't be here, should return less than cap."\
                ", so check if we should return max. dem. (that's why)\n";
        // possible that total demand < capacity actually, don't want to say C

            bestDiff = getDiff(floor(maxDemand), linkCapacityForData);
            bestFairShare = floor(maxDemand);
        } else {
        for (fairShare = upper; fairShare >= lower; fairShare--) {
        diff = getDiff(fairShare, linkCapacityForData);
        if (diff >= 0) {
          if (bestDiff == -1 or diff < bestDiff) {
             bestDiff = diff;
             bestFairShare = fairShare;
          }
        }
        }
    }
    /*
    for (fairShare = 0; fairShare <= linkCapacity; fairShare++) {
        int total = 0;
        for (const auto& session : activeSessions) {
            double min = session.second.recordedRate;
            if (fairShare <= min) min = fairShare;
            total += min;
        }
        diff = linkCapacity - total;
        if (diff >= 0) {
            if (bestDiff == -1 or diff < bestDiff) {
                bestDiff = diff;
                bestFairShare = fairShare;
            }
        }
    }
    */
    EV3 << "Demand " << totalDemand/1.0e+9 << " Gbps exceeds capacity "\
            << linkCapacityForData/1.0e+9 << " Gbps. ";
    EV3 << "Fair share " << bestFairShare << " gives diff " << bestDiff << " from link capacity (for data)";
    EV3 << " for " << activeSessions.size() << " active sessions. (that's why) \n";
    return bestFairShare;
}


// HELLO AND FIN PACKETS ON HIGH PRIO QUEUE, DATA PACKETS ACCORDING TO RATE
// DECOUPLE CONTROL AND DATA
void PerHopLinkState::linkUpdateSessionExit(const PerHopPacket *pkt) {
    if (pkt->getIsForward()) {
        numForwardSessions -= 1;
    } else {
        numFeedbackSessions -= 1;
    }
    activeSessions.erase(pkt->getFlowId());
    calculateAdvRate();
}

// now this is not just like UpdateKnownSession..
void PerHopLinkState::linkUpdateNewSession(PerHopPacket *pkt) {

    int flowId = pkt->getFlowId();
    if (pkt->getIsForward()) numForwardSessions += 1;
    else numFeedbackSessions += 1;
    PerHopSwitchSessionState session(0, 0, flowId);
    activeSessions[flowId] = session;

    linkUpdateKnownSession(pkt);

}

void PerHopLinkState::linkUpdateKnownSession(PerHopPacket *pkt) {
    PerHopSwitchSessionState& session = activeSessions[pkt->getFlowId()];
    session.numPackets += (pkt->getIsForward()) + K * (1 - pkt->getIsForward());
    session.recordedRate = pkt->getStampedRate(pkt->getHop());

    EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                        " for flow " << pkt->getFlowId() <<\
                        ", records demand[flow] as " << session.recordedRate
                        << " because that's packet.rate(hop="<< pkt->getHop()<<")...\n";



    EV2 << "Packet has stamped rate (hop " << pkt->getHop() << ") " << pkt->getStampedRate(pkt->getHop()) << ".\n";
    calculateAdvRate(); // rate including this new session

    //if (rateGreaterThan(pkt->getStampedRate(pkt->getHop()), session.advertisedRate)) {
        pkt->setStampedRate(pkt->getHop(), session.advertisedRate);

        EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                                " for flow " << pkt->getFlowId() <<\
                                ", stamps packet.rate as " << session.advertisedRate
                                << " (fair_share) because ...\n";

        EV2 << "Switch " << this->getId() << " set flow # " << pkt->getFlowId() << " packet's stamped rate (hop " << pkt->getHop() << ") to advertised rate " << session.advertisedRate << ".\n";
    //}
    EV2 << "Mark session " << pkt->getFlowId() << "'s recorded rate (hop " << pkt->getHop() << ") as "\
            << session.recordedRate << ".\n";
}

void PerHopLinkState::linkAction(PerHopPacket *pkt){
    if(!pkt->getIsForward()) {
        EV2 << "No link action on feedback packet of session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        return;
    }


    if (pkt->getIsExit()) {
        EV3 << ((activeSessions.count(pkt->getFlowId()) == 1) ? "Known" : "New")\
                << " flow " << pkt->getFlowId() << " exiting, at Switch " << this->getId() << ".\n";
        linkUpdateSessionExit(pkt);
        return;
    }

    if (activeSessions.count(pkt->getFlowId()) == 0) {
        EV3 << "New flow " << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        linkUpdateNewSession(pkt);
    } else {
        EV3 << "Known flow " << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        linkUpdateKnownSession(pkt);
    }

}


#endif // EV1
#endif // EV2
#endif // EV3
}; // namespace
