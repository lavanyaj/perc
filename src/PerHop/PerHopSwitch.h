//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __PERHOP_SWITCH_H
#define __PERHOP_SWITCH_H

#include <omnetpp.h>
#include "util.h"
#include "GenericSwitch.h"
#include "perhop_m.h"
#include <map>

namespace rcp {


class PerHopSwitchSessionState {
public:
    double recordedRate;
    double advertisedRate;
    int numPackets;
    int flowId;

    PerHopSwitchSessionState(double rr, int np, int fid) :
        recordedRate(rr), numPackets(np), flowId(fid), advertisedRate(-1) {
    }
    PerHopSwitchSessionState() :
            recordedRate(0), numPackets(0), flowId(-1), advertisedRate(-1) {
        }
};

class PerHopLinkState {
    public:
    std::string idStr;
    std::string getId() {return idStr;}
    //cMessage *dequeueMessage;

    // PerHop state
    double linkCapacity;
    double header_size_;
    double payload_size_;
    double minRtt;
    double maxSessionsPerRtt;

    typedef std::map<int, PerHopSwitchSessionState> SessionTable;
    SessionTable activeSessions;

    int numForwardSessions;
    int numFeedbackSessions;
    //bool eagerSwitchMode;
    bool singleFairShareMode;

    PerHopLinkState(std::string idStr);
    ~PerHopLinkState();

    PerHopPacket *processIngress(PerHopPacket *pkt);
    PerHopPacket *processEgress(PerHopPacket *pkt);
    double getDiff(double fairShare, double capacity);
    void calculateAdvRate();
    double calculateAdvRateAndRevert();
    double calculateRate();
    double getMaxRate();
    double getTotalRate();
    void linkUpdateSessionExit(const PerHopPacket *pkt);
    void linkUpdateNewSession(PerHopPacket *pkt);
    void linkUpdateKnownSession(PerHopPacket *pkt);
    void linkAction(PerHopPacket *pkt);
};



/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class PerHopSwitch : public GenericSwitch
{

private:
    typedef std::map<int,PerHopLinkState*> PerHopLinkStateTable; // output queue # -> link state
    PerHopLinkStateTable cltable;
protected:
    virtual void initialize();
    virtual void finish();

    cMessage* protocolSpecificProcessIngress(int outGateIndex, cMessage* msg);
    cMessage* protocolSpecificProcessEgress(int outGateIndex, cMessage* msg);

public:
    PerHopSwitch();
    ~PerHopSwitch();
};

}; // namespace

#endif
