#include "FlowGenerator.h"
#include <vector>
#include <sstream>
#include <string>
namespace rcp {

Define_Module(FlowGenerator);

void FlowGenerator::initialize()
{
    flowTimeSignal = registerSignal("flowTime");
    numFlows = 0;
    meanNpkts = par("meanNpkts");
    paretoShape = par("paretoShape");
    lambda = par("lambda");

    randomInterval = 0.01;
    int sim = par("sim");
    if(sim == 1)
        initializeRcpSim1();
    else if (sim == 3)
        initializeRcpSim3();
    else if (sim == 5)
        initializeRcpSim5();
}

void FlowGenerator::initializeRcpSim1() {
    double startTime = 0.0;
    int numLongFlows = par("longLived");
    if(numLongFlows > 0)
        startLongLivedFlows(numLongFlows, startTime);
    else
        EV << "Tried long-lived flows, but flow count was 0.\n";
}
void FlowGenerator::initializeRcpSim3() {
    double startTime = 0.0;
    double randomStart;
    // 0 --> 4
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg04 = getMakeFlowMsg(randomStart, true, 0, 4);
    scheduleAt(randomStart, msg04);
    // 1 --> 5
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg15 = getMakeFlowMsg(randomStart, true, 1, 5);
    scheduleAt(randomStart, msg15);
    // 3 --> 5
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg35 = getMakeFlowMsg(randomStart, true, 3, 5);
    scheduleAt(randomStart, msg35);

    // wait until stabilize, then add in another message from 0 to 3
    // 0 --> 3
    double delayTime = startTime + 0.3 + uniform(0,randomInterval);
    MakeFlowMsg* msg03 = getMakeFlowMsg(delayTime, true, 0, 3);
    scheduleAt(delayTime, msg03);
}

void FlowGenerator::initializeRcpSim5() {
    double startTime = 0.0;
    double randomStart;
    // 0 --> 2
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg1 = getMakeFlowMsg(randomStart, true, 0, 2);
    scheduleAt(randomStart, msg1);
    // 0 --> 2
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg2 = getMakeFlowMsg(randomStart, true, 0, 2);
    scheduleAt(randomStart, msg2);
    // 0 --> 3
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg3 = getMakeFlowMsg(randomStart, true, 0, 3);
    scheduleAt(randomStart, msg3);
    // 0 --> 3
    randomStart = startTime + uniform(0,randomInterval);
    MakeFlowMsg* msg4 = getMakeFlowMsg(randomStart, true, 0, 3);
    scheduleAt(randomStart, msg4);
}
void FlowGenerator::startLongLivedFlows(int longLived, double startTime) {
    MakeFlowMsg *msg;
    for(int i=0; i < longLived; i++) {
        double randomStart = startTime + uniform(0,0.1);
        msg = getMakeFlowMsg(randomStart, true, 0, 1);
        scheduleAt(randomStart, msg);
    }
}

MakeFlowMsg* FlowGenerator::getMakeFlowMsg(double sendTime, bool start, int src, int dest) {
    std::stringstream msgName;
    if(start)
        msgName << "start ";
    else
        msgName << "end ";
    msgName << "flow " << numFlows << ": " << src << "->" << dest << "\n";
    MakeFlowMsg *flowMsg = new MakeFlowMsg(msgName.str().c_str());
    flowMsg->setSource(src);
    flowMsg->setDestination(dest);
    flowMsg->setFlowId(numFlows++);
    flowMsg->setStart(start);
    flowMsg->setDuration(-1);
    flowMsg->setSendTime(sendTime);
    return flowMsg;
}

void FlowGenerator::handleMessage(cMessage *msg)
{
    emit(flowTimeSignal, simTime()); // when new flow signaled
    MakeFlowMsg *flowMsg = check_and_cast<MakeFlowMsg *>(msg);
    send(msg, "gate$o", flowMsg->getSource());
}

}; // namespace
