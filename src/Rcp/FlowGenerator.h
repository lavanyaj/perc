//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __RCP_FLOWGENERATOR_H
#define __RCP_FLOWGENERATOR_H

#include <omnetpp.h>
#include "util.h"
#include "makeflow_m.h"

namespace rcp {

/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class FlowGenerator : public cSimpleModule
{
    private:
        simsignal_t flowTimeSignal;
        int numFlows;
        // int flowId;
        int meanNpkts;
        double paretoShape;
        double lambda;
        double randomInterval;
    protected:
      virtual void initialize();
      virtual void handleMessage(cMessage *msg);
      virtual void initializeRcpSim1();
      virtual void initializeRcpSim5();
      virtual void initializeRcpSim3();
      virtual void startLongLivedFlows(int longLived, double startTime);
      MakeFlowMsg* getMakeFlowMsg(double sendTime, bool start, int src, int dest);
};

}; // namespace

#endif
