#include "QueueChannelManager.h"

/*
 * Toy module, to be ported over to RcpSwitch.
 */
namespace rcp {

Define_Module(QueueChannelManager);

using inet::DropTailQueue;
void QueueChannelManager::initialize()
{
    cGate* qgate = gate("queue_out")->getPreviousGate();
    qptr = check_and_cast<DropTailQueue *> (qgate->getOwnerModule());
    chanptr = check_and_cast<cDatarateChannel *> (gate("channel_io$o")->getChannel());
    std::stringstream qmsg_name;
    qmsg_name << "queue_request" << getIndex();
    qmsg = new cMessage(qmsg_name.str().c_str());
}

void QueueChannelManager::handleMessage(cMessage *msg)
{
    if(msg == qmsg) {
        qptr->requestPacket();
        return;
    }
    // DO NOT DELETE ANY MESSAGES.
    RcpPacket *pkt = check_and_cast<RcpPacket *>(msg);
    if (!pkt) {
        EV << getIndex() << ": Unidentified packet at Channel.\n";
        return;
    }
    if(pkt->arrivedOn("outside_io$i")){ // put incoming packet from outside on queue.
        send(pkt, "queue_in");
        requestQueueMessage();
    } else if(pkt->arrivedOn("channel_io$i")) { // send incoming packet from channel to outside.
        send(pkt, "outside_io$o"); //destination
    } else if(pkt->arrivedOn("queue_out")) {
        send(pkt, "channel_io$o");
        requestQueueMessage();
    }
}
void QueueChannelManager::requestQueueMessage() {
    if(!qmsg->isScheduled()) {
        if(chanptr->isBusy())
            scheduleAt(chanptr->getTransmissionFinishTime(), qmsg);
        else {
            if(!qptr->getNumPendingRequests())
                scheduleAt(simTime(), qmsg);
            else {// last packet transmitted and already requested. so don't request again bruh
                EV << getIndex() << ": queue already has a request\n";
            }
        }
    }
}
}; // namespace
