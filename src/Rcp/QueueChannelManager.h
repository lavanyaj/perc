//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __RCP_QUEUECHANNELMANAGER_H
#define __RCP_QUEUECHANNELMANAGER_H

#include <omnetpp.h>
#include "util.h"
#include <map>
#include "inet/common/queue/DropTailQueue.h"
#include "rcp_m.h"
#include "generic_m.h"

namespace rcp {

using inet::DropTailQueue;
  
class QueueChannelManager : public cSimpleModule
{
private:
    DropTailQueue* qptr;
    cDatarateChannel* chanptr;
    cMessage* qmsg;

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void requestQueueMessage();
};

}; // namespace

#endif
