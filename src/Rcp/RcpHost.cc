#include "RcpHost.h"
#include <cstring>
#include <assert.h>
namespace rcp {
Define_Module(RcpHost);

#define REF_ON 0

void RcpHost::initialize()
{
    header_size_ = par("headerSize").doubleValue();
    payload_size_ = par("payloadSize").doubleValue();

    SYN_DELAY = par("SYN_DELAY").doubleValue(); // 0.5
    REF_FACT = par("REF_FACT").doubleValue(); //1  //Number of REFs per RTT
    QUIT_PROB = par("QUIT_PROB").doubleValue();// 0.1  //probability of quitting if assigned rate is zero
    REF_INTVAL = par("REF_INTVAL").doubleValue();

    startSignal = registerSignal("start");
    endSignal = registerSignal("end");
    lengthSignal = registerSignal("length");
    address = getParentModule()->getIndex();
     // Bytes -> bits, Mbps
    //BitsPerByte = 8*1e-6;
    const char *vinit[] = {"NumPkts", "Rate", "Rtt", "MinRtt", "Interval",\
            "PktType", "HostState", "TimeoutState"};

    /*,\
    "SynRecvd", "SynackRecvd", "RefackRecvd", "AckRecvd", "FinRecvd",\
   "FinackRecvd", "RefRecvd", "DataRecvd",\
   "RunningState", "RunningWrefState", "RetransmitState", "InactState",\
    "RcpTimeout", "RefTimeout", "RtoTimeout"};
     */

    statNames = std::vector<std::string>(vinit, vinit+sizeof(vinit)/sizeof(char*));

    /*
    for (const std::string& pktType: rcpPktTypeStr) {
        statNames.push_back(pktType);
    }

    for (const std::string& hostState: rcpHostStateStr) {
        statNames.push_back(hostState);
    }

    for (const std::string& timeoutState: rcpTimeoutStateStr) {
        statNames.push_back(timeoutState);
    }
    */
}

void RcpHost::emitSignal(int flowId, std::string name, double value) {
    std::stringstream ss; ss.clear(); ss.str("");
    ss << "session-" << flowId << "-" << name;
    emit(ftable.at(flowId).signals[ss.str()], value);
    EV << "At time "<< simTime() << ", emitting " << ss.str() << ": " << value << "\n";
 }

void RcpHost::handleMessage(cMessage *msg)
{
    if(msg->arrivedOn("flowAlert$i")) // start new flow
    {

        EV << "Local " << address << ": Arrived on Flow Alert\n";

        if (!std::strcmp(msg->getName(), "StopSwitch")) {
            EV << "Forwarding stop message form flwo gen to switch.\n";
            send(msg, "gate$o");
            return;
        }
        CharnyMakeFlowMsg *flowmsg = check_and_cast<CharnyMakeFlowMsg *>(msg);
        if(flowmsg->getChangeType()==CHARNY_MAKEFLOW_START)
            setupSession(flowmsg);
        else if(flowmsg->getChangeType()==CHARNY_MAKEFLOW_END)
            tearDownSession(flowmsg->getFlowId());

    }
    else if(msg->isSelfMessage()) // timeout event
    {
        RcpTimeoutPacket *pkt = check_and_cast<RcpTimeoutPacket *>(msg);
        emitSignal(pkt->getFlowId(), "TimeoutState", pkt->getTimeoutType());
        EV <<"Local " << address <<  ": received message " << pkt->getName() << "\n";
        EV << "timeout type: " << pkt->getTimeoutType() << ", rcp: " << RCP_TIMEOUT <<  ", ref: " << REF_TIMEOUT << "\n";
        switch(pkt->getTimeoutType()) {
        case RCP_TIMEOUT:
            rcp_timeout(pkt->getFlowId());
            break;
        case REF_TIMEOUT:
            ref_timeout(pkt->getFlowId());
            break;
        case RTO_TIMEOUT:
            rto_timeout(pkt->getFlowId());
        }
    }
    else // recv from outside
    {


        RcpPacket * pkt = check_and_cast<RcpPacket *>(msg);
        EV << "Received a " << rcpPktTypeStr[pkt->getPktType()]\
                        << " from " << pkt->getSource() << ".\n";

        recv(pkt);
    }
}

void RcpHost::finish()
{
    for (FlowTable::iterator it=ftable.begin(); it!=ftable.end(); ++it){
        if(it->second.flowmsg != NULL) // sketchy
            delete (it->second.flowmsg);
        cancelAndDelete((it->second).rcp_timeout_pkt_);
        cancelAndDelete(it->second.ref_timeout_pkt_);
        cancelAndDelete(it->second.rto_timeout_pkt_);
    }
}

void RcpHost::setupSession(CharnyMakeFlowMsg *flowmsg) {
    int flowId = flowmsg->getFlowId();
    createFlowEntry(flowId, flowmsg);

    setupSignals(flowId);
    EV << "setting up " << ftable.at(flowId).signals.size() << " signals for sender of flow # "  << flowId << ".\n";
    emitSignal(flowId, "MinRtt", ftable.at(flowId).min_rtt_);
    sendfile(flowmsg->getFlowId());
}

void RcpHost::tearDownSession(int flowId) {
    if(ftable.find(flowId) == ftable.end()) {
        EV << "Local " << address << ": could not tear down session " << flowId << "because it is not started.\n";
    } else {
        EV << "FlowGenerator called finish on session " << flowId << "\n";
        setRcpState(flowId, RCP_FINTRIGGER);
    }
}

void RcpHost::setRcpState(int flowId, int value) {

    int oldValue = ftable.at(flowId).RCP_state;
    ftable.at(flowId).RCP_state = value;
    if (oldValue != value) {
        emitSignal(flowId, "HostState", value);
    }

}

int RcpHost::getRcpState(int flowId) {
    return ftable.at(flowId).RCP_state;
}
void RcpHost::createFlowEntry(int flowId, CharnyMakeFlowMsg *flowmsg) {
    std::stringstream strbuf;
    strbuf << flowId << ":rcp_timeout";
    RcpTimeoutPacket *rcp_timeout = new RcpTimeoutPacket(strbuf.str().c_str());
    rcp_timeout->setFlowId(flowId);
    rcp_timeout->setTimeoutType(RCP_TIMEOUT);
    strbuf.clear(); strbuf.str("");
    strbuf << flowId << ":ref_timeout";
    RcpTimeoutPacket *ref_timeout = new RcpTimeoutPacket(strbuf.str().c_str());
    ref_timeout->setFlowId(flowId);
    ref_timeout->setTimeoutType(REF_TIMEOUT);
    strbuf.clear(); strbuf.str(""); strbuf << flowId << ":rto_timeout";
    RcpTimeoutPacket *rto_timeout = new RcpTimeoutPacket(strbuf.str().c_str());
    rto_timeout->setFlowId(flowId);
    rto_timeout->setTimeoutType(RTO_TIMEOUT);


    flowEntry_t temp = {
        0, // RTT_, // double
        0.0, // INTERVAL_, // double
        0, // NUM_SENT_,
        RCP_INACT, // RCP_STATE,
        0, // NUMOUTREFS_,
        SYN_DELAY*REF_FACT*10, // MIN_RTT_, //double 0.5 * 1 * 10 = 5
        0, // REF_SEQNO_,

        0, // NUM_DATAPKTS_ACKED_BY_RECEIVER_,
        0, // NUM_DATAPKTS_RECEIVED_,
        0, // NUM_PKTS_TO_RETRANSMIT_,
        0, // NUM_PKTS_RESENT_,
        0, // NUM_ENTER_RETRANSMIT_MODE_,

        0, // SEQNO_,
        -1, //NUMPKTS_
        -100000, //lastpkttime_
        -1, // destination_

        -1,// transmission_rate_
        NULL,// MakeFlowMsg
        NULL,// Charny timeoutpkt_
        std::map<std::string, simsignal_t>(), // signal
        rcp_timeout, // rcp_timeout_pkt_
        ref_timeout, // ref_timeout_pkt_
        rto_timeout, // rto_timeout_pkt_
    };
    if(flowmsg != NULL) {
        temp.flowmsg = flowmsg; // flowmsg has default rate initial rate, not used by Rcp host.
        temp.num_pkts_ = int(flowmsg->getDuration());
        temp.destination_ = flowmsg->getDestination();
    }

    ftable[flowId] = temp;
    EV << "Just created flow entry for flowId " << flowId\
            << " e.g., destination is " << ftable.at(flowId).destination_ << ".\n";
}


void RcpHost::setupSignals(int flowId) {
    std::stringstream ss;
    for (const auto& statName : statNames ) {
        ss.clear(); ss.str("");
        //ss << flowId << ":" << address << "->" << ftable.at(flowId).destination_ << "-" << statName;
        ss << "session-" << flowId << "-" << statName;
        ftable.at(flowId).signals[ss.str()] = registerSignal(ss.str().c_str());

        std::stringstream ts;
        ts.clear(); ts.str("");
        ts << "session" << statName;

        cProperty *statisticTemplate =\
        getProperties()->get("statisticTemplate", ts.str().c_str());
        ev.addResultRecorders(this, ftable.at(flowId).signals[ss.str()],\
                ss.str().c_str(), statisticTemplate);
    }
}

// void RcpHost::reset() // not used in this version of RCP.


void RcpHost::start(int flowId)
{
    double now = SIMTIME_DBL(simTime());

    setRcpState(flowId, RCP_RUNNING);

    if( ftable.at(flowId).interval_ > REF_INTVAL * ftable.at(flowId).min_rtt_ ){  // At this moment min_rtt_ has been set (by SYNACK)
        set_timeout(flowId, REF_TIMEOUT, REF_INTVAL * ftable.at(flowId).min_rtt_);
        setRcpState(flowId, RCP_RUNNING_WREF);
    } else {
        setRcpState(flowId, RCP_RUNNING);
        cancel_timeout(flowId, REF_TIMEOUT);
    }
    signalRate(flowId);
    sendFlowMsg(flowId, ftable.at(flowId).transmission_rate_);

    rcp_timeout(flowId);
}

void RcpHost::stop(int flowId)
{
    cancel_timeout(flowId, RCP_TIMEOUT);
    cancel_timeout(flowId, REF_TIMEOUT);
    cancel_timeout(flowId, RTO_TIMEOUT);

    finishRcp(flowId);
}

void RcpHost::pause(int flowId) {
    cancel_timeout(flowId, RCP_TIMEOUT);
    cancel_timeout(flowId, REF_TIMEOUT);
    cancel_timeout(flowId, RTO_TIMEOUT);
    setRcpState(flowId, RCP_INACT);
}


void RcpHost::set_timeout(int flowId, int timeout_type, double next_interval) {
    RcpTimeoutPacket *timeout_pkt_ = ftable.at(flowId).rcp_timeout_pkt_; // default
    if(timeout_type == REF_TIMEOUT)
        timeout_pkt_ = ftable.at(flowId).ref_timeout_pkt_;
    else if (timeout_type == RTO_TIMEOUT)
        timeout_pkt_ = ftable.at(flowId).rto_timeout_pkt_;
    if(timeout_pkt_->isScheduled())
        cancelEvent(timeout_pkt_);
    EV << "timeout_pkt type " << timeout_type << "scheduled for" << simTime() + next_interval << " (interval: " << next_interval << ")\n";
    scheduleAt(simTime() + next_interval, timeout_pkt_);
}
void RcpHost::cancel_timeout(int flowId, int timeout_type) {
    RcpTimeoutPacket *timeout_pkt_ = ftable.at(flowId).rcp_timeout_pkt_; // default
    if(timeout_type == REF_TIMEOUT)
        timeout_pkt_ = ftable.at(flowId).ref_timeout_pkt_;
    else if (timeout_type == RTO_TIMEOUT)
        timeout_pkt_ = ftable.at(flowId).rto_timeout_pkt_;

    cancelEvent(timeout_pkt_);
}

void RcpHost::sendfile(int flowId) {
    emit(startSignal, flowId);
    emit(lengthSignal, ftable.at(flowId).num_pkts_);


    EV << "Sending start flow message to flow generator (I'm about to send SYN).";
    CharnyMakeFlowMsg * startMsg = new CharnyMakeFlowMsg(); // deleted by (control) host
    *startMsg = *ftable.at(flowId).flowmsg;
    startMsg->setChangeType(CHARNY_MAKEFLOW_SYN);
    send(startMsg, "flowAlert$o");

    std::stringstream pktName;
    pktName << "RCP_SYN:" << flowId << ":" << address << "->" << ftable.at(flowId).destination_;
    RcpPacket *pkt = new RcpPacket(pktName.str().c_str());
    pkt->setSource(address);
    pkt->setDestination(ftable.at(flowId).destination_);
    pkt->setByteLength(header_size_); // syn length
    pkt->setSeqNo(ftable.at(flowId).ref_seqno_++);
    pktName << "::" << ftable.at(flowId).seqno_;
    pkt->setName(pktName.str().c_str());

    pkt->setTs(SIMTIME_DBL(simTime()));
    pkt->setRtt(ftable.at(flowId).rtt_); // whatever

    pkt->setPktType(RCP_SYN);
    pkt->setRcpRate(-1); // whatever

    pkt->setNumPktsRecv(ftable.at(flowId).num_datapkts_received_);
    pkt->setFlowId(flowId);

    forwardPacket(pkt);
    setRcpState(flowId, RCP_SYNSENT);
    ftable.at(flowId).lastpkttime_ = SIMTIME_DBL(simTime());
    set_timeout(flowId, REF_TIMEOUT, SYN_DELAY);
}


void RcpHost::rcp_timeout(int flowId)
{
    if (ftable.at(flowId).RCP_state == RCP_FINTRIGGER) {
        // fin caused by FlowGenerator
        EV << "flow " << flowId << " received FlowGenerator fin signal.\n";
        double now = SIMTIME_DBL(simTime());
        sendlast(flowId);
        setRcpState(flowId, RCP_FINSENT);
        cancel_timeout(flowId, RCP_TIMEOUT);
        cancel_timeout(flowId, REF_TIMEOUT);
        set_timeout(flowId, RTO_TIMEOUT, 2*ftable.at(flowId).rtt_);

    } else if (ftable.at(flowId).RCP_state == RCP_RUNNING || ftable.at(flowId).RCP_state == RCP_RUNNING_WREF) {
        if (ftable.at(flowId).num_sent_ < ftable.at(flowId).num_pkts_ - 1 ||
                ftable.at(flowId).num_pkts_ == -1) {


            sendpkt(flowId);
            set_timeout(flowId, RCP_TIMEOUT, ftable.at(flowId).interval_);

        } else {
            double now = SIMTIME_DBL(simTime());
            sendlast(flowId);
            setRcpState(flowId, RCP_FINSENT);
            cancel_timeout(flowId, RCP_TIMEOUT);
            cancel_timeout(flowId, REF_TIMEOUT);
            set_timeout(flowId, RTO_TIMEOUT, 2*ftable.at(flowId).rtt_);

        }

    } else if (ftable.at(flowId).RCP_state == RCP_RETRANSMIT) {

          if (ftable.at(flowId).num_pkts_resent_ < ftable.at(flowId).num_pkts_to_retransmit_ - 1) {
              sendpkt(flowId);
              set_timeout(flowId, RCP_TIMEOUT, ftable.at(flowId).interval_);
          } else if (ftable.at(flowId).num_pkts_resent_ == ftable.at(flowId).num_pkts_to_retransmit_ - 1) {
              sendpkt(flowId);
              cancel_timeout(flowId, RCP_TIMEOUT);
              cancel_timeout(flowId, REF_TIMEOUT);
              set_timeout(flowId, RTO_TIMEOUT, 2*ftable.at(flowId).rtt_);
          }
    }


}


void RcpHost::rto_timeout(int flowId)
{
    setRcpState(flowId, RCP_RETRANSMIT);
    ftable.at(flowId).num_enter_retransmit_mode_++;

    ftable.at(flowId).num_pkts_resent_ = 0;
    ftable.at(flowId).num_pkts_to_retransmit_ = ftable.at(flowId).num_pkts_ - ftable.at(flowId).num_datapkts_acked_by_receiver_;
    if (ftable.at(flowId).num_pkts_to_retransmit_ > 0)
        set_timeout(flowId, RCP_TIMEOUT, ftable.at(flowId).interval_);
}

void RcpHost::ref_timeout(int flowId)
{
    if (ftable.at(flowId).RCP_state==RCP_SYNSENT || ftable.at(flowId).RCP_state==RCP_RUNNING ||
            ftable.at(flowId).RCP_state==RCP_CONGEST || ftable.at(flowId).RCP_state == RCP_RUNNING_WREF){
        std::stringstream pktName;
        pktName << "RCP_REF:" << flowId << ":" << address << "->" << ftable.at(flowId).destination_;
        pktName << "::" << ftable.at(flowId).seqno_;
        RcpPacket *pkt = new RcpPacket(pktName.str().c_str());
        pkt->setByteLength(header_size_); // syn length

        pkt->setSource(address);
        pkt->setDestination(ftable.at(flowId).destination_);
        pkt->setSeqNo(ftable.at(flowId).seqno_);
        ftable.at(flowId).ref_seqno_ ++;
        pkt->setTs(SIMTIME_DBL(simTime()));
        pkt->setRtt(ftable.at(flowId).rtt_);

        if (ftable.at(flowId).RCP_state == RCP_SYNSENT) {

            CharnyMakeFlowMsg * startMsg = new CharnyMakeFlowMsg(); // deleted by (control) host
            *startMsg = *ftable.at(flowId).flowmsg;
            startMsg->setChangeType(CHARNY_MAKEFLOW_SYN);
            send(startMsg, "flowAlert$o");

            pkt->setPktType(RCP_SYN);;
           // fprintf(stdout, "%lf %s Sending SYN packet again... \n", Scheduler::instance().clock(), this->name());
        } else {
            pkt->setPktType(RCP_REF);
            ftable.at(flowId).numoutrefs_++;
        }

        pkt->setRcpRate(-1);
        pkt->setNumPktsRecv(ftable.at(flowId).num_datapkts_received_);
        pkt->setFlowId(flowId);

        forwardPacket(pkt);

        set_timeout(flowId, REF_TIMEOUT, std::min(SYN_DELAY, REF_INTVAL * ftable.at(flowId).min_rtt_));
    }
}

void RcpHost::finishRcp(int flowId)
{
    double now = SIMTIME_DBL(simTime());

    setRcpState(flowId, RCP_INACT);

}


void RcpHost::signalRate(int flowId) {
    emitSignal(flowId, "Rate", ftable.at(flowId).transmission_rate_);
    emitSignal(flowId, "Interval", ftable.at(flowId).interval_);

//
//    if(pkt->getPktType() == RCP_REFACK)
//        return;
//    if(flowId == 0) {
//        ss.clear(); ss.str(""); ss << flowId << "-TransmissionRate";
//        emit(ftable.at(flowId).signals[ss.str()], ftable.at(flowId).transmission_rate_);
//        ss.clear(); ss.str(""); ss << flowId << "-Rtt";
//        emit(ftable.at(flowId).signals[ss.str()], ftable.at(flowId).rtt_);
//    }
}

void RcpHost::recv(RcpPacket *pkt)
{
    int flowId = pkt->getFlowId();
    if(ftable.find(flowId) == ftable.end()) { // this is a receiver. sending only ACKs
        createFlowEntry(flowId, NULL);
        // not setting up any signals at receiver
        ftable.at(flowId).destination_ = pkt->getSource();
        ftable.at(flowId).seqno_ = pkt->getSeqNo();
        EV << "Local" << address << "set up ACK ftable entry.\n";

    }

    emitSignal(pkt->getFlowId(), "PktType", pkt->getPktType());

    EV << "Local" << address << " pktType:" << pkt->getPktType()\
            << ", RCP_STATE:" << rcpHostStateStr[ftable.at(flowId).RCP_state] << "\n";
    if ( (pkt->getPktType() == RCP_SYN) || ((ftable.at(flowId).RCP_state != RCP_INACT)) ) {
        switch (pkt->getPktType()) {
        case RCP_SYNACK:
            ftable.at(flowId).rtt_ = SIMTIME_DBL(simTime()) - pkt->getTs();
            EV << "orig packet sent at: " << pkt->getTs() << ", calculated RTT: " << ftable.at(flowId).rtt_ << "\n";
            if (ftable.at(flowId).min_rtt_ > ftable.at(flowId).rtt_)
                ftable.at(flowId).min_rtt_ = ftable.at(flowId).rtt_;
            if (pkt->getRcpRate() > 0) {
                double now = SIMTIME_DBL(simTime());
                ftable.at(flowId).interval_ = (payload_size_+header_size_)/(pkt->getRcpRate());
                ftable.at(flowId).transmission_rate_ = pkt->getRcpRate();
                if (ftable.at(flowId).RCP_state == RCP_SYNSENT)
                    start(flowId);
            }
            else {
                if (pkt->getRcpRate() < 0)
                    EV << "Error: RCP rate < 0: " << pkt->getRcpRate() << "\n";
                if (uniform(0,1)< QUIT_PROB) { //sender decides to stop
                    pause(flowId);
                    double now = SIMTIME_DBL(simTime());
                    EV << "quit by QUIT_PROB: " << flowId << "\n";
                }
                else {
                    setRcpState(flowId, RCP_CONGEST);
                }
            }
            break;
        case RCP_REFACK:
            if ( (pkt->getSeqNo() <= ftable.at(flowId).seqno_)  && ftable.at(flowId).RCP_state != RCP_INACT){
                ftable.at(flowId).numoutrefs_--;
                if (ftable.at(flowId).numoutrefs_ < 0) {
                    EV << flowId << ": Extra REF_ACK received! \n";
                    {
                        if(ftable.at(flowId).RCP_state == RCP_INACT)
                            EV << "RCP_INACT\n";
                        if(ftable.at(flowId).RCP_state == RCP_SYNSENT)
                            EV << "RCP_SYNSENT\n";
                        if(ftable.at(flowId).RCP_state == RCP_RUNNING)
                            EV << "RCP_RUNNING\n";
                        if(ftable.at(flowId).RCP_state == RCP_RUNNING_WREF)
                            EV << "RCP_RUNNING_WREF\n";
                        if(ftable.at(flowId).RCP_state == RCP_CONGEST)
                            EV << "RCP_CONGEST\n";
                    }
                    exit(1);
                }

                ftable.at(flowId).rtt_ = SIMTIME_DBL(simTime()) - pkt->getTs();
                if (ftable.at(flowId).min_rtt_ > ftable.at(flowId).rtt_) {
                    ftable.at(flowId).min_rtt_ = ftable.at(flowId).rtt_;

                }
                emitSignal(flowId, "Rtt", ftable.at(flowId).rtt_);
                emitSignal(flowId, "MinRtt", ftable.at(flowId).min_rtt_);


                if (pkt->getRcpRate() > 0) {
                    double oldRate = ftable.at(flowId).transmission_rate_;
                    double new_interval = ((payload_size_+header_size_))/(pkt->getRcpRate());
                    if( new_interval != ftable.at(flowId).interval_ ){
                        ftable.at(flowId).interval_ = new_interval;
                        ftable.at(flowId).transmission_rate_ = pkt->getRcpRate();
                        if (ftable.at(flowId).RCP_state == RCP_CONGEST)
                            start(flowId);
                        else {
                            if (ftable.at(flowId).transmission_rate_ != oldRate) {
                                sendFlowMsg(flowId, ftable.at(flowId).transmission_rate_*8);
                            }
                            set_next_packet_timeout(flowId);
                        }
                       }
                }
                else {
                    if (pkt->getRcpRate() < 0)
                        EV << "Error: RCP rate < 0: " << pkt->getRcpRate() << "\n";
                    cancel_timeout(flowId, RCP_TIMEOUT);
                    setRcpState(flowId, RCP_CONGEST); //can do exponential backoff or probalistic stopping here.
                }
            }
            break;
        case RCP_ACK:
            ftable.at(flowId).num_datapkts_acked_by_receiver_ = pkt->getNumPktsRecv();
            if (ftable.at(flowId).num_datapkts_acked_by_receiver_ == ftable.at(flowId).num_pkts_) {
                stop(flowId);
            }

            ftable.at(flowId).rtt_ = SIMTIME_DBL(simTime()) - pkt->getTs();

            if (ftable.at(flowId).min_rtt_ > ftable.at(flowId).rtt_) {
                ftable.at(flowId).min_rtt_ = ftable.at(flowId).rtt_;

            }

            emitSignal(flowId, "Rtt", ftable.at(flowId).rtt_);
            emitSignal(flowId, "MinRtt", ftable.at(flowId).min_rtt_);

            if (pkt->getRcpRate() > 0) {
                double oldRate = ftable.at(flowId).transmission_rate_;
                double new_interval = ((payload_size_+header_size_))/(pkt->getRcpRate());
                if( new_interval != ftable.at(flowId).interval_ ){
                    ftable.at(flowId).interval_ = new_interval;
                    ftable.at(flowId).transmission_rate_ = pkt->getRcpRate();
                    if (ftable.at(flowId).RCP_state == RCP_CONGEST)
                        start(flowId);
                    else {
                        if (ftable.at(flowId).transmission_rate_ != oldRate) {
                        sendFlowMsg(flowId, ftable.at(flowId).transmission_rate_);
                       }
                        set_next_packet_timeout(flowId);
                    }
                }
            }
            else {
                if (pkt->getRcpRate() < 0)
                    EV << "Error: RCP rate < 0: " << pkt->getRcpRate() << "\n";
                cancel_timeout(flowId, RCP_TIMEOUT);
                setRcpState(flowId, RCP_CONGEST); //can do exponential backoff or probalistic stopping here.
            }
            break;
        case RCP_FIN:
             {


             ftable.at(flowId).num_datapkts_received_++; // because RCP_FIN is piggybacked on the last packet of flow
             RcpPacket *newpkt = ack_pkt(flowId, RCP_FINACK, pkt);
             forwardPacket(newpkt);
             break;
             }
        case RCP_SYN:
        {
            RcpPacket *newpkt = ack_pkt(flowId, RCP_SYNACK, pkt);
            forwardPacket(newpkt);
            setRcpState(flowId, RCP_RUNNING); // Only the receiver changes state here
            break;
        }
        case RCP_FINACK:
        {
            ftable.at(flowId).num_datapkts_acked_by_receiver_ = pkt->getNumPktsRecv();

            // tell flow generator that flow just ended
            CharnyMakeFlowMsg* endMsg = new CharnyMakeFlowMsg(*ftable.at(flowId).flowmsg);
            endMsg->setRate(ftable.at(flowId).transmission_rate_*8); // transmission_rate_ in Bps
            endMsg->setChangeType(CHARNY_MAKEFLOW_FINACK); // in CharnyDataHost changed on receipt
            send(endMsg, "flowAlert$o");

            if (ftable.at(flowId).num_datapkts_acked_by_receiver_ == ftable.at(flowId).num_pkts_)
                stop(flowId);


            break;
        }
        case RCP_REF:
        {
            RcpPacket *newpkt = ack_pkt(flowId, RCP_REFACK, pkt);
            forwardPacket(newpkt);
            break;
        }
        case RCP_DATA:
        {
        ftable.at(flowId).num_datapkts_received_++;
        RcpPacket *newpkt = ack_pkt(flowId, RCP_ACK, pkt);
        forwardPacket(newpkt);
        break;
        }
        case RCP_OTHER:
            EV << "received RCP_OTHER\n";
            exit(1);
            break;
        default:
            EV << "Unknown RCP packet type!\n";
            exit(1);
            break;
        } // end switch
    }
    delete pkt;
}


void RcpHost::sendFlowMsg(int flowId, double newRate) {
    flowEntry_t &session = ftable.at(flowId);
    CharnyMakeFlowMsg* msg = new CharnyMakeFlowMsg(*session.flowmsg);
    msg->setRate(newRate*8); // rate is in Bytes per second
    msg->setChangeType(CHARNY_MAKEFLOW_CHANGE_RATE);
    EV << "After " << session.num_sent_ << " packets sent, sending "\
    << (session.transmission_rate_ * 8)\
    << " bps rate message for flow # " << flowId\
    << " at time " << simTime().dbl() << ".\n" ;
    send(msg, "flowAlert$o");
}

void RcpHost::set_next_packet_timeout(int flowId) // on receiver side
{
    if (ftable.at(flowId).RCP_state == RCP_RUNNING || ftable.at(flowId).RCP_state == RCP_RUNNING_WREF) {
        cancel_timeout(flowId, RCP_TIMEOUT);

        double t = ftable.at(flowId).lastpkttime_ + ftable.at(flowId).interval_;
        double now = SIMTIME_DBL(simTime());

        if ( t > now) {
            set_timeout(flowId, RCP_TIMEOUT, t - now);

            if( ftable.at(flowId).interval_ > REF_INTVAL * ftable.at(flowId).min_rtt_ &&
                    ftable.at(flowId).RCP_state != RCP_RUNNING_WREF ){
            // the inter-packet time > min_rtt and not in REF mode. Enter REF MODE.
                setRcpState(flowId, RCP_RUNNING_WREF);
                if( ftable.at(flowId).lastpkttime_ + REF_INTVAL * ftable.at(flowId).min_rtt_ > now ){
                    set_timeout(flowId, REF_TIMEOUT,
                            ftable.at(flowId).lastpkttime_ + REF_INTVAL * ftable.at(flowId).min_rtt_ - now);
                } else {
                    ref_timeout(flowId);  // send ref packet now.
                }
            }else if (ftable.at(flowId).interval_ <= REF_INTVAL * ftable.at(flowId).min_rtt_ &&
                    ftable.at(flowId).RCP_state == RCP_RUNNING_WREF ){
            // the inter-packet time <= min_rtt and in REF mode.  Exit REF MODE
                setRcpState(flowId, RCP_RUNNING);
                cancel_timeout(flowId, REF_TIMEOUT);
            }

        } else {
            rcp_timeout(flowId); // send a packet immediately and reschedule timer
            if( ftable.at(flowId).interval_ > REF_INTVAL * ftable.at(flowId).min_rtt_ && ftable.at(flowId).RCP_state != RCP_RUNNING_WREF ){
            // the next packet sendingtime > min_rtt and not in REF mode. Enter REF MODE.
                setRcpState(flowId, RCP_RUNNING_WREF);
                set_timeout(flowId, REF_TIMEOUT, REF_INTVAL * ftable.at(flowId).min_rtt_);
            }else if ( ftable.at(flowId).interval_ <= REF_INTVAL * ftable.at(flowId).min_rtt_ &&
                    ftable.at(flowId).RCP_state == RCP_RUNNING_WREF ){
            // the next packet sending time <= min_rtt and in REF mode.  Exit REF MODE
                setRcpState(flowId, RCP_RUNNING);
                cancel_timeout(flowId, REF_TIMEOUT);
            }
        }
    }
    signalRate(flowId);
}

void RcpHost::sendpkt(int flowId)
{

    // tell flow generator that flow about to send first data packet
    if (ftable.at(flowId).seqno_ == 0) {
        CharnyMakeFlowMsg* startMsg = new CharnyMakeFlowMsg();
        *startMsg = *ftable.at(flowId).flowmsg;
        startMsg->setChangeType(CHARNY_MAKEFLOW_FIRST_DATA); // in CharnyDataHost changed on receipt
        send(startMsg, "flowAlert$o");
    }

    emit(startSignal, flowId);
    emit(lengthSignal, ftable.at(flowId).num_pkts_);
    std::stringstream pktName;
    pktName << "RCP_DATA:" << flowId << ":" << address << "->" << ftable.at(flowId).destination_;
    pktName << "::" << ftable.at(flowId).seqno_;
    RcpPacket *pkt = new RcpPacket(pktName.str().c_str());
    pkt->setByteLength(payload_size_+header_size_);

    pkt->setSource(address);
    pkt->setDestination(ftable.at(flowId).destination_);

    pkt->setSeqNo(ftable.at(flowId).seqno_++);
    pkt->setTs(SIMTIME_DBL(simTime()));
    pkt->setRtt(ftable.at(flowId).rtt_);
    pkt->setPktType(RCP_DATA);
    pkt->setRcpRate(-1);
    pkt->setNumPktsRecv(ftable.at(flowId).num_datapkts_received_);
    pkt->setFlowId(flowId);

    ftable.at(flowId).lastpkttime_ = SIMTIME_DBL(simTime());
    forwardPacket(pkt);

    if (ftable.at(flowId).RCP_state == RCP_RETRANSMIT)
        ftable.at(flowId).num_pkts_resent_++;
    else
        ftable.at(flowId).num_sent_++;
    if(flowId == 0) {
        std::stringstream ss; ss.clear(); ss.str("");
        ss << "session-" << flowId << "-NumPkts";
        emit(ftable.at(flowId).signals[ss.str()], ftable.at(flowId).seqno_);
    }
}

RcpPacket *RcpHost::ack_pkt(int flowId, int packet_type, RcpPacket *pkt)
{
    RcpPacket *newpkt = pkt->dup();

    std::stringstream pktName;
    switch(packet_type)
    {
    case RCP_SYNACK:
        pktName << "RCP_SYNACK:";
        break;
    case RCP_REFACK:
        pktName << "RCP_REFACK:";
        break;
    case RCP_ACK:
        pktName << "RCP_ACK:";
        break;
    case RCP_FINACK:
        pktName << "RCP_FINACK:";
        break;
    }
    pktName << flowId << ":" << pkt->getSource() << "->" << pkt->getDestination();


    pktName << "::" << pkt->getSeqNo();
    newpkt->setName(pktName.str().c_str());
    newpkt->setByteLength(header_size_);

    newpkt->setSource(address);
    newpkt->setDestination(ftable.at(flowId).destination_);
    newpkt->setPktType(packet_type);
    double copy_rate = pkt->getRcpRate(); // Can modify the rate here.
    newpkt->setRcpRate(copy_rate);
    newpkt->setNumPktsRecv(ftable.at(flowId).num_datapkts_received_);
    return newpkt;
}

void RcpHost::sendlast(int flowId)
{
    // tell flow generator that flow about to send first data packet (could be FIN)
      if (ftable.at(flowId).seqno_ == 0) {
        CharnyMakeFlowMsg* startMsg = new CharnyMakeFlowMsg();
        *startMsg = *ftable.at(flowId).flowmsg;
         startMsg->setChangeType(CHARNY_MAKEFLOW_FIRST_DATA); // in CharnyDataHost changed on receipt
         send(startMsg, "flowAlert$o");
      }

      // tell flow generator that flow just ended
      CharnyMakeFlowMsg* endMsg = new CharnyMakeFlowMsg(*ftable.at(flowId).flowmsg);
      endMsg->setRate(ftable.at(flowId).transmission_rate_*8); // transmission_rate_ in Bps
      endMsg->setChangeType(CHARNY_MAKEFLOW_FIN); // in CharnyDataHost changed on receipt
      send(endMsg, "flowAlert$o");

    emit(startSignal, flowId);
    emit(lengthSignal, ftable.at(flowId).num_pkts_);
    std::stringstream pktName;
    pktName << "RCP_FIN:" << flowId << ":" << address << "->" << ftable.at(flowId).destination_;
    pktName << "::" << ftable.at(flowId).seqno_;
    RcpPacket *pkt = new RcpPacket(pktName.str().c_str());
    pkt->setByteLength(payload_size_+header_size_);

    pkt->setSource(address);
    pkt->setDestination(ftable.at(flowId).destination_);

    pkt->setSeqNo(ftable.at(flowId).seqno_++);
    pkt->setTs(SIMTIME_DBL(simTime()));
    pkt->setRtt(ftable.at(flowId).rtt_);
    pkt->setPktType(RCP_FIN);
    pkt->setRcpRate(-1);
    pkt->setNumPktsRecv(ftable.at(flowId).num_datapkts_received_);
    pkt->setFlowId(flowId);

    ftable.at(flowId).lastpkttime_ = SIMTIME_DBL(simTime());
    ftable.at(flowId).num_sent_++;
    forwardPacket(pkt);
}

void RcpHost::forwardPacket(RcpPacket *pkt)
{
    EV << "Local " << address << ": Created " << pkt\
            << " from " << pkt->getSource() << " to " << pkt->getDestination()\
            << ", send to ingress\n";
    send(pkt, "gate$o");
}


}; // namespace
