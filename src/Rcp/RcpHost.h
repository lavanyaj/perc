//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __RCP_RCPHOST_H
#define __RCP_RCPHOST_H

#include <omnetpp.h>
#include "util.h"
#include "generic_m.h"
#include "rcp_m.h"
#include "charnymakeflow_m.h"
#include <map>
#include <utility>
namespace rcp {

struct flowEntry_t {
    double rtt_;
    double interval_;
    int num_sent_;
    int RCP_state;
    int numoutrefs_;
    double min_rtt_;
    int ref_seqno_;

    int num_datapkts_acked_by_receiver_;
    int num_datapkts_received_;
    int num_pkts_to_retransmit_;
    int num_pkts_resent_;
    int num_enter_retransmit_mode_;

    int seqno_;
    int num_pkts_;
    double lastpkttime_;
    int destination_;

    double transmission_rate_;
    CharnyMakeFlowMsg *flowmsg;
    RcpTimeoutPacket *timeoutpkt_;

    std::map<std::string, simsignal_t> signals;;
    RcpTimeoutPacket *rcp_timeout_pkt_;
    RcpTimeoutPacket *ref_timeout_pkt_;
    RcpTimeoutPacket *rto_timeout_pkt_;
};

/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class RcpHost : public cSimpleModule
{
  private:
    double header_size_;
    double payload_size_;

    double SYN_DELAY; // 0.5
    double REF_FACT; //1  //Number of REFs per RTT
    double QUIT_PROB;// 0.1  //probability of quitting if assigned rate is zero
    double REF_INTVAL;// 1.0

    typedef std::map<int, flowEntry_t> FlowTable;
    FlowTable ftable;
//    typedef std::map<int,
//            (std::tuple<double, double,
//                int, int, int, double, int,
//                int, int, int, int, int,
//                int, int, double, int>)> FlowTable; // destaddr -> gateindex
//    FlowTable ftable; // flowID -> FLOW_STATE enum
    simsignal_t startSignal;
    simsignal_t endSignal;
    simsignal_t lengthSignal;
    int address; // RcpHost index for now
    std::vector<std::string> statNames;
    double scalar;

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
    virtual void createFlowEntry(int flowId, CharnyMakeFlowMsg *flowmsg);
    virtual void setupSession(CharnyMakeFlowMsg *flowmsg);
    virtual void tearDownSession(int flowId);
    virtual void setupSignals(int flowId);

    virtual void setRcpState(int flowId, int value);
    virtual int getRcpState(int flowId);
    virtual void forwardPacket(RcpPacket *pkt);


    virtual void set_timeout(int flowId, int timeout_type, double next_interval);
    virtual void cancel_timeout(int flowId, int timeout_type);
    virtual void rcp_timeout(int flowId);
    virtual void ref_timeout(int flowId);
    virtual void rto_timeout(int flowId);
    virtual void emitSignal(int flowId, std::string name, double value);
    virtual void set_next_packet_timeout(int flowId);
    virtual void signalRate(int flowId);
    virtual void sendfile(int flowId); // syn
    virtual void start(int flowId); // data start

    virtual void finishRcp(int flowId);
    virtual void pause(int flowId);
    virtual void stop(int flowId);

    virtual void sendpkt(int flowId);
    virtual void sendlast(int flowId);
    virtual void recv(RcpPacket *pkt);
    // send rate change message to flow generator
    void sendFlowMsg(int flowId, double newRate);

    virtual RcpPacket *ack_pkt(int flowId, int packet_type, RcpPacket *oldpkt);
};


}; // namespace

#endif
