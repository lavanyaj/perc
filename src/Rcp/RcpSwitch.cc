#include "RcpSwitch.h"
#include "waterfilling.h"
#include <cstring>

namespace rcp {
#ifndef EV1
#define EV1 if (false) EV

//const double RTT_GAIN = 0.02;
Define_Module(RcpSwitch);

/*
 * Note: this version of RCP only records RTT and stamps rates on packets for OUTGOING LINKS.
 * If a packet comes in and is destined for this switch, no rates or RTTs are stamped for it.
 * So packets on incoming links will only get rates stamped for their outgoing links; the previous
 * hop switch is responsible for that incoming link.
 * In this way, the last switch in any path does not modify or record rates/RTTs.
 */



void RcpSwitch::initialize()
{

    init_numflows_ = par("initNumFlows").doubleValue();
    alpha_ = par("alpha").doubleValue();
    beta_ = par("beta_").doubleValue();
    upd_timeslot_ = par("updTimeslot").doubleValue();
    gamma_ = par("gamma").doubleValue();
    min_pprtt_ = par("minPprtt").doubleValue();
    init_rate_fact_ = par("initRateFact").doubleValue();
    rtt_gain_ = par("rttGain").doubleValue();



    address = getParentModule()->getIndex();
    numLinks = gateSize("port"); // OUTGOING links.
    scalar = 8*1e-6; // Bytes -> bits, Mbps
    noMoreTimeouts = false;

    ////////// LINK INFO SETUP
    for (int k = 0; k < numLinks; k++)
    {
        std::stringstream qrpname; // queue request packet
        qrpname << address << ":queue_request[" << k <<"]";
        std::stringstream qtpname; // queue timeout packet
        qtpname << address << ":queue_timeout[" << k <<"]";
        std::stringstream stpname; // stats timeout packet
        stpname << address << ":stats_timeout[" << k <<"]";
        queueEntry_t temp = {
            new cPacketQueue(), // cPacketQueue* queue;
            check_and_cast<cDatarateChannel *> \
                (getParentModule()->gate("port$o", k)->getChannel()),// cDatarateChannel* chan;
            new QueueRequestPacket(qrpname.str().c_str()), // QueueRequestPacket* qrp;
            new QueueTimeoutPacket(qtpname.str().c_str()),
            -1, // double link_capacity_;
            0.0, // double input_traffic_;
            0.0, // double act_input_traffic_;
            0.0, // double output_traffic_;
            0, // double last_load_;
            0.0, // double traffic_spill_;
            init_numflows_, // init num_flows_;
            -1, // double avg_rtt_;
            0, // double this_Tq_rtt_sum_;
            0, // double this_Tq_rtt_;
            0, // double this_Tq_rtt_numPkts_;
            0, // int input_traffic_rtt_;
            rtt_gain_, // double rtt_moving_gain_;
            0, // double Q_;
            0, // double Q_last;
            -1, // double flow_rate_;
            -1, // double Tq_;
            SIMTIME_DBL(simTime()), // double end_slot_;
            std::map<std::string, simsignal_t>(), // signal
            new StatsTimeoutPacket(qtpname.str().c_str()),
            std::map<std::string, double>(),
            SimTime(0)
        };
        temp.qrp->setQueueIndex(k);
        temp.qtp->setQueueIndex(k);
        temp.link_capacity_ = (temp.chan)->getDatarate()/8; // bits->Bytes
        temp.avg_rtt_ = SIMTIME_DBL((temp.chan)->getDelay())*2; // minimum RTT
        temp.Tq_ = std::min(temp.avg_rtt_, upd_timeslot_);
        temp.flow_rate_ = temp.link_capacity_ * init_rate_fact_;

        temp.stp->setQueueIndex(k);
        temp.stats["arrival"] = 0;
        temp.stats["service"] = 0;
        temp.stats["arrivalRef"] = 0;
        temp.stats["serviceRef"] = 0;
        temp.stats["arrivalData"] = 0;
        temp.stats["serviceData"] = 0;
        temp.statsTimeoutInterval = SimTime(par("statsTimeoutInterval").doubleValue());

        EV << "Scheduled stats timeout at " << temp.statsTimeoutInterval << " seconds.\n";
         scheduleAt(temp.statsTimeoutInterval, temp.stp);

        // find link's other end
        cGate* connect_gate = getParentModule()->gate("port$o", k)->getNextGate();
        std::stringstream qname;
        qname.clear();
        qname << address << "." << k << "->" << \
                connect_gate->getOwnerModule()->getIndex() << "." << \
                connect_gate->getIndex();
        temp.queue->setName(qname.str().c_str());

        // Scheduling queue_timer randomly so that routers are not synchronized
        double T = normal(temp.Tq_, 0.2*temp.Tq_);

        temp.end_slot_ = SIMTIME_DBL(simTime()) + T;
        EV << "Scheduled queue timeout at " << temp.end_slot_ << " seconds.\n";
        scheduleAt(temp.end_slot_, temp.qtp);

        // statistics
        const char *vinit[] = {"queueLength", "queueByteLength", "flowRate", "Rtt", "AvailableRate","QueueDrain", "InputTraffic",\
                "arrivalRate", "serviceRate", "arrivalRefRate", "serviceRefRate", "arrivalDataRate",\
                "serviceDataRate", "interPacketTime"};
        std::vector<std::string> statNames(vinit, vinit+sizeof(vinit)/sizeof(char*));
        for (const auto& statName : statNames ) {
            std::stringstream ss;
            ss.clear(); ss.str("");
            ss << qname.str() << "-" << statName;
            temp.signals[ss.str()] = registerSignal(ss.str().c_str());

            std::stringstream ts;
            ts.clear(); ts.str("");
            ts << statName;
            cProperty *statisticTemplate =\
                    getProperties()->get("statisticTemplate", ts.str().c_str());
            ev.addResultRecorders(this, temp.signals[ss.str()],\
                    ss.str().c_str(), statisticTemplate);
        }
        qtable[k] = temp;
    }

    ////////// ROUTING
    // From Routing.cc
    cTopology *topo = new cTopology("topo");

    std::vector<std::string> nedTypes;
    nedTypes.push_back(getParentModule()->getNedTypeName());
    topo->extractByNedTypeName(nedTypes);
    EV << "Node " << address << ": cTopology found " << topo->getNumNodes() << " nodes\n";

    cTopology::Node *thisNode = topo->getNodeFor(getParentModule());

    // find and store next hops
    for (int i=0; i<topo->getNumNodes(); i++)
    {
        if (topo->getNode(i)==thisNode) continue; // skip ourselves
        topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(i));

        if (thisNode->getNumPaths()==0) continue; // not connected

        cGate *parentModuleGate = thisNode->getPath(0)->getLocalGate();
        int gateIndex = parentModuleGate->getIndex();
        //int address = topo->getNode(i)->getModule()->par("address");
        int destaddress = topo->getNode(i)->getModule()->getIndex();
        rtable[destaddress] = gateIndex;
        EV << address << "->" << destaddress << ", gateIndex: " << gateIndex << endl;
    }
    delete topo;
}

void RcpSwitch::handleMessage(cMessage *msg)
{
    // EV << "RcpSwitch"<< address <<": handleMessage(" << msg->getName() << ").\n";
    std::stringstream ss;
    if (!std::strcmp(msg->getClassName(), "StatsTimeoutPacket")) {
            StatsTimeoutPacket *stpkt = check_and_cast<StatsTimeoutPacket *> (msg);
            int k = stpkt->getQueueIndex();
            stats_timeout(k);
    } else if (!std::strcmp(msg->getClassName(), "QueueTimeoutPacket")) {
        QueueTimeoutPacket *qtpkt = check_and_cast<QueueTimeoutPacket *> (msg);
        int k = qtpkt->getQueueIndex();
        queue_timeout(k);
    } else if(!std::strcmp(msg->getClassName(), "QueueRequestPacket")){ // queue request packet
        // could have been scheduled erroneously.
        QueueRequestPacket *qrpkt = check_and_cast<QueueRequestPacket *>(msg);
        int k = qrpkt->getQueueIndex();
        if(qtable[k].chan->isBusy()) {
            EV1 << "channel was busy when queue request arrived, so rescheduling.\n";
            scheduleAt(qtable[k].chan->getTransmissionFinishTime(), qrpkt);
        } else if(!qtable[k].queue->empty()) {
            // process egress moved here!!
            RcpPacket *pkt = check_and_cast<RcpPacket *> (qtable[k].queue->pop());
            qtable[k].stats.at("service") += 1;
            processEgress(pkt, k);
            EV1 << "RcpSwitch"<< address << ": egress, sending " << pkt->getName() << "\n";
            send(pkt, "port$o", k);
            requestQueueMessage(k);
        } else {
            EV1 << "Queue request for " << k << " but queue is empty.\n";
        }
    } else if (!std::strcmp(msg->getName(), "StopSwitch")) {
        noMoreTimeouts = true;
        delete msg;
        return;
    } else { // came from ports: process send to local or process egress.
        RcpPacket *pkt = check_and_cast<RcpPacket *>(msg);
        if (!pkt) {
            EV << "RcpSwitch"<< address << "Unidentified packet at Switch.\n";
        }
        int k = findOutGate(pkt->getDestination());
        EV1 << "Packet destined for " << pkt->getDestination() << " should go out port " << k << ".\n";
        if (k == -1) // packet does not have an outgoing link
        {
            if (pkt->getDestination() == address) // local packet
            {
                EV1 << "RcpSwitch"<< address << ": sending to local:" << pkt->getName() << "\n";
                send(pkt, "local$o");
                // emit(outputIfSignal, -1); // -1: local // from Routing
            }
            else { // packet destination not found
                EV1 << "RcpSwitch"<< address << ": dest " << pkt->getDestination() << " unreachable, discarding packet " \
                        << pkt->getName() << endl;
                emit(dropSignal, (long)pkt->getByteLength());
            }
        } else {
            processIngress(pkt, k);
            EV1 << "RcpSwitch"<< address << ": ingress, processing " << pkt->getName() << "\n";
            qtable[k].stats.at("arrival") += 1;
            qtable[k].queue->insert(pkt);
            requestQueueMessage(k);
        }
    }
}

void RcpSwitch::finish() {
    for (int k = 0; k < numLinks; k++)
    {
        cancelAndDelete(qtable[k].qtp);
        cancelAndDelete(qtable[k].qrp);
        while(!qtable[k].queue->empty()) {
            RcpPacket *pkt = check_and_cast<RcpPacket *> (qtable[k].queue->pop());
            // EV << pkt->getName() << " deleting\n";
            delete pkt;
        }
        delete qtable[k].queue;
    }
}

void RcpSwitch::requestQueueMessage(int k) {
    if(!qtable[k].qrp->isScheduled()) {
        if(qtable[k].chan->isBusy()) {
            scheduleAt(qtable[k].chan->getTransmissionFinishTime(), qtable[k].qrp);

            EV1 << "RcpSwitch" << address << ": queue[" << k << "] "
                    << qtable[k].queue->length() << " packets long, "\
                    << "scheduled for " << qtable[k].chan->getTransmissionFinishTime() << "\n";
        } else {
            scheduleAt(simTime(), qtable[k].qrp);
            EV1 << "RcpSwitch" << address << ": queue[" << k << "] "
                    << qtable[k].queue->length() << " packets long, "\
                    << "scheduled for " << simTime() << "\n";
        }
   }
}

int RcpSwitch::findOutGate(int destAddr)
{
    // EV << address << ": trying to find address " << destAddr <<"\n";
    RoutingTable::iterator it = rtable.find(destAddr);
    if (it==rtable.end())
    {
        return -1;
    }
    return (*it).second; //k
}

void RcpSwitch::processIngress(RcpPacket *pkt, int k)
{
    if (pkt->getPktType() == RCP_REF)
        qtable[k].stats.at("arrivalRef") += 1;
    else if (pkt->getPktType() == RCP_DATA)
        qtable[k].stats.at("arrivalData") += 1;

    // do_on_packet_arrival(Packet *pkt)
    // Taking input traffic statistics
    int size = pkt->getByteLength();
    double pkt_time_ = pkt->getByteLength()/qtable[k].link_capacity_; // double pkt_time_ = packet_time(pkt);
    double end_time = SIMTIME_DBL(simTime()) + pkt_time_;
    double part1, part2;

    // update avg_rtt_ here
    double this_rtt = pkt->getRtt();

    if (this_rtt > 0) {
         qtable[k].this_Tq_rtt_sum_ += (this_rtt * size);
         qtable[k].input_traffic_rtt_ += size;
         qtable[k].this_Tq_rtt_ = running_avg(this_rtt, qtable[k].this_Tq_rtt_, qtable[k].flow_rate_/qtable[k].link_capacity_);
    }

    if (end_time <= qtable[k].end_slot_)
      qtable[k].act_input_traffic_ += size;
    else {
      part2 = size * (end_time - qtable[k].end_slot_)/pkt_time_;
      part1 = size - part2;
      qtable[k].act_input_traffic_ += part1;
      qtable[k].traffic_spill_ += part2;
    }

    // Can do some measurement of queue length here
    // length() in packets and byteLength() in bytes

    /* Can read the flow size from a last packet here */
}

void RcpSwitch::processEgress(RcpPacket *pkt, int k)
{   // do_before_packet_departure(Packet* p)
    int size = pkt->getByteLength();
    qtable[k].output_traffic_ += size;

    if ( pkt->getPktType() == RCP_SYN )
    {
  //    num_flows_++;
        fill_in_feedback(pkt, k);
    }
    else if (pkt->getPktType() == RCP_FIN )
    {
  //    num_flows_--;
    }
    else if ( pkt->getPktType() == RCP_REF || pkt->getPktType() == RCP_DATA )
    {
        if (pkt->getPktType() == RCP_REF)
            qtable[k].stats.at("serviceRef") += 1;
        else
            qtable[k].stats.at("serviceData") += 1;
        fill_in_feedback(pkt, k);
    }
}

void RcpSwitch::stats_timeout(int k) {
    for (auto& rt: qtable[k].stats) {
        double rate = rt.second/qtable[k].statsTimeoutInterval.dbl();

        std::stringstream ss;
            ss.clear(); ss.str("");
            ss << qtable[k].queue->getName() << "-" << rt.first << "Rate";
            EV << "Emitting " << ss.str() << " signal.\n";
            emit(qtable[k].signals[ss.str()], rate);
            rt.second = 0;

    }
    if (noMoreTimeouts) {
        // don't reschedule timeout packet
        EV << "No more stats timeouts.\n";
    } else {

        SimTime next = simTime() + qtable[k].statsTimeoutInterval;
        EV << "Schedule stats timeout for " << k << " at " << next << " again.\n";
        scheduleAt(next, qtable[k].stp);
    }

}

void RcpSwitch::queue_timeout(int k) {
    double temp;
    double datarate_fact;
    double estN1;
    double estN2;
    int Q_pkts;
    char clip;
    int Q_target_;

    double ratio;
    double input_traffic_devider_;
    double queueing_delay_;

    double virtual_link_capacity; // bytes per second
    qtable[k].last_load_ = qtable[k].act_input_traffic_/qtable[k].Tq_; // bytes per second

    qtable[k].Q_ = qtable[k].queue->getByteLength();
    Q_pkts = qtable[k].queue->length();

    qtable[k].input_traffic_ = qtable[k].last_load_;
    EV << "Queue " << qtable[k].queue->getName() << ", queue length: " << qtable[k].queue->getByteLength();
        EV << ", last load (input traffic): " << qtable[k].input_traffic_;
        EV << "\ncapacity: " << qtable[k].link_capacity_;
        EV << ", act_input_traffic: " << qtable[k].act_input_traffic_;
        EV << ", Tq: " << qtable[k].Tq_ << "\n";
        EV << "input_traffic_rtt: " << qtable[k].input_traffic_rtt_ << ", traffic spill" << qtable[k].traffic_spill_ << "\n";

    if (qtable[k].input_traffic_rtt_ > 0)
        qtable[k].this_Tq_rtt_numPkts_ = qtable[k].this_Tq_rtt_sum_/qtable[k].input_traffic_rtt_;
    EV << "this interval's rtt: " << qtable[k].this_Tq_rtt_numPkts_ << " (rtt total: " << \
            qtable[k].this_Tq_rtt_sum_ << ", bytes: " << qtable[k].input_traffic_rtt_ << ")\n";

    if (qtable[k].this_Tq_rtt_numPkts_ >= qtable[k].avg_rtt_)
        qtable[k].rtt_moving_gain_ = (qtable[k].Tq_/qtable[k].avg_rtt_);
    else
        qtable[k].rtt_moving_gain_ = // (qtable[k].flow_rate_/qtable[k].link_capacity_)* // seems too conservative?
                (qtable[k].this_Tq_rtt_numPkts_/qtable[k].avg_rtt_)*(qtable[k].Tq_/qtable[k].avg_rtt_);


    EV << "avg_rtt prev: " << qtable[k].avg_rtt_;
    qtable[k].avg_rtt_ = running_avg(qtable[k].this_Tq_rtt_numPkts_, qtable[k].avg_rtt_, qtable[k].rtt_moving_gain_);
    EV << ", now: " << qtable[k].avg_rtt_ << " (gain " << qtable[k].rtt_moving_gain_ << ", fraction:" << qtable[k].Tq_/qtable[k].avg_rtt_ << ")\n";


    estN1 = qtable[k].input_traffic_ / qtable[k].flow_rate_;
    estN2 = qtable[k].link_capacity_ / qtable[k].flow_rate_;

    virtual_link_capacity = gamma_ * qtable[k].link_capacity_;

    /* Estimate # of active flows with  estN2 = (link_capacity_/flow_rate_) */
    ratio = (1 + ((qtable[k].Tq_/qtable[k].avg_rtt_)*\
            (alpha_*(virtual_link_capacity - qtable[k].input_traffic_) - \
                    beta_*(qtable[k].Q_/qtable[k].avg_rtt_)))/virtual_link_capacity);
    temp = qtable[k].flow_rate_ * ratio;

     EV << "ratio: " << ratio;
    EV << ", link cap - input traffic: " << qtable[k].link_capacity_ - qtable[k].input_traffic_ << "\n";
    EV << "difference:" << (alpha_*(qtable[k].link_capacity_ - qtable[k].input_traffic_) - \
            beta_*(qtable[k].queue->getByteLength()/qtable[k].avg_rtt_)) << \
       ", mult by: Tq/rtt:" << qtable[k].Tq_/qtable[k].avg_rtt_ << "\n";
    EV << "temp becomes: " << temp << " vs current flow rate: "<< qtable[k].flow_rate_ << "\n";

    double new_available_rate = qtable[k].flow_rate_*(virtual_link_capacity - qtable[k].input_traffic_)/virtual_link_capacity;
    double new_queue_drain = qtable[k].flow_rate_*(qtable[k].Q_/qtable[k].avg_rtt_)/virtual_link_capacity;

    if (temp < min_pprtt_ * (1000/qtable[k].avg_rtt_) ){     // Masayoshi
        qtable[k].flow_rate_ = min_pprtt_ * (1000/qtable[k].avg_rtt_) ; // min pkt per rtt
        clip  = 'L';
    } else if (temp > virtual_link_capacity){
        qtable[k].flow_rate_ = virtual_link_capacity;
        clip = 'U';
    } else {
        qtable[k].flow_rate_ = temp;
        clip = 'M';
    }
    EV << "Queue " << qtable[k].queue->getName() << " timeout, rate update:" << qtable[k].flow_rate_ << "(clip " << clip << "), Tq: " << qtable[k].Tq_ << "\n";

    // statistics
    std::stringstream ss;
    double interPacketTime = -1;
    if (qtable[k].flow_rate_ > 0)
        interPacketTime = 1.0/(qtable[k].flow_rate_*8);
    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "interPacketTime";
    emit(qtable[k].signals[ss.str()], interPacketTime);

    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "flowRate";
    emit(qtable[k].signals[ss.str()], qtable[k].flow_rate_*8);
    ss.clear(); ss.str("");
    ss << qtable[k].queue->getName() << "-" << "queueLength";
    emit(qtable[k].signals[ss.str()], qtable[k].queue->length()); // in packets
    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "queueByteLength";
    emit(qtable[k].signals[ss.str()], (int) qtable[k].queue->getByteLength()); // in bytes
    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "Rtt";
    emit(qtable[k].signals[ss.str()], qtable[k].avg_rtt_);
    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "AvailableRate";
    emit(qtable[k].signals[ss.str()], 8*new_available_rate);
    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "QueueDrain";
    emit(qtable[k].signals[ss.str()], 8*new_queue_drain);
    ss.clear(); ss.str(""); ss << qtable[k].queue->getName() << "-" << "InputTraffic";
    emit(qtable[k].signals[ss.str()], 8*qtable[k].flow_rate_*(qtable[k].input_traffic_)/virtual_link_capacity);

    datarate_fact = qtable[k].flow_rate_/qtable[k].link_capacity_;
    qtable[k].Tq_ = std::min(qtable[k].avg_rtt_, upd_timeslot_);
    qtable[k].this_Tq_rtt_ = 0;
    qtable[k].this_Tq_rtt_sum_ = 0;
    qtable[k].input_traffic_rtt_ = 0;
    qtable[k].Q_last = qtable[k].Q_;
    qtable[k].act_input_traffic_ = qtable[k].traffic_spill_;
    qtable[k].traffic_spill_ = 0;
    qtable[k].output_traffic_ = 0.0;
    qtable[k].end_slot_ = SIMTIME_DBL(simTime()) + qtable[k].Tq_;


    if (noMoreTimeouts) {
        // don't reschedule timeout packet
        EV << "No more timeouts.\n";
    } else {
        EV << "Schedule queue timeout for " << k << " again.\n";
        scheduleAt(qtable[k].end_slot_, qtable[k].qtp);
    }

}

void RcpSwitch::fill_in_feedback(RcpPacket *pkt, int k) {
    double request = pkt->getRcpRate();

    if (request < 0 || request > qtable[k].flow_rate_)
      pkt->setRcpRate(qtable[k].flow_rate_);
}

double RcpSwitch::running_avg(double var_sample, double var_last_avg, double gain) {
    double avg;
    if (gain < 0)
      exit(3);
    avg = gain * var_sample + ( 1 - gain) * var_last_avg;
    return avg;
}
#endif // EV1
}; // namespace
