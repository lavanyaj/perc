//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __RCP_RCPSWITCH_H
#define __RCP_RCPSWITCH_H

#include <omnetpp.h>
#include "util.h"
#include "rcp_m.h"
#include <map>

namespace rcp {


struct queueEntry_t {
    cPacketQueue* queue;
    cDatarateChannel* chan;
    QueueRequestPacket* qrp;
    QueueTimeoutPacket* qtp;

    double link_capacity_;
    double input_traffic_;
    double act_input_traffic_;
    double output_traffic_;
    double last_load_;
    double traffic_spill_;
    int num_flows_;
    double avg_rtt_;
    double this_Tq_rtt_sum_;
    double this_Tq_rtt_;
    double this_Tq_rtt_numPkts_;
    int input_traffic_rtt_;
    double rtt_moving_gain_;
    double Q_;
    double Q_last;
    double flow_rate_;
    double Tq_;
    double end_slot_;
    std::map<std::string, simsignal_t> signals;
    StatsTimeoutPacket* stp;
    std::map<std::string, double> stats;
    SimTime statsTimeoutInterval;
};

/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class RcpSwitch : public cSimpleModule
{
private:
    typedef std::map<int,int> RoutingTable; // destaddr -> gateindex
    RoutingTable rtable;
    typedef std::map<int, queueEntry_t> QueueTable;
    QueueTable qtable;
    simsignal_t dropSignal;
    int address; // RcpHost index for now
    int numLinks;

    // RCP state
    // const double linkCapacity;
    int init_numflows_;

    double init_rate_fact_;
    double rtt_gain_;

    double min_pprtt_;
    double upd_timeslot_;
    double scalar; // graph scalar; Bps -> Mbps

    double alpha_;  // Masayoshi
    double beta_;   // Masayoshi
    double gamma_;

    bool noMoreTimeouts; // set when flow gen says no more timeouts
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
    virtual void processIngress(RcpPacket *pkt, int k);
    virtual void processEgress(RcpPacket *pkt, int k);
    virtual double running_avg(double var_sample, double var_last_avg, double gain);
    virtual void fill_in_feedback(RcpPacket *pkt, int k);
    virtual void queue_timeout(int k);
    virtual void stats_timeout(int k);
    virtual int findOutGate(int destAddr);
    virtual void requestQueueMessage(int k);
};

}; // namespace

#endif
