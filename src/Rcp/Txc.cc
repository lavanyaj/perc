//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "Txc.h"
#include "rcp_m.h"

namespace rcp {

Define_Module(Txc);

void Txc::initialize()
{
    if (par("sendInitialMessage").boolValue())
    {
        RcpPacket *msg1 = new RcpPacket("tictocMsg1");
        msg1->setByteLength(1000);
        send(msg1, "io$o");
        RcpPacket *msg2 = new RcpPacket("tictocMsg2");
        msg2->setByteLength(1000);
        send(msg2, "io$o");
        RcpPacket *msg3 = new RcpPacket("tictocMsg3");
        msg3->setByteLength(1000);
        send(msg3, "io$o");
        RcpPacket *msg4 = new RcpPacket("tictocMsg3");
        msg4->setByteLength(1000);
        send(msg4, "io$o");
    }
}

void Txc::handleMessage(cMessage *msg)
{
    // just send back the message we received
    send(msg, "io$o");
}

}; // namespace

