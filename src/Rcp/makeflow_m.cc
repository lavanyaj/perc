//
// Generated file, do not edit! Created by nedtool 4.6 from Rcp/makeflow.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "makeflow_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(MakeFlowMsg);

MakeFlowMsg::MakeFlowMsg(const char *name, int kind) : ::cMessage(name,kind)
{
    this->source_var = 0;
    this->destination_var = 0;
    this->flowId_var = 0;
    this->duration_var = 0;
    this->sendTime_var = 0;
    this->rate_var = 0;
    this->start_var = 0;
    this->end_var = 0;
}

MakeFlowMsg::MakeFlowMsg(const MakeFlowMsg& other) : ::cMessage(other)
{
    copy(other);
}

MakeFlowMsg::~MakeFlowMsg()
{
}

MakeFlowMsg& MakeFlowMsg::operator=(const MakeFlowMsg& other)
{
    if (this==&other) return *this;
    ::cMessage::operator=(other);
    copy(other);
    return *this;
}

void MakeFlowMsg::copy(const MakeFlowMsg& other)
{
    this->source_var = other.source_var;
    this->destination_var = other.destination_var;
    this->flowId_var = other.flowId_var;
    this->duration_var = other.duration_var;
    this->sendTime_var = other.sendTime_var;
    this->rate_var = other.rate_var;
    this->start_var = other.start_var;
    this->end_var = other.end_var;
}

void MakeFlowMsg::parsimPack(cCommBuffer *b)
{
    ::cMessage::parsimPack(b);
    doPacking(b,this->source_var);
    doPacking(b,this->destination_var);
    doPacking(b,this->flowId_var);
    doPacking(b,this->duration_var);
    doPacking(b,this->sendTime_var);
    doPacking(b,this->rate_var);
    doPacking(b,this->start_var);
    doPacking(b,this->end_var);
}

void MakeFlowMsg::parsimUnpack(cCommBuffer *b)
{
    ::cMessage::parsimUnpack(b);
    doUnpacking(b,this->source_var);
    doUnpacking(b,this->destination_var);
    doUnpacking(b,this->flowId_var);
    doUnpacking(b,this->duration_var);
    doUnpacking(b,this->sendTime_var);
    doUnpacking(b,this->rate_var);
    doUnpacking(b,this->start_var);
    doUnpacking(b,this->end_var);
}

int MakeFlowMsg::getSource() const
{
    return source_var;
}

void MakeFlowMsg::setSource(int source)
{
    this->source_var = source;
}

int MakeFlowMsg::getDestination() const
{
    return destination_var;
}

void MakeFlowMsg::setDestination(int destination)
{
    this->destination_var = destination;
}

int MakeFlowMsg::getFlowId() const
{
    return flowId_var;
}

void MakeFlowMsg::setFlowId(int flowId)
{
    this->flowId_var = flowId;
}

int MakeFlowMsg::getDuration() const
{
    return duration_var;
}

void MakeFlowMsg::setDuration(int duration)
{
    this->duration_var = duration;
}

double MakeFlowMsg::getSendTime() const
{
    return sendTime_var;
}

void MakeFlowMsg::setSendTime(double sendTime)
{
    this->sendTime_var = sendTime;
}

double MakeFlowMsg::getRate() const
{
    return rate_var;
}

void MakeFlowMsg::setRate(double rate)
{
    this->rate_var = rate;
}

bool MakeFlowMsg::getStart() const
{
    return start_var;
}

void MakeFlowMsg::setStart(bool start)
{
    this->start_var = start;
}

bool MakeFlowMsg::getEnd() const
{
    return end_var;
}

void MakeFlowMsg::setEnd(bool end)
{
    this->end_var = end;
}

class MakeFlowMsgDescriptor : public cClassDescriptor
{
  public:
    MakeFlowMsgDescriptor();
    virtual ~MakeFlowMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MakeFlowMsgDescriptor);

MakeFlowMsgDescriptor::MakeFlowMsgDescriptor() : cClassDescriptor("MakeFlowMsg", "cMessage")
{
}

MakeFlowMsgDescriptor::~MakeFlowMsgDescriptor()
{
}

bool MakeFlowMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<MakeFlowMsg *>(obj)!=NULL;
}

const char *MakeFlowMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MakeFlowMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 8+basedesc->getFieldCount(object) : 8;
}

unsigned int MakeFlowMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<8) ? fieldTypeFlags[field] : 0;
}

const char *MakeFlowMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "source",
        "destination",
        "flowId",
        "duration",
        "sendTime",
        "rate",
        "start",
        "end",
    };
    return (field>=0 && field<8) ? fieldNames[field] : NULL;
}

int MakeFlowMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='s' && strcmp(fieldName, "source")==0) return base+0;
    if (fieldName[0]=='d' && strcmp(fieldName, "destination")==0) return base+1;
    if (fieldName[0]=='f' && strcmp(fieldName, "flowId")==0) return base+2;
    if (fieldName[0]=='d' && strcmp(fieldName, "duration")==0) return base+3;
    if (fieldName[0]=='s' && strcmp(fieldName, "sendTime")==0) return base+4;
    if (fieldName[0]=='r' && strcmp(fieldName, "rate")==0) return base+5;
    if (fieldName[0]=='s' && strcmp(fieldName, "start")==0) return base+6;
    if (fieldName[0]=='e' && strcmp(fieldName, "end")==0) return base+7;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MakeFlowMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "int",
        "double",
        "double",
        "bool",
        "bool",
    };
    return (field>=0 && field<8) ? fieldTypeStrings[field] : NULL;
}

const char *MakeFlowMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MakeFlowMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    MakeFlowMsg *pp = (MakeFlowMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MakeFlowMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    MakeFlowMsg *pp = (MakeFlowMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getSource());
        case 1: return long2string(pp->getDestination());
        case 2: return long2string(pp->getFlowId());
        case 3: return long2string(pp->getDuration());
        case 4: return double2string(pp->getSendTime());
        case 5: return double2string(pp->getRate());
        case 6: return bool2string(pp->getStart());
        case 7: return bool2string(pp->getEnd());
        default: return "";
    }
}

bool MakeFlowMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    MakeFlowMsg *pp = (MakeFlowMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setSource(string2long(value)); return true;
        case 1: pp->setDestination(string2long(value)); return true;
        case 2: pp->setFlowId(string2long(value)); return true;
        case 3: pp->setDuration(string2long(value)); return true;
        case 4: pp->setSendTime(string2double(value)); return true;
        case 5: pp->setRate(string2double(value)); return true;
        case 6: pp->setStart(string2bool(value)); return true;
        case 7: pp->setEnd(string2bool(value)); return true;
        default: return false;
    }
}

const char *MakeFlowMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *MakeFlowMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    MakeFlowMsg *pp = (MakeFlowMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


