//
// Generated file, do not edit! Created by nedtool 4.6 from Rcp/rcp.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "rcp_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(RcpPacket);

RcpPacket::RcpPacket(const char *name, int kind) : ::cPacket(name,kind)
{
    this->source_var = 0;
    this->destination_var = 0;
    this->hopCount_var = 0;
    this->pktType_var = 0;
    this->ts_var = 0;
    this->rtt_var = 0;
    this->rcpRate_var = 0;
    this->flowId_var = 0;
    this->seqNo_var = 0;
    this->numPktsRecv_var = 0;
}

RcpPacket::RcpPacket(const RcpPacket& other) : ::cPacket(other)
{
    copy(other);
}

RcpPacket::~RcpPacket()
{
}

RcpPacket& RcpPacket::operator=(const RcpPacket& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void RcpPacket::copy(const RcpPacket& other)
{
    this->source_var = other.source_var;
    this->destination_var = other.destination_var;
    this->hopCount_var = other.hopCount_var;
    this->pktType_var = other.pktType_var;
    this->ts_var = other.ts_var;
    this->rtt_var = other.rtt_var;
    this->rcpRate_var = other.rcpRate_var;
    this->flowId_var = other.flowId_var;
    this->seqNo_var = other.seqNo_var;
    this->numPktsRecv_var = other.numPktsRecv_var;
}

void RcpPacket::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->source_var);
    doPacking(b,this->destination_var);
    doPacking(b,this->hopCount_var);
    doPacking(b,this->pktType_var);
    doPacking(b,this->ts_var);
    doPacking(b,this->rtt_var);
    doPacking(b,this->rcpRate_var);
    doPacking(b,this->flowId_var);
    doPacking(b,this->seqNo_var);
    doPacking(b,this->numPktsRecv_var);
}

void RcpPacket::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->source_var);
    doUnpacking(b,this->destination_var);
    doUnpacking(b,this->hopCount_var);
    doUnpacking(b,this->pktType_var);
    doUnpacking(b,this->ts_var);
    doUnpacking(b,this->rtt_var);
    doUnpacking(b,this->rcpRate_var);
    doUnpacking(b,this->flowId_var);
    doUnpacking(b,this->seqNo_var);
    doUnpacking(b,this->numPktsRecv_var);
}

int RcpPacket::getSource() const
{
    return source_var;
}

void RcpPacket::setSource(int source)
{
    this->source_var = source;
}

int RcpPacket::getDestination() const
{
    return destination_var;
}

void RcpPacket::setDestination(int destination)
{
    this->destination_var = destination;
}

int RcpPacket::getHopCount() const
{
    return hopCount_var;
}

void RcpPacket::setHopCount(int hopCount)
{
    this->hopCount_var = hopCount;
}

int RcpPacket::getPktType() const
{
    return pktType_var;
}

void RcpPacket::setPktType(int pktType)
{
    this->pktType_var = pktType;
}

double RcpPacket::getTs() const
{
    return ts_var;
}

void RcpPacket::setTs(double ts)
{
    this->ts_var = ts;
}

double RcpPacket::getRtt() const
{
    return rtt_var;
}

void RcpPacket::setRtt(double rtt)
{
    this->rtt_var = rtt;
}

double RcpPacket::getRcpRate() const
{
    return rcpRate_var;
}

void RcpPacket::setRcpRate(double rcpRate)
{
    this->rcpRate_var = rcpRate;
}

int RcpPacket::getFlowId() const
{
    return flowId_var;
}

void RcpPacket::setFlowId(int flowId)
{
    this->flowId_var = flowId;
}

int RcpPacket::getSeqNo() const
{
    return seqNo_var;
}

void RcpPacket::setSeqNo(int seqNo)
{
    this->seqNo_var = seqNo;
}

int RcpPacket::getNumPktsRecv() const
{
    return numPktsRecv_var;
}

void RcpPacket::setNumPktsRecv(int numPktsRecv)
{
    this->numPktsRecv_var = numPktsRecv;
}

class RcpPacketDescriptor : public cClassDescriptor
{
  public:
    RcpPacketDescriptor();
    virtual ~RcpPacketDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(RcpPacketDescriptor);

RcpPacketDescriptor::RcpPacketDescriptor() : cClassDescriptor("RcpPacket", "cPacket")
{
}

RcpPacketDescriptor::~RcpPacketDescriptor()
{
}

bool RcpPacketDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<RcpPacket *>(obj)!=NULL;
}

const char *RcpPacketDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int RcpPacketDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 10+basedesc->getFieldCount(object) : 10;
}

unsigned int RcpPacketDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<10) ? fieldTypeFlags[field] : 0;
}

const char *RcpPacketDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "source",
        "destination",
        "hopCount",
        "pktType",
        "ts",
        "rtt",
        "rcpRate",
        "flowId",
        "seqNo",
        "numPktsRecv",
    };
    return (field>=0 && field<10) ? fieldNames[field] : NULL;
}

int RcpPacketDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='s' && strcmp(fieldName, "source")==0) return base+0;
    if (fieldName[0]=='d' && strcmp(fieldName, "destination")==0) return base+1;
    if (fieldName[0]=='h' && strcmp(fieldName, "hopCount")==0) return base+2;
    if (fieldName[0]=='p' && strcmp(fieldName, "pktType")==0) return base+3;
    if (fieldName[0]=='t' && strcmp(fieldName, "ts")==0) return base+4;
    if (fieldName[0]=='r' && strcmp(fieldName, "rtt")==0) return base+5;
    if (fieldName[0]=='r' && strcmp(fieldName, "rcpRate")==0) return base+6;
    if (fieldName[0]=='f' && strcmp(fieldName, "flowId")==0) return base+7;
    if (fieldName[0]=='s' && strcmp(fieldName, "seqNo")==0) return base+8;
    if (fieldName[0]=='n' && strcmp(fieldName, "numPktsRecv")==0) return base+9;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *RcpPacketDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "int",
        "double",
        "double",
        "double",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<10) ? fieldTypeStrings[field] : NULL;
}

const char *RcpPacketDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int RcpPacketDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    RcpPacket *pp = (RcpPacket *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string RcpPacketDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    RcpPacket *pp = (RcpPacket *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getSource());
        case 1: return long2string(pp->getDestination());
        case 2: return long2string(pp->getHopCount());
        case 3: return long2string(pp->getPktType());
        case 4: return double2string(pp->getTs());
        case 5: return double2string(pp->getRtt());
        case 6: return double2string(pp->getRcpRate());
        case 7: return long2string(pp->getFlowId());
        case 8: return long2string(pp->getSeqNo());
        case 9: return long2string(pp->getNumPktsRecv());
        default: return "";
    }
}

bool RcpPacketDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    RcpPacket *pp = (RcpPacket *)object; (void)pp;
    switch (field) {
        case 0: pp->setSource(string2long(value)); return true;
        case 1: pp->setDestination(string2long(value)); return true;
        case 2: pp->setHopCount(string2long(value)); return true;
        case 3: pp->setPktType(string2long(value)); return true;
        case 4: pp->setTs(string2double(value)); return true;
        case 5: pp->setRtt(string2double(value)); return true;
        case 6: pp->setRcpRate(string2double(value)); return true;
        case 7: pp->setFlowId(string2long(value)); return true;
        case 8: pp->setSeqNo(string2long(value)); return true;
        case 9: pp->setNumPktsRecv(string2long(value)); return true;
        default: return false;
    }
}

const char *RcpPacketDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *RcpPacketDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    RcpPacket *pp = (RcpPacket *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(StatsTimeoutPacket);

StatsTimeoutPacket::StatsTimeoutPacket(const char *name, int kind) : ::cPacket(name,kind)
{
    this->queueIndex_var = 0;
}

StatsTimeoutPacket::StatsTimeoutPacket(const StatsTimeoutPacket& other) : ::cPacket(other)
{
    copy(other);
}

StatsTimeoutPacket::~StatsTimeoutPacket()
{
}

StatsTimeoutPacket& StatsTimeoutPacket::operator=(const StatsTimeoutPacket& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void StatsTimeoutPacket::copy(const StatsTimeoutPacket& other)
{
    this->queueIndex_var = other.queueIndex_var;
}

void StatsTimeoutPacket::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->queueIndex_var);
}

void StatsTimeoutPacket::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->queueIndex_var);
}

int StatsTimeoutPacket::getQueueIndex() const
{
    return queueIndex_var;
}

void StatsTimeoutPacket::setQueueIndex(int queueIndex)
{
    this->queueIndex_var = queueIndex;
}

class StatsTimeoutPacketDescriptor : public cClassDescriptor
{
  public:
    StatsTimeoutPacketDescriptor();
    virtual ~StatsTimeoutPacketDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(StatsTimeoutPacketDescriptor);

StatsTimeoutPacketDescriptor::StatsTimeoutPacketDescriptor() : cClassDescriptor("StatsTimeoutPacket", "cPacket")
{
}

StatsTimeoutPacketDescriptor::~StatsTimeoutPacketDescriptor()
{
}

bool StatsTimeoutPacketDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<StatsTimeoutPacket *>(obj)!=NULL;
}

const char *StatsTimeoutPacketDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int StatsTimeoutPacketDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int StatsTimeoutPacketDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *StatsTimeoutPacketDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "queueIndex",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int StatsTimeoutPacketDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='q' && strcmp(fieldName, "queueIndex")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *StatsTimeoutPacketDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *StatsTimeoutPacketDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int StatsTimeoutPacketDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    StatsTimeoutPacket *pp = (StatsTimeoutPacket *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string StatsTimeoutPacketDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    StatsTimeoutPacket *pp = (StatsTimeoutPacket *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getQueueIndex());
        default: return "";
    }
}

bool StatsTimeoutPacketDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    StatsTimeoutPacket *pp = (StatsTimeoutPacket *)object; (void)pp;
    switch (field) {
        case 0: pp->setQueueIndex(string2long(value)); return true;
        default: return false;
    }
}

const char *StatsTimeoutPacketDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *StatsTimeoutPacketDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    StatsTimeoutPacket *pp = (StatsTimeoutPacket *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(QueueRequestPacket);

QueueRequestPacket::QueueRequestPacket(const char *name, int kind) : ::cPacket(name,kind)
{
    this->queueIndex_var = 0;
}

QueueRequestPacket::QueueRequestPacket(const QueueRequestPacket& other) : ::cPacket(other)
{
    copy(other);
}

QueueRequestPacket::~QueueRequestPacket()
{
}

QueueRequestPacket& QueueRequestPacket::operator=(const QueueRequestPacket& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void QueueRequestPacket::copy(const QueueRequestPacket& other)
{
    this->queueIndex_var = other.queueIndex_var;
}

void QueueRequestPacket::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->queueIndex_var);
}

void QueueRequestPacket::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->queueIndex_var);
}

int QueueRequestPacket::getQueueIndex() const
{
    return queueIndex_var;
}

void QueueRequestPacket::setQueueIndex(int queueIndex)
{
    this->queueIndex_var = queueIndex;
}

class QueueRequestPacketDescriptor : public cClassDescriptor
{
  public:
    QueueRequestPacketDescriptor();
    virtual ~QueueRequestPacketDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(QueueRequestPacketDescriptor);

QueueRequestPacketDescriptor::QueueRequestPacketDescriptor() : cClassDescriptor("QueueRequestPacket", "cPacket")
{
}

QueueRequestPacketDescriptor::~QueueRequestPacketDescriptor()
{
}

bool QueueRequestPacketDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<QueueRequestPacket *>(obj)!=NULL;
}

const char *QueueRequestPacketDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int QueueRequestPacketDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int QueueRequestPacketDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *QueueRequestPacketDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "queueIndex",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int QueueRequestPacketDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='q' && strcmp(fieldName, "queueIndex")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *QueueRequestPacketDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *QueueRequestPacketDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int QueueRequestPacketDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    QueueRequestPacket *pp = (QueueRequestPacket *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string QueueRequestPacketDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    QueueRequestPacket *pp = (QueueRequestPacket *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getQueueIndex());
        default: return "";
    }
}

bool QueueRequestPacketDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    QueueRequestPacket *pp = (QueueRequestPacket *)object; (void)pp;
    switch (field) {
        case 0: pp->setQueueIndex(string2long(value)); return true;
        default: return false;
    }
}

const char *QueueRequestPacketDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *QueueRequestPacketDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    QueueRequestPacket *pp = (QueueRequestPacket *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(QueueTimeoutPacket);

QueueTimeoutPacket::QueueTimeoutPacket(const char *name, int kind) : ::cPacket(name,kind)
{
    this->queueIndex_var = 0;
}

QueueTimeoutPacket::QueueTimeoutPacket(const QueueTimeoutPacket& other) : ::cPacket(other)
{
    copy(other);
}

QueueTimeoutPacket::~QueueTimeoutPacket()
{
}

QueueTimeoutPacket& QueueTimeoutPacket::operator=(const QueueTimeoutPacket& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void QueueTimeoutPacket::copy(const QueueTimeoutPacket& other)
{
    this->queueIndex_var = other.queueIndex_var;
}

void QueueTimeoutPacket::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->queueIndex_var);
}

void QueueTimeoutPacket::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->queueIndex_var);
}

int QueueTimeoutPacket::getQueueIndex() const
{
    return queueIndex_var;
}

void QueueTimeoutPacket::setQueueIndex(int queueIndex)
{
    this->queueIndex_var = queueIndex;
}

class QueueTimeoutPacketDescriptor : public cClassDescriptor
{
  public:
    QueueTimeoutPacketDescriptor();
    virtual ~QueueTimeoutPacketDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(QueueTimeoutPacketDescriptor);

QueueTimeoutPacketDescriptor::QueueTimeoutPacketDescriptor() : cClassDescriptor("QueueTimeoutPacket", "cPacket")
{
}

QueueTimeoutPacketDescriptor::~QueueTimeoutPacketDescriptor()
{
}

bool QueueTimeoutPacketDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<QueueTimeoutPacket *>(obj)!=NULL;
}

const char *QueueTimeoutPacketDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int QueueTimeoutPacketDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int QueueTimeoutPacketDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *QueueTimeoutPacketDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "queueIndex",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int QueueTimeoutPacketDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='q' && strcmp(fieldName, "queueIndex")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *QueueTimeoutPacketDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *QueueTimeoutPacketDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int QueueTimeoutPacketDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    QueueTimeoutPacket *pp = (QueueTimeoutPacket *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string QueueTimeoutPacketDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    QueueTimeoutPacket *pp = (QueueTimeoutPacket *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getQueueIndex());
        default: return "";
    }
}

bool QueueTimeoutPacketDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    QueueTimeoutPacket *pp = (QueueTimeoutPacket *)object; (void)pp;
    switch (field) {
        case 0: pp->setQueueIndex(string2long(value)); return true;
        default: return false;
    }
}

const char *QueueTimeoutPacketDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *QueueTimeoutPacketDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    QueueTimeoutPacket *pp = (QueueTimeoutPacket *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


