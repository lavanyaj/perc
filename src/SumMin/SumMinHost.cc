#include "SumMinHost.h"
#include <assert.h>

namespace rcp {
Define_Module(SumMinHost);
#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 if (false) std::cout

GenericControlPacket* SumMinHost::newProtocolSpecificControlPacket(const char* pktname, int flowId) {
    SumMinPacket* pkt = new SumMinPacket(pktname);
    GenericControlHostSessionState &session = pIActiveSessions[flowId];
    SumMinHostSessionState &sMSession = sMActiveSessions[flowId];
    if (sMSession.stampedRate == -1) {
            // first packet, haven't received any incoming stamped rates or marked bit yet
            pkt->setStampedRate(session.demand);
            if (session.demand < INFINITE_RATE)
                pkt->setMarkedBit(true);
            else pkt->setMarkedBit(false);
        } else {
            pkt->setStampedRate(sMSession.stampedRate);
            pkt->setMarkedBit(sMSession.markedBit);
        }

    return pkt;
}

void SumMinHost::protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    SumMinHostSessionState session(flowId);
    session.linkState.recordThenCalculate = par("recordThenCalculate");
    sMActiveSessions[flowId] = session;
}


void SumMinHost::sourceReceivePacket(const GenericControlPacket *msg) {
    const SumMinPacket *pkt = check_and_cast<SumMinPacket *>(msg);
    emitSignal(pkt->getFlowId(), "StampedRate", pkt->getStampedRate());
    //emit(sourceReceivePacketSignal, pkt->getStampedRate());
    EV2 << "At time "<< simTime() << ", emitting sourceReceivePacketSignal: "\
            << pkt->getStampedRate() << " marked(" << pkt->getMarkedBit() << ")\n";

    GenericControlHostSessionState &session = pIActiveSessions[pkt->getFlowId()];
    SumMinHostSessionState &sMSession = sMActiveSessions[pkt->getFlowId()];
    // incoming stamped rate, maybe add one for outgoing stamped rate

   // NOTE THIS IS THE VERSION WHERE WE ASSUME DEMANDS ARE INFINITE
    session.numRecvd++;
    sMSession.markedBit = false;
     if (pkt->getMarkedBit() == false) {
        sMSession.stampedRate = session.demand;

        EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                       << ", sets session.rate to session.demand "<< session.demand <<\
                       " because incoming packet was not marked.\n";
    } else {
        // Packet passed at least one link whose advertised rate
        // was equal to the packets's current stamped rate
        // so obey this rate.
      sMSession.stampedRate = pkt->getStampedRate();
      sMSession.markedBit = false; // not restricted by host

      EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                             << ", sets packet.rate "<< pkt->getStampedRate() <<\
                             " and packet.markedBit to false " <<\
                             " because incoming packet was marked.\n";
    }


    updateTransmissionRate(msg);
}

void SumMinHost::sourceReceivePacketGeneral(const GenericControlPacket *msg) {
    const SumMinPacket *pkt = check_and_cast<SumMinPacket *>(msg);
    emitSignal(pkt->getFlowId(), "StampedRate", pkt->getStampedRate());
    //emit(sourceReceivePacketSignal, pkt->getStampedRate());
    EV2 << "At time "<< simTime() << ", emitting sourceReceivePacketSignal: "\
            << pkt->getStampedRate() << " marked(" << pkt->getMarkedBit() << ")\n";

    GenericControlHostSessionState &session = pIActiveSessions[pkt->getFlowId()];
    SumMinHostSessionState &sMSession = sMActiveSessions[pkt->getFlowId()];
    // incoming stamped rate, maybe add one for outgoing stamped rate


    session.numRecvd++;

    if (rateGreaterThan(pkt->getStampedRate(), session.demand)) { // must be that demand had decreased
           sMSession.stampedRate = session.demand;
           sMSession.markedBit = true;
           EV2 << "Packet stamped rate is more than session demand, demand must have decreased.\n";

           EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                          << ", sets packet.rate to "<< session.demand\
                          << " and marks packet "
                          << " because incoming packet rate > demand.\n";

       } else if (pkt->getMarkedBit() == false) {
           EV2 << "Packet return unmarked.\n";
           sMSession.stampedRate = session.demand;
           if (session.demand < INFINITE_RATE) {
               sMSession.markedBit = true;

               EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                                      << ", sets packet.rate to "<< session.demand\
                                      << " and marks packet "
                                      << " because incoming packet unmarked, rate <= demand < INF.\n";
           }
           else {
               sMSession.markedBit = false;

               EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                                                  << ", sets packet.rate to "<< session.demand\
                                                  << " and marks packet "
                                                  << " because incoming packet unmarked, rate <= demand = INF.\n";
           }
       } else {
           // Packet passed at least one link whose advertised rate
           // was equal to the packets's current stamped rate
           // so obey this rate.
           // Or packet's current stamped rate is just host's demand
           // and it's marked by host itself- possible that
           // all links have higher advertised rates.
         sMSession.stampedRate = pkt->getStampedRate();
         sMSession.markedBit = false;

         EV3 << "At time " << simTime() <<", host h for flow " << pkt->getFlowId()\
                                            << ", sets packet.rate to "<< pkt->getStampedRate()\
                                            << " and unmarks packet "
                                            << " because incoming packet marked, rate <= demand.\n";
       }

    // flows with infinite demands.
    // When outgoing packet had a rate that's > fair share at switch.
    assert((sMSession.stampedRate == pkt->getStampedRate() and !sMSession.markedBit)\
            or (sMSession.stampedRate == INFINITE_RATE and !sMSession.markedBit)\
            or (sMSession.stampedRate == 0 and sMSession.markedBit)); // not limited by host

    // When outgoing packet had a rate that's <= fair share at switch.
    // then incoming packet was unmarked, since demand's infinity
    // markedBit is false as always. Stamped Rate is infinity.
    // switch updates only if rate is more than fair share.

    updateTransmissionRate(msg);
}

void SumMinHost::updateTransmissionRate(const GenericControlPacket *msg) {
    const SumMinPacket *pkt = check_and_cast<SumMinPacket *>(msg);
    GenericControlHostSessionState& session = pIActiveSessions.at(pkt->getFlowId());
    SumMinHostSessionState& sMSession = sMActiveSessions.at(pkt->getFlowId());

    // UPDATE ACTUAL TRANSMISSION RATE

    struct MarkedState {
        bool markedBit;
        double stampedRate;
        MarkedState(bool markedBit, double stampedRate) : markedBit(markedBit), stampedRate(stampedRate) {}
    } newIncoming(pkt->getMarkedBit(), pkt->getStampedRate());
    EV2 << "flow # " << session.flowId << ", incoming pkt # " << session.numRecvd << "\n";
    // How do data rates follow stamped rates? Some subset, in order, slightly delayed. I think.

    if (!par("conservative")) {
        EV2 << "Being aggressive.\n";
        if (newIncoming.markedBit) {
            if (! rateEqual(newIncoming.stampedRate, session.dataRate)) {
                EV2 << "incoming packet was marked, ";
                EV2 << "incoming rate "<< newIncoming.stampedRate\
                        <<" is "\
                        << (rateGreaterThan(newIncoming.stampedRate, session.dataRate) ? "more" : "less")\
                        << " than actual rate " << session.dataRate << ", ";
                EV2 << "schedule rate change now  "  << session.numRecvd << ".\n";
                session.sendAtPktNum = session.numRecvd;
                session.sendDataRate = newIncoming.stampedRate;
            }
        }
    }

    if (session.sendAtPktNum == session.numRecvd) {
        assert(session.sendDataRate >= 0);
        sendFlowMsg(session.flowId, session.sendDataRate);
        //session.sendAtPktNum = -1;
    }

}
bool SumMinHost::destinationReceivePacket(const GenericControlPacket *msg) {
    const SumMinPacket *pkt = check_and_cast<SumMinPacket *>(msg);
    SumMinHostSessionState &session = sMActiveSessions[pkt->getFlowId()];
    session.count += 1;
    if (session.count == K) {
        session.stampedRate = pkt->getStampedRate();
        session.markedBit = pkt->getMarkedBit();
        session.count = 0;
        return true;
    } else {
        return false;
    }
}

void SumMinHost::addProtocolSpecificActiveSession(const GenericControlPacket *pkt) {
    int flowId = pkt->getFlowId();
    SumMinHostSessionState session(flowId);
    session.linkState.recordThenCalculate = par("recordThenCalculate");
    sMActiveSessions[flowId] = session;
}; // at destination
void SumMinHost::removeProtocolSpecificActiveSession(int flowId) {\
    sMActiveSessions.erase(flowId);
}; // on FIN

#endif
#endif
}; // namespace
