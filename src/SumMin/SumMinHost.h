//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SUMMIN_HOST_H
#define __SUMMIN_HOST_H

#include <omnetpp.h>
#include "util.h"
#include "GenericControlHost.h"
#include "SumMinSwitch.h"
#include "summin_m.h"
#include <map>
#include <utility>

namespace rcp {

/**
 * Implements the Txc simple module. See the NED file for more information.
 */


class SumMinHostSessionState {
public:
    int flowId;
    bool markedBit;
    double stampedRate;
    int count;
    SumMinLinkState linkState;
    SumMinHostSessionState(int fid) : flowId(fid), markedBit(false), stampedRate(-1), count(0), linkState(std::to_string(fid)) {}
    SumMinHostSessionState(): flowId(-1), markedBit(false), stampedRate(-1), count(0), linkState(std::to_string(-1)) {}
};

/*
 *  To have flows with finite demands..
 *  For each session, switch.linkCapacity, switch.advertisedRate, switch.numForwardSessions,..
 *  so maybe to host session state, add a SumMinLinkState(idStr) and linkAction every packet
 *  going out.
 *  When flow's ending, change switch linkCapacity to 0..
 */


class SumMinHost : public GenericControlHost
{

  private:
     // Charny state
    typedef std::map<int, SumMinHostSessionState> SessionTable;
    SessionTable sMActiveSessions;

  protected:
    virtual GenericControlPacket *newProtocolSpecificControlPacket(const char* pktname, int flowId);
    virtual void sourceReceivePacket(const GenericControlPacket *pkt); // assumes demand is always INFINITE
    virtual void sourceReceivePacketGeneral(const GenericControlPacket *pkt); // demand could be FINITE, changing
    virtual bool destinationReceivePacket(const GenericControlPacket *pkt);
    virtual void updateTransmissionRate(const GenericControlPacket *pkt);

    virtual void protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg); // at source
    virtual void addProtocolSpecificActiveSession(const GenericControlPacket *pkt); // at destination
    virtual void removeProtocolSpecificActiveSession(int flowId); // on FIN

};


}; // namespace

#endif
