#include "SumMinSwitch.h"

#include <assert.h>

namespace rcp {

Define_Module(SumMinSwitch);

#ifndef EV3
#define EV3 EV << "EV3: "
#ifndef EV2
#define EV2 EV
#ifndef EV1
#define EV1 if (false) std::cout

SumMinLinkState::SumMinLinkState(std::string idStr):
        linkCapacity(-1), advertisedRate(-1), numForwardSessions(0),\
                numFeedbackSessions(0), idStr(idStr) {


            WATCH(advertisedRate);
            WATCH(numForwardSessions);
            WATCH(numFeedbackSessions);

        }

SumMinLinkState::~SumMinLinkState() {
    activeSessions.clear();
}


SumMinSwitch::SumMinSwitch() {
}

SumMinSwitch::~SumMinSwitch() {



}

void SumMinSwitch::finish() {
    EV << "Clearing Link State for SumMin.\n";
    for (auto ls : cltable) {
        delete ls.second;
        ls.second = NULL;
    }
    cltable.clear();
    GenericSwitch::finish();
}
void SumMinSwitch::initialize()
{
    GenericSwitch::initialize();
   for (auto linkState: ltable) {
        cltable[linkState.first] = new SumMinLinkState(linkState.second->getId());
        EV << "Added new SumMin link state for link " << linkState.first\
                << ".\n";
        cltable[linkState.first]->linkCapacity = linkState.second->linkCapacity;
        cltable[linkState.first]->advertisedRate = linkState.second->linkCapacity;
        cltable[linkState.first]->eagerSwitchMode = linkState.second->eagerSwitchMode;
        cltable[linkState.first]->recordThenCalculate = par("recordThenCalculate");
    }
}

cMessage* SumMinSwitch::protocolSpecificProcessEgress(int outGateIndex, cMessage* msg) {
    EV << "Calling cltable.at("<< outGateIndex << ")->processEgress\n";
    SumMinPacket * pkt = check_and_cast<SumMinPacket *>(msg);
    return cltable.at(outGateIndex)->processEgress(pkt);

}

// actually ingate index etc.
cMessage* SumMinSwitch::protocolSpecificProcessIngress(int outGateIndex, cMessage* msg) {
    return cltable[outGateIndex]->processIngress(check_and_cast<SumMinPacket *>(msg));

}

SumMinPacket *SumMinLinkState::processIngress(SumMinPacket *pkt)
{
     return pkt;
     //->dup();
}

SumMinPacket *SumMinLinkState::processEgress(SumMinPacket *pkt)
{

    if (pkt->getPktType() == CHARNY_HELLO || pkt->getPktType() == CHARNY_FIN) {
        EV << "Link action HELLO/FIN\n";
        linkAction(pkt);
    } else {
        EV << "Link action: Unknown packet type.\n";
    }
    return pkt;
    //->dup();
}




void SumMinLinkState::calculateAdvRate() {
 double oldRate = advertisedRate;
 advertisedRate = calculateRate();
 EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                     " for flows"\
                     ", computes fair_share as " << advertisedRate\
                     << " because previous fair_share was " << oldRate << "\n";

 EV2 << "Switch " << this->getId() << " updated rate from "\
         << oldRate << " -> " << advertisedRate << ".\n";
 }

double SumMinLinkState::getDiff(double fairShare) {
    double total = 0;
      for (const auto& session : activeSessions) {
          double min = session.second.recordedRate;
          if (fairShare <= min) min = fairShare;
          total += min;
      }
      double diff = linkCapacity - total;
      return floor(diff);
}

double SumMinLinkState::calculateRate() {
    //return linkCapacity;

    double totalDemand = 0;
    double maxDemand = -1;
    bool greedy = false;
    for (const auto& session : activeSessions) {
        if (session.second.recordedRate == INFINITE_RATE)
            greedy = true;
      totalDemand += session.second.recordedRate;
      if (maxDemand == -1 or maxDemand < session.second.recordedRate)
          maxDemand = session.second.recordedRate;
    }
    if (not greedy and totalDemand <= linkCapacity) {
        EV2 << "max Demand = fair share = " << maxDemand << ".\n";
        return maxDemand;
    }

    // total demand exceeds capacity
    double fairShare = 0;
    double bestFairShare = 0;
    double diff = linkCapacity;
    double bestDiff = -1;

    // When fair share is too low .. diff >= 0
    // When fair share is too high .. diff < 0
    double upper = linkCapacity;
    double lower = 0;
    while(upper - lower > 2) {
        fairShare = floor((upper+lower)/2);
        diff = getDiff(fairShare);
        EV2 << "Fair share between " << lower << " and " << upper << ", diff: " << diff << "\n";
        if (diff < 0) upper = fairShare;
        else if (diff > 0) lower = fairShare;
        else {upper = fairShare; lower = fairShare;}
    }
    fairShare = floor((upper+lower)/2);
    diff = getDiff(fairShare);
    EV2 << "Fair share between " << lower << " and " << upper << ", diff: " << diff << "\n";

    for (fairShare = lower; fairShare <= upper; fairShare++) {
        diff = getDiff(fairShare);
        if (diff >= 0) {
          if (bestDiff == -1 or diff < bestDiff) {
             bestDiff = diff;
             bestFairShare = fairShare;
          }
        }
    }
    /*
    for (fairShare = 0; fairShare <= linkCapacity; fairShare++) {
        int total = 0;
        for (const auto& session : activeSessions) {
            double min = session.second.recordedRate;
            if (fairShare <= min) min = fairShare;
            total += min;
        }
        diff = linkCapacity - total;
        if (diff >= 0) {
            if (bestDiff == -1 or diff < bestDiff) {
                bestDiff = diff;
                bestFairShare = fairShare;
            }
        }
    }
    */
    EV3 << "Fair share " << bestFairShare << " gives diff " << bestDiff << " from link capacity ";
    EV3 << " for " << activeSessions.size() << " active sessions.\n";
    return bestFairShare;
}


// HELLO AND FIN PACKETS ON HIGH PRIO QUEUE, DATA PACKETS ACCORDING TO RATE
// DECOUPLE CONTROL AND DATA
void SumMinLinkState::linkUpdateSessionExit(const SumMinPacket *pkt) {
    if (pkt->getIsForward()) {
        numForwardSessions -= 1;
    } else {
        numFeedbackSessions -= 1;
    }
    activeSessions.erase(pkt->getFlowId());
    calculateAdvRate();
}

// now this is not just like UpdateKnownSession..
void SumMinLinkState::linkUpdateNewSession(SumMinPacket *pkt) {

    int flowId = pkt->getFlowId();
    if (pkt->getIsForward()) numForwardSessions += 1;
    else numFeedbackSessions += 1;
    double marked = false;
    SumMinSwitchSessionState session(pkt->getStampedRate(), 0, marked, flowId);
    activeSessions[flowId] = session;

    linkUpdateKnownSession(pkt);

}

void SumMinLinkState::linkUpdateKnownSession(SumMinPacket *pkt) {
    SumMinSwitchSessionState& session = activeSessions[pkt->getFlowId()];
    session.numPackets += (pkt->getIsForward()) + K * (1 - pkt->getIsForward());

    if (recordThenCalculate) {
            session.recordedRate = pkt->getStampedRate();

        EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                " for flow " << pkt->getFlowId() <<\
                ", records demand[flow] as " << session.recordedRate
                << " because that's packet.rate...\n";
    }

    EV2 << "Packet has stamped rate " << pkt->getStampedRate() << ".\n";
    calculateAdvRate(); // rate including this new session, but unmarked .. don't want to change marked set yet.

    if (rateGreaterThanOrEqual(pkt->getStampedRate(), advertisedRate)) {
        pkt->setStampedRate(advertisedRate);
        pkt->setMarkedBit(true);

        EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
         " for flow " << pkt->getFlowId() <<\
         ", stamps packet.rate as " << advertisedRate <<\
         " (fair_share) and packet.marked as true" <<\
         " because old rate was greater than fair_share.\n";
    } else {
        EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                 " for flow " << pkt->getFlowId() <<\
                 ", doesn't update packet.rate " << pkt->getStampedRate() <<\
                 " and leaves marked bit to " << pkt->getMarkedBit() <<\
                 " because it's smaller than fair_Share.\n";

    }

    if (not recordThenCalculate) {
                session.recordedRate = pkt->getStampedRate();

            EV3 << "At time " << simTime() <<", switch " << this->getId() <<\
                    " for flow " << pkt->getFlowId() <<\
                    ", records demand[flow] as " << session.recordedRate
                    << " because that's packet.rate...\n";
    }
    EV2 << "Mark session " << pkt->getFlowId() << "'s recorded rate as "\
            << session.recordedRate << ".\n";
}

void SumMinLinkState::linkAction(SumMinPacket *pkt){
    if(!pkt->getIsForward()) {
        EV2 << "No link action on feedback packet of session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        return;
    }

    if (pkt->getIsExit()) {
        EV2 << ((activeSessions.count(pkt->getFlowId()) == 1) ? "Known" : "New")\
                << " session #" << pkt->getFlowId() << " exiting, at Switch " << this->getId() << ".\n";
        linkUpdateSessionExit(pkt);
        return;
    }

    if (activeSessions.count(pkt->getFlowId()) == 0) {
        EV2 << "New session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        linkUpdateNewSession(pkt);
    } else {
        EV2 << "Known session #" << pkt->getFlowId() << " at Switch " << this->getId() << ".\n";
        linkUpdateKnownSession(pkt);
    }
}


#endif // EV1
#endif // EV2
#endif // EV3
}; // namespace
