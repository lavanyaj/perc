//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SumMin_SWITCH_H
#define __SumMin_SWITCH_H

#include <omnetpp.h>
#include "util.h"
#include "GenericSwitch.h"
#include "summin_m.h"
#include <map>

namespace rcp {


class SumMinSwitchSessionState {
public:
    double recordedRate;
    int numPackets;
    bool marked;
    int flowId;

    SumMinSwitchSessionState(double rr, int np, bool m, int fid) :
        recordedRate(rr), numPackets(np), marked(m), flowId(fid) {
    }
    SumMinSwitchSessionState() :
            recordedRate(0), numPackets(0), marked(false), flowId(-1) {
        }
};

class SumMinLinkState {
    public:
    std::string idStr;
    std::string getId() {return idStr;}
    //cMessage *dequeueMessage;

    // SumMin state
    double linkCapacity;
    typedef std::map<int, SumMinSwitchSessionState> SessionTable;
    SessionTable activeSessions;
    double advertisedRate;
    int numForwardSessions;
    int numFeedbackSessions;
    bool eagerSwitchMode;
    bool recordThenCalculate;
    SumMinLinkState(std::string idStr);
    ~SumMinLinkState();

    SumMinPacket *processIngress(SumMinPacket *pkt);
    SumMinPacket *processEgress(SumMinPacket *pkt);
    double getDiff(double fairShare);
    void calculateAdvRate();
    double calculateAdvRateAndRevert();
    double calculateRate();
    double getMaxRate();
    double getTotalRate();
    void linkUpdateSessionExit(const SumMinPacket *pkt);
    void linkUpdateNewSession(SumMinPacket *pkt);
    void linkUpdateKnownSession(SumMinPacket *pkt);
    void linkAction(SumMinPacket *pkt);
};



/**
 * Implements the Txc simple module. See the NED file for more information.
 */
class SumMinSwitch : public GenericSwitch
{

private:
    typedef std::map<int,SumMinLinkState*> SumMinLinkStateTable; // output queue # -> link state
    SumMinLinkStateTable cltable;
protected:
    void initialize();
    void finish();

    cMessage* protocolSpecificProcessIngress(int outGateIndex, cMessage* msg);
    cMessage* protocolSpecificProcessEgress(int outGateIndex, cMessage* msg);

public:
    SumMinSwitch();
    ~SumMinSwitch();
};

}; // namespace

#endif
