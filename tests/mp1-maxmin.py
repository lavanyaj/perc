import sys
import random
import matplotlib.pyplot as plt
import numpy as np
import fileinput
import re
import operator

###################### Global constants ########################
num_instances = 1
max_iterations = 10000
max_capacity = 100
################################################################

# two hop paths for now.
def get_path(src, dst):
    return [src, dst]

class MPMaxMin:
    def __init__(self, routes, c):
        
        ########################## Inputs ##############################        
        (self.num_flows, self.num_links) = routes.shape
        self.routes = routes  # incidence matrix for flows / links
        self.c = c            # link capacities
        ################################################################

        self.no_flows = np.array([f for f in range(self.num_flows) if\
                                      sum([self.routes[f,l]\
                                               for l in range(self.num_links)]) == 0])

        # max-min rates
        self.maxmin_x = self.water_filling()
        # iteration
        self.t = 0                                            
        # sending rates
        self.x = np.zeros((self.num_flows,1))        
         
        # flow to link messages
        self.flow_to_link = self.routes * max_capacity * 1.0
        # link to flow messages
        self.link_to_flow = np.zeros((self.num_links, self.num_flows))
           
        # timeseries
        self.xs = self.x
        self.errors = max(np.abs(self.x - self.maxmin_x))

        print self.no_flows
        pass

    def doWaterfilling(self):
        self.maxmin_x = self.water_filling()
        print 're-do waterfilling to get optimal rates: \n'
        print self.maxmin_x
        pass

    def add_flow(self, f, src, dst):
        path = get_path(src, dst)
        self.add_flow_path(f, [src, dst])
        pass
    
    def add_flow_path(self, f, path):
        self.x[f] = 0
        for i in path:
            self.routes[f, i] = 1
            self.flow_to_link[f,i] = max_capacity * 1.0
            self.link_to_flow[i,f] = 0
            pass

        self.no_flows = \
            np.array([f for f in range(self.num_flows) if\
                          sum([self.routes[f,l]\
                                   for l in range(self.num_links)]) == 0])
        pass
    
    def remove_flow(self, f):
        self.x[f] = 0
        for i in range(self.num_links):
            self.routes[f, i] = 0
            self.flow_to_link[f,i] = 0
            self.link_to_flow[i,f] = 0
            pass

        self.no_flows = \
            np.array([f for f in range(self.num_flows) if\
                          sum([self.routes[f,l]\
                                   for l in range(self.num_links)]) == 0])
        return

    def water_filling(self):
        weights = np.ones((self.num_flows, 1)) # 0 for flow not in system
        x = np.zeros((self.num_flows,1))
        rem_flows = np.array([f for f in range(self.num_flows) if f not in self.no_flows])
        rem_cap = np.array(self.c, copy=True)   
        while rem_flows.size != 0:
            link_weights = self.routes.T.dot(weights)
            print rem_cap
            with np.errstate(divide='ignore', invalid='ignore'):
                bl = np.argmax(np.where(link_weights>0.0, link_weights/rem_cap, -1))
                inc = rem_cap[bl]/link_weights[bl]
            x[rem_flows] = x[rem_flows] + inc*weights[rem_flows]                
            rem_cap = rem_cap - inc*link_weights
            rem_cap = np.where(rem_cap>0.0, rem_cap, 0.0)       
            bf = np.nonzero(self.routes[:,bl])[0]
            rem_flows = np.array([f for f in rem_flows if f not in bf])
            weights[bf] = 0 
        return x

    # step uses existing link_to_flow and flow_to_link
    def step(self):
        self.t += 1
        
        print '$$$$$$$$ t = ', self.t
        print 'flow_to_link='
        print self.flow_to_link
        self.update_link_to_flow1()
        print 'link_to_flow='
        print self.link_to_flow
        #print 'errors='
        #print self.errors
        self.update_flow_to_link()
              
        self.xs = np.column_stack((self.xs, self.x))
        self.errors = np.column_stack((self.errors, max(np.abs(self.x - self.maxmin_x))))

    def mp(self):
        print "Starting message passing with "
        print "starting optimal rates: "
        print self.maxmin_x
        print "starting actual rates: "
        print self.x
        
        maxSteps = 50
        i = 0
        #(not (self.x.all() != self.maxmin_x.all()))
        while i < maxSteps and max(np.abs(self.x - self.maxmin_x)) > 0.1:
            self.step()
            i += 1
            pass
        if max(np.abs(self.x - self.maxmin_x)) > 0.1:
            print "!! didn't converge to optimal"
            pass
        print "optimal rates: "
        print self.maxmin_x
        print "actual rates: "
        print self.x
        pass
    
    def update_link_to_flow1(self):
        for l in range(self.num_links):
            flow_indices = np.nonzero(self.routes[:,l])[0]
            for ind in range(len(flow_indices)):
                temp = list(flow_indices)
                #del temp[ind]
                f = flow_indices[ind]
                print 'updated link to flow for link %d, flow %d\n' % (l, f)
                if len(temp) == 0:
                    self.link_to_flow[l,f] = self.c[l]
                else:  
                    demands = self.flow_to_link[temp,l]
                    if (sum(demands) > self.c[l]):
                        self.link_to_flow[l,f] = self.fair_share1(demands, self.c[l])
                        pass
                    else:
                        self.link_to_flow[l,f] = self.c[l]
                        pass
                    pass
                pass
            pass
        pass
    
    def update_flow_to_link(self):
        for f in range(self.num_flows):
            if f in self.no_flows:
                continue

            link_indices = np.nonzero(self.routes[f,:])[0]
            self.x[f] = min(self.link_to_flow[link_indices,f])
            for ind in range(len(link_indices)):
                temp = list(link_indices)
                del temp[ind]
                l = link_indices[ind]
                print 'updated flow to link for flow %d, link %d\n' % (f, l)
                if len(temp) == 0:
                    self.flow_to_link[f,l] = max_capacity
                else:
                    fair_shares = self.link_to_flow[temp,f]
                    self.flow_to_link[f,l] = min(fair_shares)

                pass
            pass
        pass

    def parse_flow_changes(self):
        pass
    
    def fair_share1(self, demands, cap):
        demands = sorted(demands)
        #demands.append(max_capacity)
        nflows = len(demands)
        level = 0.0
        mycap = float(cap)
        for f in range(nflows):
            rem_share = mycap / (nflows - f)
            inc = np.min([rem_share, demands[f]])
            level += inc
            mycap -= inc*(nflows-f)
            demands = demands - inc
        return level
                                        
    def print_details(self):
        print 'iteration=', self.t
        print 'x=', self.x
        print 'maxmin=', self.maxmin_x
        print 'l2 error=', np.linalg.norm(self.x - self.maxmin_x)
        print 'linf error=', max(np.abs(self.x - self.maxmin_x))

def gen_random_bipartite_instance(nports, nflows):
    A = np.zeros((nflows, 2*nports))
    for i in range(nflows):
        src = np.random.randint(nports)
        dst = np.random.randint(nports)
        A[i, src] = 1
        A[i, nports+dst] = 1
    c = np.ones((2*nports, 1))
    return A,c

def gen_large_chain(nports):
    nflows = nports*(nports+1)/2
    A = np.zeros((nflows, 2*nports))
    f = 0
    for j in range(nports):
        for i in range(j,nports):
            A[f,i] = 1
            A[f,nports+j] = 1
            f += 1
    c = np.ones((2*nports, 1))
    return A,c

def parse(numFlows, numPorts):
    numLinks = numPorts * 2

    incidence = []
    for f in range(numFlows):
        links = []
        for l in range(numLinks):
            links.append(0)
            pass
        incidence.append(links)
        pass

    A = np.array(incidence)

    #     A = np.array([[1,0],
    #                   [1,1],
    #                   [1,0],
    #                   [0,1]])       
    capacities = [[]]
    for l in range(numLinks):
        capacities[0].append(4.0)
        pass
    c = np.array(capacities).T
    #A, c = gen_random_bipartite_instance(50, 130)
    print 'flow uses link (A):'
    print A
    
    wf_maxmin = MPMaxMin(A,c)
    print 'starting at %d' % wf_maxmin.t
    time = 0
    remFlows = {}
    
    for line in fileinput.input():
        words = re.split(' |\.', line)
        fromIndex = words.index('from')
        f = int(words[fromIndex-1])
        src = int(words[fromIndex+1])
        dst = int(words[fromIndex+3])+numPorts
        if 'Adding' in words:
            remFlows[f] = wf_maxmin.t
            wf_maxmin.add_flow_path(f, [src, dst])
            pass
        elif 'Removing' in words:
            del remFlows[f]
            wf_maxmin.remove_flow(f)
            pass
        wf_maxmin.doWaterfilling()
        wf_maxmin.mp()
        
        print "!!! %s: %d" % (line.rstrip(), wf_maxmin.t - time)
        time = wf_maxmin.t
        pass

    flows = sorted(remFlows.items(), key=operator.itemgetter(1))
    for f in flows:
        wf_maxmin.remove_flow(f[0])
        wf_maxmin.doWaterfilling()
        wf_maxmin.mp()        
        print "!!! %s: %d" % ("Removing flow %d"%f[0], wf_maxmin.t - time)
        time = wf_maxmin.t
        pass
    pass

def main():
    np.random.seed(241)
    plt.close("all")
    
    parse(numFlows=64, numPorts=4)
    
    """    
    A,c = gen_large_chain(5)  
    wf_maxmin = MPMaxMin(A, c)
    for i in range(20):
        wf_maxmin.step()    
    error5 = wf_maxmin.errors

    A,c = gen_large_chain(10)  
    wf_maxmin = MPMaxMin(A, c)
    for i in range(20):
        wf_maxmin.step()   
    error10 = wf_maxmin.errors 
                                   
    A,c = gen_large_chain(50)  
    wf_maxmin = MPMaxMin(A, c)
    for i in range(20):
        wf_maxmin.step()   
    error50 = wf_maxmin.errors 

    A,c = gen_large_chain(100)  
    wf_maxmin = MPMaxMin(A, c)
    for i in range(20):
        wf_maxmin.step()   
    error100 = wf_maxmin.errors         
                                                                                                
    plt.figure('link rate')
    plt.title('link rates')
    plt.plot(wf_maxmin.xs.T.dot(wf_maxmin.routes))
    plt.legend(range(wf_maxmin.num_links))
    plt.show()
                    
    plt.figure('flow rate')
    plt.title('flow rates')
    plt.plot(wf_maxmin.xs.T)
    plt.legend(range(wf_maxmin.num_flows))
    plt.show()
    
    plt.figure('error')
    plt.title('error')
    plt.semilogy(wf_maxmin.errors.T)
    plt.ylabel('max element-wise error')
    plt.xlabel('iteration')
    plt.show()
 
    plt.figure('error2')
    plt.title('error')
    plt.semilogy(error5.T)
    plt.semilogy(error10.T)
    plt.semilogy(error50.T)
    plt.semilogy(error100.T)
    plt.legend(['N=5', 'N=10', 'N=50', 'N=100'])
    plt.ylabel('max element-wise error')
    plt.xlabel('iteration')
    plt.show()
    """
    sys.stdout.flush()
                    
if __name__ == '__main__':
    main()
    pass



    
